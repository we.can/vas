from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.manager.blueprint import mod_manager
from app.manager.models import Manager
from app.campaign.models import Campaign
from app.campaign.models import Location
import datetime as dt

# Add locations to a campaign
@mod_manager.route('/add_locations', methods=['POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def add_locations():
    print("Attempting to add location to campaign.")

    # check if manager
    cookieID = request.cookies.get('cookieID')
    if not Manager.is_manager(cookieID):
        print("Add location failed. Only managers can perform this action.")
        return make_response(jsonify({'comment': "Add location failed. Only managers can perform this action.", 'status': 403}))

    # get request
    req = request.get_json(force=True)
    print(req)

    # get campaign from db
    campaign = Campaign.query.filter_by(id=req['campaign_id']).first()
    if not campaign:
        print("Add location failed. Campaign not found.")
        return make_response(jsonify({'comment': "Add location failed. Campaign not found.", 'status': 404}))

    # if campaign started, cannot make edits
    today_str = str(dt.date.today())
    
    if today_str >= campaign.startDate:
        print("Add location failed. Campaign already started.")
        return make_response(jsonify({
            'comment': "Add location failed. Campaign already started", 
            'status': 400,
            'locked': True
        }))

    locations = Location.many_from_json(req)
    
    for l in locations:
        # add location to db
        db.session.add(l)
        db.session.commit()

    # return success
    print("Location(s) added to %s" % campaign)
    resp = make_response(jsonify({
        'comment': "Location(s) added to %s" % campaign, 
        'status': 200,
        'locked': False
    }))
    return resp
