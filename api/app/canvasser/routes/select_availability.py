from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.canvasser.blueprint import mod_canvasser
from app.auth.models import User
from app.canvasser.models import Canvasser
from app.canvasser.models import Date

@mod_canvasser.route('/select_availability', methods=['POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def select_availability():
    print("Canvasser selected a contiguous range of dates to mark as available. Attempting set availability.")

    # check if canvasser
    cookieID = request.cookies.get('cookieID')
    if not Canvasser.is_canvasser(cookieID):
        print("Select availability failed. Only canvassers can perform this action.")
        return make_response(jsonify({'comment': "Select availability failed. Only canvassers can perform this action.", 'status': 403}))

    # get request
    req = request.get_json(force=True)
    print(req)

    # get canvasser from db
    user = User.query.filter_by(cookie_id=cookieID).first()
    if not user:
        print("Select availability failed. User not found.")
        return make_response(jsonify({'comment': "Select availability failed. User not found.", 'status': 404}) )
    canvasser = user.canvasser

    # remove dates canvasser already has
    for d in canvasser.dates:
        if d.date in req['dates']:
            req['dates'].remove(d.date)

    # add dates to db
    for date_str in req['dates']:
        to_add_date = Date(date=date_str)
        canvasser.dates.append(to_add_date)

    # commit to db
    db.session.commit()

    # return success
    print("The specified dates have been selected as available.")
    resp = make_response(jsonify({
        'comment': "The specified dates have been selected as available", 
        'status': 200}))
    return resp

