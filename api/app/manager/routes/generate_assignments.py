from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.manager.blueprint import mod_manager
from app.manager.models import Manager
from app.campaign.models import Campaign
from app.campaign.models import Task
from app.campaign.models import Location
import datetime as dt

@mod_manager.route('/generate_assignments', methods=['POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def generate_assignments():
    print("Attempting to generate canvassing assignments.")

    # check if manager
    cookieID = request.cookies.get('cookieID')
    if not Manager.is_manager(cookieID):
        print("Generate canvassing assignments failed. Only managers can perform this action.")
        return make_response(jsonify({'comment': "Generate canvassing assignments failed. Only managers can perform this action.", 'status': 403}))

    # get request
    req = request.get_json(force=True)
    print(req)

    # get campaign from db
    campaign = Campaign.query.filter_by(id=req['campaign_id']).first()
    if not campaign:
        print("Generate canvassing assignments failed. Campaign not found")
        return make_response(jsonify({'comment': "Generate canvassing assignments failed. Campaign not found", 'status': 404}))

    # if campaign started, cannot make edits
    today_str = str(dt.date.today())
    if today_str >= campaign.startDate:
        print("Generate assignments failed. Campaign already started.")
        return make_response(jsonify({
            'comment': "Generate assignments failed. Campaign already started", 
            'status': 400,
            'locked': True}))

    success = Campaign.generate_assignments(campaign)
    
    if not success:
        # return failure
        print("WARNING: Assignments cannot be generated. Not enough canvassers days for campaign.")
        return make_response(jsonify({'status': 400, 'comment': "WARNING: Assignments cannot be generated. Not enough canvassers days for campaign."}))

    # return success
    print("Assignments have been generated.")
    resp = make_response(jsonify({
        'status': 200, 
        'comment': "Assignments have been generated.",
        'locked': False
    }))
    return resp
