__author__ = 'Manjur Khan'
__project__ = 'Canvass'
__credits__ = ['Manjur Khan']

# Statement for enabling the development environment
DEBUG = True

# Define the application directory
import os
BASE_DIR = os.path.abspath(os.path.dirname(__file__))  

# Define the database - we are working with
# SQLite for this example
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASE_DIR, 'app.db')
# SQLALCHEMY_DATABASE_URI = "mysql+mysqlconnector://cse308:lifeishard@54.172.85.69/canvas"
DATABASE_CONNECT_OPTIONS = {}

# Application threads. A common general assumption is
# using 2 per available processor cores - to handle
# incoming requests using one and performing background
# operations using the other.
THREADS_PER_PAGE = 2

CORS_HEADERS = 'Content-Type'
SESSION_COOKIE_HTTPONLY = True
REMEMBER_COOKIE_HTTPONLY = True

# Enable protection agains *Cross-site Request Forgery (CSRF)*
# CSRF_ENABLED     = True
# CSRF_ENABLED     = False

# Use a secure, unique and absolutely secret key for
# signing the data. 
#CSRF_SESSION_KEY = "secret"

# Secret key for signing cookies
SECRET_KEY = "v2m3C48%OH5S8TUGJqGZygzBP6J3m#IWEnaxp6SR8Y3lR2H9jX"
