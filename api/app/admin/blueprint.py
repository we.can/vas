from flask import Blueprint

# Define the blueprint: 'admin', set its url prefix: app.url/admin
mod_admin = Blueprint('admin', __name__, url_prefix='/api/admin')
 