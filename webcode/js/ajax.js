var URL = "http://lifeishard.fyi/api";

function makePost(path, data, success_f, error_f) {
  path = URL + path;
  console.log(data);
  console.log("it", path);
  if (error_f === undefined) {
    error_f = function(exception) {
      console.log("Exeption:" + exception);
    };
  }
  $.ajax({
    url: path,
    method: "POST",
    data: JSON.stringify(data),
    success: success_f,
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    error: error_f
  });
  console.log(data);
}

function makeGet(path, data, success_f) {
  path = URL + path;
  console.log(data);
  console.log("it", path);
  $.ajax({
    url: path,
    method: "GET",
    success: success_f,
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    error: function(exception) {
      console.log("Exeption:" + exception);
    }
  });
  console.log(data);
}
