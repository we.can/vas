function setupMap(markers) {
  // L.mapquest.key = "jW8WlC6YtKY6KwY0aLiA3P9JXkrGLHJN";

  // // 'map' refers to a <div> element with the ID map
  // map = L.mapquest.map("map", {
  //   center: [markers[0]["_latlng"]["lng"], markers[0]["_latlng"]["lat"]],
  //   layers: L.mapquest.tileLayer("map"),
  //   zoom: 13
  // });

  // var dir;
  var map;
  if (markers.length == 0) {
    map = L.map("map", {
      layers: MQ.mapLayer(),
      center: [40.9257, 73.1409],
      zoom: 15
    });
    return map;
  } else {
    var group = L.featureGroup(markers);
    var bounds = group.getBounds();
    var lat_center =
      (bounds["_northEast"]["lat"] + bounds["_southWest"]["lat"]) / 2;
    var lng_center =
      (bounds["_northEast"]["lng"] + bounds["_southWest"]["lng"]) / 2;

    map = L.map("map", {
      layers: [MQ.mapLayer(), group],
      center: [lng_center, lat_center],
      zoom: 13
    });

    for (i = 0; i < markers.length; i++) {
      markers[i].addTo(map);
    }
    setTimeout(function() {
      map.fitBounds(group.getBounds());
    }, 1000);
    return map;
  }
}

function getDirections(address, map) {
  var dir;

  dir = MQ.routing.directions();

  dir.route({
    locations: address
  });

  map.addLayer(
    MQ.routing.routeLayer({
      directions: dir,
      fitBounds: true
    })
  );
}

function setupPopup(markers, popupArr) {
  for (i = 0; i < markers.length; i++) {
    markers[i].bindPopup(popupArr[i]);
  }
}
