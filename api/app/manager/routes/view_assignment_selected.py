from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.manager.blueprint import mod_manager
import datetime as dt
from app.manager.models import Manager
from app.campaign.models import Task

@mod_manager.route('/view_assignment_selected', methods=['POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def view_assignment_selected():
    print("Manager selected an assignment (task) from a campaign. Attempting to view assignment details.")

    # check if manager
    cookieID = request.cookies.get('cookieID')
    if not Manager.is_manager(cookieID):
        print("View assignment details failed. Only managers can perform this action.")
        return make_response(jsonify({'comment': "View assignment details failed. Only managers can perform this action.", 'status': 403}))

    # get request
    req = request.get_json(force=True)
    print(req)

    # get task 
    task = Task.query.filter_by(id=req['task_id']).first()
    if not task:
        print("View assignment details failed. Task not found.")
        return make_response(jsonify({'comment': "View assignment details failed. Task not found.", 'status': 404}))
        
    # return success
    print("Returning locations for selected assignment: %s" % [l.serialize() for l in task.locations])
    resp = make_response(jsonify({
        'comment': "Returning locations for selected assignment", 
        'materials': [l.serialize() for l in task.locations], 
        'status': 200}))
    return resp
