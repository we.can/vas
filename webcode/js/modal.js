//id: modal id (target), section1: cotnent, submitFunc: handler
function modalHandler(id, target, submitFunc, showFunc) {
  $(target).attr("id", id);

  //TO-DO: Clean this function by modularizing
  // appendSectionHTML(htmlContent, ".mSection1");

  $(target).on("show.bs.modal", function(event) {
    var trigger = $(event.relatedTarget);
    var title = trigger.data("title");
    $(target + " .dynamicModalTitle").text(title);
  });
  if (showFunc != undefined) {
    showFunc();
  }
  submitHandler(submitFunc);
}

function submitHandler(submitFunc) {
  $(".submit").click(function() {
    submitFunc();
  });
}

//Append HTML to a section (NOT LOAD)
//@htmlContent: html to load
//@section: section class to load to
//@showFunc: optional function to run when appended
function appendSectionHTML(htmlContent, section, setupFunc) {
  var object = $("<div/>")
    .html(htmlContent)
    .contents();
  $(section).append(object);

  //If setup function isn't undefined run
  if (setupFunc != undefined) {
    setupFunc();
  }
}

//Replace HTML to a section (NOT LOAD)
//@htmlContent: html to load
//@section: section class to load to
//@showFunc: optional function to run when appended
function replaceSectionHTML(htmlContent, section, setupFunc) {
  var object = $("<div/>")
    .html(htmlContent)
    .contents();
  $(section).html(object);

  //If setup function isn't undefined run
  if (setupFunc != undefined) {
    setupFunc();
  }
}

function hideHandler() {
  $(".dynamicModal").on("hidden.bs.modal", function(event) {
    location.reload(true);

    // var trigger = $(event.relatedTarget);
    // var title = trigger.data("title");
    // $(".dynamicModalTitle").text(title);

    if (hideFunc != undefined) {
      hideFunc();
    }
  });
}
