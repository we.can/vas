$(document).ready(function() {
  hasStarted();
});

function hasStarted() {
  var startDate = new Date(getQueryParm("startDate"));
  var todayDate = new Date();
  //Today's date is pass the start date
  if (todayDate > startDate) {
    $(".disable-check").prop("disabled", true);
  }
}
