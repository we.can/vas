from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.auth.models import User
from app.auth.blueprint import mod_auth

@mod_auth.route('/logout', methods=['GET'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def logout():
    # logout
    #TODO
    cookieID = request.cookies.get('cookieID')
    if (cookieID is not None) and (cookieID is not ""):
        user = User.query.filter_by(cookie_id=cookieID).first()
        if user:
            user.cookie_id = None
            db.session.commit()
            
    resp = make_response(jsonify({'comment': 'Logout successful.', 'status': 200}))
    resp.set_cookie('cookieID', "", expires=0)
    resp.set_cookie('name', "", expires=0)
    resp.set_cookie('email', "", expires=0)
    resp.set_cookie('manager', "", expires=0)
    resp.set_cookie('admin', "", expires=0)
    resp.set_cookie('canvasser', "", expires=0)
    # return success
    return resp
