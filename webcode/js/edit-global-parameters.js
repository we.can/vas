$(document).ready(function() {
  var path = "/admin/get_global_parameters";
  var data = {};
  makeGet(path, data, prefillForm);
  onSubmit();
});

function prefillForm(data) {
  $(".workday").attr("value", data["materials"]["workday_duration"]);
  $(".speed").attr("value", data["materials"]["travel_speed"]);
}

function onSubmit() {
  $(".submit").click(function() {
    var path = "/admin/edit_global_parameters";
    var workday = $(".workday").prop("value");
    var speed = $(".speed").prop("value");
    var data = {
      workday_duration: workday,
      travel_speed: speed
    };
    makePost(path, data, success_f);
  });
}

function success_f() {
  alert("Successful Updated");
  location.reload();
}
