from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.manager.blueprint import mod_manager
from app.manager.models import Manager
from app.campaign.models import Campaign
from app.auth.models import User

@mod_manager.route('/get_users_add_campaign', methods=['POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def get_users_add_campaign():
    print("Attempting to get users to add to campaign ")
    
    # check if manager
    cookieID = request.cookies.get('cookieID')
    if not Manager.is_manager(cookieID):
        print("Get get users to add to campaign failed. Only manager can perform this action.")
        return make_response(jsonify({'status': 403, 'comment': "Get get users to add to campaign failed. Only manager can perform this action."}))
    
    # get request
    req = request.get_json(force=True)
    print(req)

    # get campaign from db
    campaign = Campaign.query.filter_by(id=req['campaign_id']).first()
    if not campaign:
        print("Get get users to add to campaign failed. Specified campaign not found")
        return make_response(jsonify({'status': 404, 'comment': "Get get users to add to campaign failed. Specified campaign not found"}))

    # query all users
    users = User.query.all()

    # to_ret
    users_to_ret = []
    for u in users:
        if (u.manager or u.canvasser) and not (u.manager in campaign.managers and u.canvasser in campaign.canvassers):
            users_to_ret.append(u)
    
    print(users_to_ret)

    # change dict roles if already in campaign
    to_ret = []
    for u in users_to_ret:
        temp = u.serialize()
        if u.manager in campaign.managers:
            temp['role']['manager'] = False
        if u.canvasser in campaign.canvassers:
            temp['role']['canvasser'] = False
        to_ret.append(temp)

    # return success
    print("Returning users for adding to campaign: %s" % users)
    resp = make_response(jsonify({'comment': 'Returning users for adding to campaign.', 'materials': to_ret, 'status': 200}))
    return resp
