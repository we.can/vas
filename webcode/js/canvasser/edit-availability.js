$(function() {
  $("#calendar").fullCalendar({
    selectable: true,
    header: {
      left: "prev,next today",
      center: "title",
      right: "month"
    },
    select: function(startDate, endDate) {
      if (startDate.isBefore(moment())) {
        $("#calendar").fullCalendar("unselect");
        alert("Date is passes already");
      } else {
        selectedHandler(startDate, endDate);
      }
    }
  });
  var path = "/canvasser/get_availability";
  var data = {};
  makePost(path, data, highlightDates);
  fcButtonHandler();
  $(".modal-container").load("/components/modal.html", undefined, setupModal);
});

function success_f(data) {
  console.log(data);
}

function selectedHandler(startDate, endDate) {
  function showModalWrapper() {
    $("#AvailabilityModal").modal("show");
  }
  var htmlContent;

  var now = startDate,
    dates = [];

  while (now.isSameOrBefore(endDate)) {
    dates.push(now.format("YYYY-MM-DD"));
    now.add(1, "days");
  }
  dates.pop();

  $(".date-submit").attr("value", dates);

  //Check if deselecting
  if ($('[data-date="' + dates[0] + '"]').hasClass("isSelected")) {
    htmlContent =
      "<p>You de-selected the follow avaialability</p>" +
      "<h6>" +
      dates[0] +
      " to " +
      dates[dates.length - 1];
    $(".dynamicModalTitle").text("De-Select Availability");
  } else {
    htmlContent =
      "<p>You selected the follow avaialability</p>" +
      "<h6>" +
      dates[0] +
      " to " +
      dates[dates.length - 1];
    $(".dynamicModalTitle").text("Add Availability");
  }

  replaceSectionHTML(htmlContent, ".mSection1", showModalWrapper);
}

function setupModal() {
  //Handler for submitting function
  function submitFunc() {
    var path;
    var dates = $(".date-submit")
      .attr("value")
      .split(",");

    var data = {
      email: "john.doe@gmail.com",
      dates: dates
    };
    highlightDates(data["dates"]);
    console.log(data);

    if ($(".dynamicModalTitle").text() === "Add Availability") {
      path = "/canvasser/select_availability";
      selectDates(dates);
    } else {
      path = "/canvasser/deselect_availability";
      deselectDates(dates);
    }
    makePost(path, data, success_f);
  }
  modalHandler("AvailabilityModal", ".dynamicModal", submitFunc);
}

function success_f(data) {
  console.log(data);
  // if (data["comment"] === "The specified dates have been deselected") {
  //   location.reload(true);
  // }
  $("#AvailabilityModal").modal("hide");
}

function highlightDates(data) {
  console.log(data);
  for (item in data["materials"]) {
    var date = data["materials"][item];
    $('[data-date="' + date + '"]').css("background-color", "grey");
    $('[data-date="' + date + '"]').addClass("isSelected");
  }
}

function fcButtonHandler() {
  $(".fc-button").click(function() {
    var path = "/canvasser/get_availability";
    var data = {};
    makePost(path, data, highlightDates);
  });
}

function deselectDates(data) {
  console.log(data);
  for (item in data) {
    var date = data[item];
    $('[data-date="' + date + '"]').css("background-color", "");
    $('[data-date="' + date + '"]').removeClass("isSelected");
  }
}

function selectDates(data) {
  console.log(data);
  for (item in data) {
    var date = data[item];
    $('[data-date="' + date + '"]').css("background-color", "grey");
    $('[data-date="' + date + '"]').addClass("isSelected");
  }
}
