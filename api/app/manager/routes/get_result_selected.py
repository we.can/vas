from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.manager.blueprint import mod_manager
from app.manager.models import Manager
from app.campaign.models import Campaign
from app.campaign.models import Location
from app.questionnaire.models import Response

@mod_manager.route('/get_result_selected', methods=['POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def get_result_selected():
    print("Manager selected a loction. Attempting to get detailed results of selected location.")

    # check if manager
    cookieID = request.cookies.get('cookieID')
    if not Manager.is_manager(cookieID):
        print("Get detailed results failed. Only managers can perform this cation")
        return make_response(jsonify({'status': 403, 'comment': "Get detailed results failed. Only managers can perform this cation"}))
        
    # get request
    req = request.get_json(force=True)
    print(req)

    # get campaign from db
    campaign = Campaign.query.filter_by(id=req['id']).first()
    if not campaign:
        print("Get detailed results failed. Specified campaign not found")
        return make_response(jsonify({'status': 404, 'comment': "Get detailed results failed. Specified campaign not found"}))

    # get location from db
    location = Location.query.filter_by(id=req['location_id']).first()
    if not location:
        print("Get detailed results failed. Specified location not found")
        return make_response(jsonify({'status': 404, 'comment': "Get detailed results failed. Specified location not found"}))

    # questions and responses
    responses = [r.response for r in location.responses]
    questions = [q.question for q in location.campaign.questions]
    # to_ret 
    to_ret = {
        'if_spoken': location.if_spoken if location.if_spoken else "In Progress",
        'rating': location.rating if location.rating else "In Progress",
        'responeses': responses,
        'questions': questions,
        'notes': location.notes if location.notes else "In Progress"
    }
    
    # return success
    print("Returning detailed results for %s: %s" % (location, to_ret))
    resp = make_response(jsonify({
        'comment': "Returning detailed results for %s" % location, 
        'materials': to_ret, 
        'status': 200}))
    return resp
