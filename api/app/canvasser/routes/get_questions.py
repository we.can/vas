from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.canvasser.blueprint import mod_canvasser
from app.manager.models import Manager
from app.campaign.models import Campaign
from app.campaign.models import Task

@mod_canvasser.route('/get_questions', methods=['POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def get_questions():
    print("Attempting to get question to enter results.")

    # check if manager
    cookieID = request.cookies.get('cookieID')
    if not Canvasser.is_canvasser(cookieID):
        print("Get question to campaign failed. Only canvasser can perform this action.")
        return make_response(jsonify({'comment': "Get question to campaign failed. Only canvasser can perform this action.", 'status': 403}))

    # get request
    req = request.get_json(force=True)
    print(req)

    # get campaign from db
    task = Task.query.filter_by(id=req['task_id']).first()
    if not task:
        print("Get question failed. Task not found.")
        return make_response(jsonify({'comment': "Get question failed. Task not found.", 'status': 404}))
    campaign = task.campaign

    # get questions
    questions = [q.question for q in campaign.questions]

    # return success
    print("Return questions for %s: %s" % (campaign, questions))
    resp = make_response(jsonify({
        'comment': "Return questions for %s" % (campaign), 
        'materials': questions,
        'status': 200}))
    return resp
