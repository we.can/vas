from flask import Blueprint

# Define the blueprint: 'manager', set its url prefix: app.url/manager
mod_manager = Blueprint('manager', __name__, url_prefix='/api/manager')
 