from flask import Blueprint

# Define the blueprint: 'campaign', set its url prefix: app.url/campaign
mod_campaign = Blueprint('campaign', __name__, url_prefix='/api/campaign')
 