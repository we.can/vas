from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.canvasser.blueprint import mod_canvasser
from app.canvasser.models import Canvasser
from app.campaign.models import Task

@mod_canvasser.route('/view_assignment_selected', methods=['POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def view_assignment_selected():
    print("Canvasser selected an assignment (task) from upcomming assignments. Attempting to view assignment details.")

    # check if canvasser
    cookieID = request.cookies.get('cookieID')
    if not Canvasser.is_canvasser(cookieID):
        print("View assignment details failed. Only canvassers can perform this action.")
        return make_response(jsonify({'comment': "View assignment details failed. Only canvassers can perform this action.", 'status': 403}))

    # get request
    req = request.get_json(force=True)
    print(req)

    # get task 
    task = Task.query.filter_by(id=req['task_id']).first()
    if not task:
        print("View assignment details failed. Task not found.")
        return make_response(jsonify({'comment': "View assignment details failed. Task not found.", 'status': 404}))
        
    # to_ret
    to_ret = [l.serialize() for l in task.locations]
    i = 0
    for l in to_ret:
        l['number'] = i
        i += 1

    # return success
    print("Returning locations for selected assignment: %s" % to_ret)
    resp = make_response(jsonify({
        'comment': "Returning locations for selected assignment", 
        'materials': to_ret, 
        'status': 200}))
    return resp

