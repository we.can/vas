# Test Sign-in

## Testing Url Path
```code
http://54.172.85.69:8080/auth/signin
```

## Test Name: Valid Admin

### Test Reason
Testing to see if the User is set correctly and that the user can login properly with the correct access level

#### Data Expectations
The person is an admin, so we expect the return value to have user's first name, last name, email, and admin role to be true. Additionally a status code of 200 should be returned

#### Data Sent
```json
{'email': 'daniel.li.2@stonybrook.edu', 'password': 'lifeishard'}
```

#### Data Recieved
```json
{'status': 200, 'materials': {'email': 'daniel.li.2@stonybrook.edu', 'firstname': 'Daniel', 'lastname': 'Li', 'phone': '18000flowers', 'role': {'canvasser': False, 'manager': False, 'admin': True}}, 'comment': 'User was successfully logged in'}
```

#### Output Result

- Status code matches the expected status code of 200
- The user was supposed to have admin privilege and this user has admin privilege

### Raw Response

```
< POST /auth/signin HTTP/1.1
< Host: 54.172.85.69:8080
< Connection: keep-alive
< Accept-Encoding: gzip, deflate
< Accept: */*
< User-Agent: python-requests/2.19.1
< Content-Length: 65
< Content-Type: application/json
< 
< {"email": "daniel.li.2@stonybrook.edu", "password": "lifeishard"}
> HTTP/1.0 200 OK
> Content-Type: application/json
> Content-Length: 309
> Access-Control-Allow-Origin: *
> Vary: Cookie
> Set-Cookie: session=.eJxNjTEKAjEQRe8y9ZJiy1QWlt5h-WtmZXAygUwiiHh3Bwux_I_H-y9CqWKUR5-80BX2gDt3ygfUg3CFKGUqMGFNKmk9-Wj23Htr98Rl0kKHdB-GyiGev2JAxY9dJHaF4fZXnnGzSaG8vj9E8y3a.DqYmAQ.F5WwHptgf6NxbSchIHi4cWBLPRY; HttpOnly; Path=/
> Server: Werkzeug/0.14.1 Python/3.6.6
> Date: Mon, 15 Oct 2018 13:22:09 GMT
> 
{
  "comment": "User was successfully logged in", 
  "materials": {
    "email": "daniel.li.2@stonybrook.edu", 
    "firstname": "Daniel", 
    "lastname": "Li", 
    "phone": "18000flowers", 
    "role": {
      "admin": true, 
      "canvasser": false, 
      "manager": false
    }
  }, 
  "status": 200
}

```
## Test Name: Invalid User

### Test Reason
Testing to see of the user is being blocked and the correct error code is returned

#### Data Expectations
Should return status code of 401 shoule be returned for invalid user

#### Data Sent
```json
{'email': 'manjur@gmail.com', 'password': 'hi'}
```

#### Data Recieved
```json
{'status': 401, 'comment': 'User email or password does not match'}
```

#### Output Result

- Status code matches the expected status code of 401

### Raw Response

```
< POST /auth/signin HTTP/1.1
< Host: 54.172.85.69:8080
< Connection: keep-alive
< Accept-Encoding: gzip, deflate
< Accept: */*
< User-Agent: python-requests/2.19.1
< Content-Length: 47
< Content-Type: application/json
< 
< {"email": "manjur@gmail.com", "password": "hi"}
> HTTP/1.0 200 OK
> Content-Type: application/json
> Content-Length: 75
> Access-Control-Allow-Origin: *
> Server: Werkzeug/0.14.1 Python/3.6.6
> Date: Mon, 15 Oct 2018 13:22:09 GMT
> 
{
  "comment": "User email or password does not match", 
  "status": 401
}

```
## Test Name: Valid Manager

### Test Reason
Testing to see if the User is set correctly and that the user can login properly with the correct access level

#### Data Expectations
The person is an admin, so we expect the return value to have user's first name, last name, email, and manager role to be true. Additionally a status code of 200 should be returned

#### Data Sent
```json
{'email': 'manjur.khan@stonybrook.edu', 'password': 'secret'}
```

#### Data Recieved
```json
{'status': 200, 'materials': {'email': 'manjur.khan@stonybrook.edu', 'firstname': 'Manjur', 'lastname': 'Khan', 'phone': '6463772357', 'role': {'canvasser': False, 'manager': True, 'admin': False}}, 'comment': 'User was successfully logged in'}
```

#### Output Result

- Status code matches the expected status code of 200
- The user was supposed to have manager privilege and this user has manager privilege

### Raw Response

```
< POST /auth/signin HTTP/1.1
< Host: 54.172.85.69:8080
< Connection: keep-alive
< Accept-Encoding: gzip, deflate
< Accept: */*
< User-Agent: python-requests/2.19.1
< Content-Length: 61
< Content-Type: application/json
< 
< {"email": "manjur.khan@stonybrook.edu", "password": "secret"}
> HTTP/1.0 200 OK
> Content-Type: application/json
> Content-Length: 309
> Access-Control-Allow-Origin: *
> Vary: Cookie
> Set-Cookie: session=.eJw9jTEKwzAQBP9ytTGkVZXe-A1hE51txdIJ7qRAMPl7LincDrOzByGWJBQWZOOBHpAXzFhPwgUpU6ACeXYd9w1ytVblfdda95Fjp4GWpNYEhV2c_6LDjJNNvnLiDay_dtPu6e4_txQpXD5f1hIvUw.DqYmAQ.uxSN5tXzsn3X63lUc8qUaaKEY50; HttpOnly; Path=/
> Server: Werkzeug/0.14.1 Python/3.6.6
> Date: Mon, 15 Oct 2018 13:22:09 GMT
> 
{
  "comment": "User was successfully logged in", 
  "materials": {
    "email": "manjur.khan@stonybrook.edu", 
    "firstname": "Manjur", 
    "lastname": "Khan", 
    "phone": "6463772357", 
    "role": {
      "admin": false, 
      "canvasser": false, 
      "manager": true
    }
  }, 
  "status": 200
}

```
## Test Name: Valid Email, But Invalid Password

### Test Reason
Testing to see of the user is being blocked and the correct error code is returned

#### Data Expectations
Should return status code of 401 shoule be returned for valid email but invalid password

#### Data Sent
```json
{'email': 'manjur.khan@stonybrook.edu', 'password': 'secrett'}
```

#### Data Recieved
```json
{'status': 401, 'comment': 'User email or password does not match'}
```

#### Output Result

- Status code matches the expected status code of 401

### Raw Response

```
< POST /auth/signin HTTP/1.1
< Host: 54.172.85.69:8080
< Connection: keep-alive
< Accept-Encoding: gzip, deflate
< Accept: */*
< User-Agent: python-requests/2.19.1
< Content-Length: 62
< Content-Type: application/json
< 
< {"email": "manjur.khan@stonybrook.edu", "password": "secrett"}
> HTTP/1.0 200 OK
> Content-Type: application/json
> Content-Length: 75
> Access-Control-Allow-Origin: *
> Server: Werkzeug/0.14.1 Python/3.6.6
> Date: Mon, 15 Oct 2018 13:22:09 GMT
> 
{
  "comment": "User email or password does not match", 
  "status": 401
}

```
## Test Name: Invalid Arguments

### Test Reason
Testing to see that if there is invalid arguments then the server returns bad reuqests

#### Data Expectations
Should return status code of 400 for bad requests

#### Data Sent
```json
{'password': 'secret', 'username': 'manjur.khan@stonybrook.edu'}
```

#### Data Recieved
```json
{'status': 400, 'comment': 'Login bad request'}
```

#### Output Result

- Status code matches the expected status code of 400

### Raw Response

```
< POST /auth/signin HTTP/1.1
< Host: 54.172.85.69:8080
< Connection: keep-alive
< Accept-Encoding: gzip, deflate
< Accept: */*
< User-Agent: python-requests/2.19.1
< Content-Length: 64
< Content-Type: application/json
< 
< {"password": "secret", "username": "manjur.khan@stonybrook.edu"}
> HTTP/1.0 200 OK
> Content-Type: application/json
> Content-Length: 55
> Access-Control-Allow-Origin: *
> Server: Werkzeug/0.14.1 Python/3.6.6
> Date: Mon, 15 Oct 2018 13:22:09 GMT
> 
{
  "comment": "Login bad request", 
  "status": 400
}

```
# Test Admin Privilege

## Testing Url Path
```code
http://54.172.85.69:8080/admin/register
```

## Test Name: Valid Admin Creating User

### Test Reason
Testing to see that only admin can create new users

#### Data Expectations
Expect code 201 for success on creating the user

#### Data Prerequisite: Perform Login

#### Now Attempt To Create User

#### Data Sent
```json
{'firstname': 'Joon', 'email': 'joon.lee@stonybrook.edu', 'lastname': 'Lee', 'phone': '6461236789', 'password': 'secret'}
```

#### Data Recieved
```json
{'status': 201, 'comment': 'User = Joon Lee Created'}
```

#### Output Result

- Status code matches the expected status code of 201

### Raw Response

```
< POST /admin/register HTTP/1.1
< Host: 54.172.85.69:8080
< Connection: keep-alive
< Accept-Encoding: gzip, deflate
< Accept: */*
< User-Agent: python-requests/2.19.1
< Cookie: session=.eJxNjTEKAjEQRe8y9ZJiy1QWlt5h-WtmZXAygUwiiHh3Bwux_I_H-y9CqWKUR5-80BX2gDt3ygfUg3CFKGUqMGFNKmk9-Wj23Htr98Rl0kKHdB-GyiGev2JAxY9dJHaF4fZXnnGzSaG8vj9E8y3a.DqYmAQ.F5WwHptgf6NxbSchIHi4cWBLPRY
< Content-Length: 121
< Content-Type: application/json
< 
< {"firstname": "Joon", "email": "joon.lee@stonybrook.edu", "lastname": "Lee", "phone": "6461236789", "password": "secret"}
> HTTP/1.0 200 OK
> Content-Type: application/json
> Content-Length: 61
> Access-Control-Allow-Origin: *
> Vary: Cookie
> Server: Werkzeug/0.14.1 Python/3.6.6
> Date: Mon, 15 Oct 2018 13:22:09 GMT
> 
{
  "comment": "User = Joon Lee Created", 
  "status": 201
}

```
## Test Name: Valid Manager Creating User

### Test Reason
Testing to see that even though the user has some privilage, they cannot create another user

#### Data Expectations
Expect code 403 for invalid permission to create user

#### Data Prerequisite: Perform Login

#### Now Attempt To Create User

#### Data Sent
```json
{'firstname': 'Andrew', 'email': 'andrew.auyoung@stonybrook.edu', 'lastname': 'AuYoung', 'phone': '6463772357', 'password': 'secret'}
```

#### Data Recieved
```json
{'status': 403, 'comment': 'Only admin can create another user'}
```

#### Output Result

- Status code matches the expected status code of 403

### Raw Response

```
< POST /admin/register HTTP/1.1
< Host: 54.172.85.69:8080
< Connection: keep-alive
< Accept-Encoding: gzip, deflate
< Accept: */*
< User-Agent: python-requests/2.19.1
< Cookie: session=.eJw9jTEKwzAQBP9ytTGkVZXe-A1hE51txdIJ7qRAMPl7LincDrOzByGWJBQWZOOBHpAXzFhPwgUpU6ACeXYd9w1ytVblfdda95Fjp4GWpNYEhV2c_6LDjJNNvnLiDay_dtPu6e4_txQpXD5f1hIvUw.DqYmAg.1NyuJAB4L0Copxrnn5pULNiYrfw
< Content-Length: 133
< Content-Type: application/json
< 
< {"firstname": "Andrew", "email": "andrew.auyoung@stonybrook.edu", "lastname": "AuYoung", "phone": "6463772357", "password": "secret"}
> HTTP/1.0 200 OK
> Content-Type: application/json
> Content-Length: 72
> Access-Control-Allow-Origin: *
> Vary: Cookie
> Server: Werkzeug/0.14.1 Python/3.6.6
> Date: Mon, 15 Oct 2018 13:22:10 GMT
> 
{
  "comment": "Only admin can create another user", 
  "status": 403
}

```
## Testing Url Path
```code
http://54.172.85.69:8080/admin/edit_user
```

## Test Name: Valid Admin Edit User Role

### Test Reason
Testing to see that only admin can edit users

#### Data Expectations
Expect code 200 for success on editing the user

#### Data Prerequisite: Perform Login

#### Now Attempt To Edit User Role

#### Data Sent
```json
{'email': 'joon.lee@stonybrook.edu', 'canvasser': True}
```

#### Data Recieved
```json
{'status': 200, 'comment': 'Successfully updated user joon.lee@stonybrook.edu role'}
```

#### Output Result

- Status code matches the expected status code of 200

### Raw Response

```
< POST /admin/edit_user HTTP/1.1
< Host: 54.172.85.69:8080
< Connection: keep-alive
< Accept-Encoding: gzip, deflate
< Accept: */*
< User-Agent: python-requests/2.19.1
< Cookie: session=.eJxNjTEKAjEQRe8y9ZJiy1QWlt5h-WtmZXAygUwiiHh3Bwux_I_H-y9CqWKUR5-80BX2gDt3ygfUg3CFKGUqMGFNKmk9-Wj23Htr98Rl0kKHdB-GyiGev2JAxY9dJHaF4fZXnnGzSaG8vj9E8y3a.DqYmAg.sahF3BzWqJmFMmKfUadJ3H-lnDY
< Content-Length: 55
< Content-Type: application/json
< 
< {"email": "joon.lee@stonybrook.edu", "canvasser": true}
> HTTP/1.0 200 OK
> Content-Type: application/json
> Content-Length: 92
> Access-Control-Allow-Origin: *
> Vary: Cookie
> Server: Werkzeug/0.14.1 Python/3.6.6
> Date: Mon, 15 Oct 2018 13:22:10 GMT
> 
{
  "comment": "Successfully updated user joon.lee@stonybrook.edu role", 
  "status": 200
}

```
## Test Name: Valid Manager Edit User Role

### Test Reason
Testing to see that even though the user has some privilage, they cannot edit user

#### Data Expectations
Expect code 403 for invalid permission to edit user

#### Data Prerequisite: Perform Login

#### Now Attempt To Edit User Role

#### Data Sent
```json
{'email': 'joon.lee@stonybrook.edu', 'manager': True}
```

#### Data Recieved
```json
{'status': 403, 'comment': 'Only admin can change user'}
```

#### Output Result

- Status code matches the expected status code of 403

### Raw Response

```
< POST /admin/edit_user HTTP/1.1
< Host: 54.172.85.69:8080
< Connection: keep-alive
< Accept-Encoding: gzip, deflate
< Accept: */*
< User-Agent: python-requests/2.19.1
< Cookie: session=.eJw9jTEKwzAQBP9ytTGkVZXe-A1hE51txdIJ7qRAMPl7LincDrOzByGWJBQWZOOBHpAXzFhPwgUpU6ACeXYd9w1ytVblfdda95Fjp4GWpNYEhV2c_6LDjJNNvnLiDay_dtPu6e4_txQpXD5f1hIvUw.DqYmAg.1NyuJAB4L0Copxrnn5pULNiYrfw
< Content-Length: 53
< Content-Type: application/json
< 
< {"email": "joon.lee@stonybrook.edu", "manager": true}
> HTTP/1.0 200 OK
> Content-Type: application/json
> Content-Length: 64
> Access-Control-Allow-Origin: *
> Vary: Cookie
> Server: Werkzeug/0.14.1 Python/3.6.6
> Date: Mon, 15 Oct 2018 13:22:10 GMT
> 
{
  "comment": "Only admin can change user", 
  "status": 403
}

```
## Testing Url Path
```code
http://54.172.85.69:8080/admin/edit_global_parameters
```

## Test Name: Valid Admin Edit Global Parameter

### Test Reason
Testing to see that only admin can edit global parameter

#### Data Expectations
Expect code 200 for success on editing the global parameter

#### Data Prerequisite: Perform Login

#### Now Attempt To Edit Global Parameters

#### Data Sent
```json
{'workdat_duration': 5, 'travel_speed': 10}
```

#### Data Recieved
```json
{'status': 200, 'comment': 'Global parameters successfully updated'}
```

#### Output Result

- Status code matches the expected status code of 200

### Raw Response

```
< POST /admin/edit_global_parameters HTTP/1.1
< Host: 54.172.85.69:8080
< Connection: keep-alive
< Accept-Encoding: gzip, deflate
< Accept: */*
< User-Agent: python-requests/2.19.1
< Cookie: session=.eJxNjTEKAjEQRe8y9ZJiy1QWlt5h-WtmZXAygUwiiHh3Bwux_I_H-y9CqWKUR5-80BX2gDt3ygfUg3CFKGUqMGFNKmk9-Wj23Htr98Rl0kKHdB-GyiGev2JAxY9dJHaF4fZXnnGzSaG8vj9E8y3a.DqYmAg.sahF3BzWqJmFMmKfUadJ3H-lnDY
< Content-Length: 43
< Content-Type: application/json
< 
< {"workdat_duration": 5, "travel_speed": 10}
> HTTP/1.0 200 OK
> Content-Type: application/json
> Content-Length: 76
> Access-Control-Allow-Origin: *
> Vary: Cookie
> Server: Werkzeug/0.14.1 Python/3.6.6
> Date: Mon, 15 Oct 2018 13:22:10 GMT
> 
{
  "comment": "Global parameters successfully updated", 
  "status": 200
}

```
## Test Name: Valid Manager Edit Global Parameter

### Test Reason
Testing to see that even though the user has some privilage, they cannot edit global parameter

#### Data Expectations
Expect code 403 for invalid permission to edit global parameter

#### Data Prerequisite: Perform Login

#### Now Attempt To Edit Global Parameters

#### Data Sent
```json
{'workdat_duration': 10, 'travel_speed': 5}
```

#### Data Recieved
```json
{'status': 403, 'comment': 'Only admin edit Global parameters'}
```

#### Output Result

- Status code matches the expected status code of 403

### Raw Response

```
< POST /admin/edit_global_parameters HTTP/1.1
< Host: 54.172.85.69:8080
< Connection: keep-alive
< Accept-Encoding: gzip, deflate
< Accept: */*
< User-Agent: python-requests/2.19.1
< Cookie: session=.eJw9jTEKwzAQBP9ytTGkVZXe-A1hE51txdIJ7qRAMPl7LincDrOzByGWJBQWZOOBHpAXzFhPwgUpU6ACeXYd9w1ytVblfdda95Fjp4GWpNYEhV2c_6LDjJNNvnLiDay_dtPu6e4_txQpXD5f1hIvUw.DqYmAg.1NyuJAB4L0Copxrnn5pULNiYrfw
< Content-Length: 43
< Content-Type: application/json
< 
< {"workdat_duration": 10, "travel_speed": 5}
> HTTP/1.0 200 OK
> Content-Type: application/json
> Content-Length: 71
> Access-Control-Allow-Origin: *
> Vary: Cookie
> Server: Werkzeug/0.14.1 Python/3.6.6
> Date: Mon, 15 Oct 2018 13:22:10 GMT
> 
{
  "comment": "Only admin edit Global parameters", 
  "status": 403
}

```
# Test Manager Privilege

## Testing Url Path
```code
http://54.172.85.69:8080/manager/create_campaign
```

## Test Name: Valid Admin Create New Campaign

### Test Reason
Testing to see that even though the user has some privilage, they cannot create campaign

#### Data Expectations
Expect code 403 for invalid permission to create a new campaign

#### Data Prerequisite: Perform Login

#### Now Attempt To Create New Campaign

#### Data Sent
```json
{'title': 'campaign 1', 'endDate': '10-11-18', 'startDate': '10-10-18'}
```

#### Data Recieved
```json
{'status': 403, 'comment': 'Only manager can create a campaign'}
```

#### Output Result

- Status code matches the expected status code of 403

### Raw Response

```
< POST /manager/create_campaign HTTP/1.1
< Host: 54.172.85.69:8080
< Connection: keep-alive
< Accept-Encoding: gzip, deflate
< Accept: */*
< User-Agent: python-requests/2.19.1
< Cookie: session=.eJxNjTEKAjEQRe8y9ZJiy1QWlt5h-WtmZXAygUwiiHh3Bwux_I_H-y9CqWKUR5-80BX2gDt3ygfUg3CFKGUqMGFNKmk9-Wj23Htr98Rl0kKHdB-GyiGev2JAxY9dJHaF4fZXnnGzSaG8vj9E8y3a.DqYmAg.sahF3BzWqJmFMmKfUadJ3H-lnDY
< Content-Length: 71
< Content-Type: application/json
< 
< {"title": "campaign 1", "endDate": "10-11-18", "startDate": "10-10-18"}
> HTTP/1.0 200 OK
> Content-Type: application/json
> Content-Length: 72
> Access-Control-Allow-Origin: *
> Vary: Cookie
> Server: Werkzeug/0.14.1 Python/3.6.6
> Date: Mon, 15 Oct 2018 13:22:10 GMT
> 
{
  "comment": "Only manager can create a campaign", 
  "status": 403
}

```
## Test Name: Valid Manager Create New Campaign

### Test Reason
Testing to see that manager can create campaign successfully

#### Data Expectations
Expect code 201 for success on creating a new campaign and a campaign ID

#### Data Prerequisite: Perform Login

#### Now Attempt To Create New Campaign

#### Data Sent
```json
{'title': 'campaign 2', 'endDate': '10-11-18', 'startDate': '10-10-18'}
```

#### Data Recieved
```json
{'status': 201, 'materials': {'id': 3}, 'comment': 'Campaign = campaign 2 Created'}
```

#### Output Result

- Status code matches the expected status code of 201
- Valid manager created the campaign with id 3 as expected

### Raw Response

```
< POST /manager/create_campaign HTTP/1.1
< Host: 54.172.85.69:8080
< Connection: keep-alive
< Accept-Encoding: gzip, deflate
< Accept: */*
< User-Agent: python-requests/2.19.1
< Cookie: session=.eJw9jTEKwzAQBP9ytTGkVZXe-A1hE51txdIJ7qRAMPl7LincDrOzByGWJBQWZOOBHpAXzFhPwgUpU6ACeXYd9w1ytVblfdda95Fjp4GWpNYEhV2c_6LDjJNNvnLiDay_dtPu6e4_txQpXD5f1hIvUw.DqYmAg.1NyuJAB4L0Copxrnn5pULNiYrfw
< Content-Length: 71
< Content-Type: application/json
< 
< {"title": "campaign 2", "endDate": "10-11-18", "startDate": "10-10-18"}
> HTTP/1.0 200 OK
> Content-Type: application/json
> Content-Length: 102
> Access-Control-Allow-Origin: *
> Vary: Cookie
> Server: Werkzeug/0.14.1 Python/3.6.6
> Date: Mon, 15 Oct 2018 13:22:10 GMT
> 
{
  "comment": "Campaign = campaign 2 Created", 
  "materials": {
    "id": 3
  }, 
  "status": 201
}

```
