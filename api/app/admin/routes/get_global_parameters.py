from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.admin.blueprint import mod_admin
from app.admin.models import Admin
from app.global_parameter.models import Global_Parameter

# Get the global parameters
@mod_admin.route('/get_global_parameters', methods=['GET'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def get_global_parameters():
    print("Attempting to get global parameters.")

    # check if admin
    cookieID = request.cookies.get('cookieID')
    if not Admin.is_admin(cookieID):
        print("Get global parameters failed. Only admin can perform this action.")
        return make_response(jsonify({'comment': "Get global parameters failed. Only admin can perform this action.", 'status': 403}))

    # get global parameters
    travel_speed = Global_Parameter.query.filter_by(name='travel_speed').first()
    workday_duration = Global_Parameter.query.filter_by(name='workday_duration').first()

    if not travel_speed:
        travel_speed = Global_Parameter(name="travel_speed", value=5)
        db.session.add(travel_speed)
    if not workday_duration:
        workday_duration = Global_Parameter(name="workday_duration", value=8)
        db.session.add(workday_duration)

    db.session.commit()

    gps = {
        'travel_speed': travel_speed.value,
        'workday_duration': workday_duration.value
    }
    print("Returning global parameters: %s" % gps)

    # return success
    resp = make_response(jsonify({'comment': 'Returning global parameters.', 'materials': gps, 'status': 200}))
    return resp