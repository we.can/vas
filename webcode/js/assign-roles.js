$(document).ready(function() {
  var path = "/manager/get_campaign_users";
  var data = {
    id: getQueryParm("cid")
  };
  makePost(path, data, parseData);

  // //#########TEST JSON#################
  // $.getJSON("/test_json/campaign-manager/assign-role.json", function(json) {
  //   console.log(json);
  //   parseData(json);
  // });
  // //###################################

  $(".modal-container").load("/components/modal.html", null, setupModal);
  createSideBar();
});

function assignUser(id, email) {
  var w = [];
  if ($(".canvas-select-container").css("display") != "none") {
    var can = $("#canvas-choice").attr("active");
    if (typeof can !== undefined && can !== false) {
      w.push("canvasser");
    }
  }

  if ($(".manager-select-container").css("display") != "none") {
    var can = $("#manager-choice").attr("active");
    if (typeof can !== undefined && can !== false) {
      w.push("manager");
    }
  }
  console.log(w);
  var path = "/manager/add_campaign_user";
  var data = {
    id: getQueryParm("cid"),
    email: email,
    role: w
  };
  console.log(data);
  makePost(path, data, addCampUser);
}

function addCampUser(data) {
  console.log(data);
  location.reload(true);
}

function setupModal() {
  function submitFunc() {
    var id = $(".active")
      .children(".id")
      .text();
    var email = $(".active")
      .children(".email")
      .text();

    console.log(id);
    console.log(email);
    assignUser(id, email);
    // rowClickHandler(onClickFunc);
  }

  function hideFunc() {
    $(".canvas-select-container").show();
    $(".manager-select-container").show();
  }

  function modalFilterWrapper() {
    filterFunction(".mSection1 #search-bar", ".available-user-table");
  }

  function hideWrapper() {
    hideHandler(hideFunc);
  }

  function setupModalTable() {
    //CANVASSER TAGS
    $(".available-user-table .clickable-row .canvasser").each(function() {
      if ($(this).attr("value") == "true") {
        $(this).html(
          '<h6><span class="badge badge-pill badge-warning">Canvasser</span></h6>'
        );
      } else if ($(this).attr("value") == "false") {
        $(this).text("");
      }
    });

    //MANAGER TAGS
    $(".available-user-table .clickable-row .manager").each(function() {
      if ($(this).attr("value") == "true") {
        $(this).html(
          '<h6><span class="badge badge-pill badge-info">Campaign Manager</span></h6>'
        );
      } else if ($(this).attr("value") == "false") {
        $(this).text("");
      }
    });

    rowClickHandler(modalTableHandler);
  }

  function modalTableHandler(element) {
    $(".mSection").show();
    $(".canvas-select-container").show();
    $(".manager-select-container").show();
    if (
      $(element)
        .find(".canvasser")
        .attr("value") === "false"
    ) {
      $(".canvas-select-container").hide();
    }
    if (
      $(element)
        .find(".manager")
        .attr("value") === "false"
    ) {
      $(".manager-select-container").hide();
    }
  }

  function loadModalTable() {
    function parseData2Wrapper(data) {
      console.log("look at this\n" + data);
      parseData2(data, ".available-user-table", setupModalTable);
    }

    var path = "/manager/get_users_add_campaign";
    var data = {
      campaign_id: getQueryParm("cid")
    };
    makePost(path, data, parseData2Wrapper);

    // //#########TEST JSON#################
    // $.getJSON("/test_json/campaign-manager/assign-role.json", function(json) {
    //   console.log(json);
    //   parseData2(json, ".available-user-table", setupModalTable);
    // });
    // //###################################
  }

  modalHandler("AddParticipantModal", ".dynamicModal", submitFunc, undefined);

  $(".mSection1").load(
    "/components/search-bar.html",
    undefined,
    modalFilterWrapper
  );
  $(".mSection2").load(
    "/components/modal-forms/add-campaign-user.html",
    null,
    loadModalTable
  );
  $(".mSection3").load(
    "/components/modal-forms/add-campaign-user-assign-role.html",
    undefined,
    hideWrapper
  );
}

function setupTable() {
  //ADMIN TAGS
  $(".table-container .clickable-row .admin").each(function() {
    if ($(this).attr("value") == "true") {
      console.log("hitt");
      $(this).html(
        '<h6><span class="badge badge-pill badge-success">Sys. Admin</span></h6>'
      );
    } else if ($(this).attr("value") == "false") {
      $(this).text("");
    }
  });

  //CANVASSER TAGS
  $(".table-container .clickable-row .canvasser").each(function() {
    if ($(this).attr("value") == "true") {
      $(this).html(
        '<h6><span class="badge badge-pill badge-warning">Canvasser</span></h6>'
      );
    } else if ($(this).attr("value") == "false") {
      $(this).text("");
    }
  });

  //MANAGER TAGS
  $(".table-container .clickable-row .manager").each(function() {
    if ($(this).attr("value") == "true") {
      $(this).html(
        '<h6><span class="badge badge-pill badge-info">Campaign Manager</span></h6>'
      );
    } else if ($(this).attr("value") == "false") {
      $(this).text("");
    }
  });

  $(".phone").hide();
  $(".id").hide();
  $(".search-container").load(
    "/components/search-bar.html",
    null,
    filterFunctionWrapper
  );
  rowClickHandler(onClickFunction);
}

function filterFunctionWrapper() {
  filterFunction("#search-bar", ".user-table");
}

function onClickFunction(element) {
  var data = [
    {
      firstname: $(element)
        .find(".firstname")
        .text(),
      lastname: $(element)
        .find(".lastname")
        .text(),
      title: "Lead Manager",
      phone: $(element)
        .find(".phone")
        .text(),
      email: $(element)
        .find(".email")
        .text(),
      admin: $(element)
        .find(".admin")
        .attr("value"),
      canvasser: $(element)
        .find(".canvasser")
        .attr("value"),
      manager: $(element)
        .find(".manager")
        .attr("value")
    }
  ];

  var section1 = [
    '<div class="row section-1 pt-3">',
    '<div class="col-5">',
    '<img src="https://via.placeholder.com/100" alt="Responsive image"></img>',
    "</div>",
    '<div class="col-7">',
    "<h4>",
    '<span class="firstname">',
    data[0].firstname,
    "</span>",
    " ",
    '<span class="lastname">',
    data[0].lastname,
    "</span>",
    "</h4>",
    "<h5>",
    data[0].title,
    "</h5>",
    "</div>",
    "</div>"
  ];

  $(".section-1").replaceWith($(section1.join("")));

  var section2 = [
    '<div class="row section-2">',
    '<div class="col-12">',
    "<h5>General Information</h5>",
    "<h6>Phone</h6>",
    '<span class="phone">',
    data[0].phone,
    "</span>",
    "<h6>Email</h6>",
    '<span class="email">',
    data[0].email,
    "</span>",
    "</div>",
    "</div>"
  ];

  $(".section-2").replaceWith($(section2.join("")));

  // $(".section-3").load(
  //   "/components/sidebar-sections/assign-role-section3.html"
  // );
  // $(".section-3").attr("class", "row section-3");
}
