from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.manager.blueprint import mod_manager
from app.manager.models import Manager
from app.auth.models import User

# Return list of all the campaigns that the manager is managing
@mod_manager.route('/get_managing_campaigns', methods=['POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def get_managing_campaigns():
    print("Attempting to get all campaigns this manager manages.")

    # check if manager
    cookieID = request.cookies.get('cookieID')
    if not Manager.is_manager(cookieID):
        print("Get managing campaigns failed. Only managers can perform this action")
        return make_response(jsonify({'comment': "Get managing campaigns failed. Only managers can perform this action", 'status': 403}))

    # get request
    req = request.get_json(force=True)
    print(req)

    # get manager from db
    user = User.query.filter_by(email=req['email']).first()
    if not user:
        print("Get managing campaigns failed. User not found.")
        return make_response(jsonify({'comment': "Get managing campaigns failed. User not found.", 'status': 404}) )

    manager = user.manager
    if not manager:
        print("Get managing campaigns failed. User does not have manager role.")
        return make_response(jsonify({'comment': "Get managing campaigns failed. User does not have manager role.", 'status': 403}) )

    # get managing campaigns
    campaigns = [c.serialize() for c in manager.campaigns]

    # return success
    print("Returning managing campaigns for %s: %s" % (manager, campaigns))
    resp = make_response(jsonify({
        'comment': 'Returning managing campaigns for %s.' % manager, 
        'materials': campaigns, 
        'status': 200}))
    return resp