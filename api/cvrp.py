from __future__ import print_function
from ortools.constraint_solver import pywrapcp
from ortools.constraint_solver import routing_enums_pb2
import math
###########################
# Problem Data Definition #
###########################
def create_data_model():
  """Stores the data for the problem"""
  data = {}
  # Locations in block units
  locations = [(0, 0), (40.752722, -73.987389), (40.75966, -73.99287), (40.75974, -73.97348), (40.758206, -73.999473), (40.741502, -74.008175), (40.75603, -73.98345), (40.756378, -73.976928), (40.754619, -73.990068), (40.782395, -73.959522), (40.750814, -73.983336), (40.750997, -73.973878), (40.7502, -73.999011), (40.74935, -73.9933), (40.748014, -73.989335), (40.747519, -73.980833), (40.74615,-73.99985), (40.745749, -73.976689)]
        

  demands = [0.5 for l in locations]
  demands[0] = 0

  capacities = [8 for i in range(1)]

  # Multiply coordinates in block units by the dimensions of an average city block, 114m x 80m,
  # to get location coordinates.
  data["locations"] = locations
  data["num_locations"] = len(data["locations"])
  data["num_vehicles"] = 1
  data["depot"] = 0
  data["demands"] = demands
  data["vehicle_capacities"] = capacities
  data["travel_speed"] = 5
  return data
#######################
# Problem Constraints #
#######################
def manhattan_distance(position_1, position_2):
    #TODO: NOT ACTUALLY MANHATTAN DISTANCE. Actually using great-circle distance
    lat1 = position_1[0]
    lon1 = position_1[1]
    lat2 = position_2[0]
    lon2 = position_2[1]
    R = 6371000 # earth's radius (meters)
    φ1 = math.radians(lat1)
    φ2 = math.radians(lat2)
    Δφ = math.radians((lat2-lat1))
    Δλ = math.radians((lon2-lon1))
    a = math.sin(Δφ/2) * math.sin(Δφ/2) + math.cos(φ1) * math.cos(φ2) * math.sin(Δλ/2) * math.sin(Δλ/2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    d = R * c
    return d * 0.00062137 # meters to miles

def create_distance_callback(data):
  """Creates callback to return distance between points."""
  _distances = {}

  for from_node in range(data["num_locations"]):
    _distances[from_node] = {}
    for to_node in range(data["num_locations"]):
      if from_node == data["depot"] or to_node == data["depot"] or from_node == to_node:
        _distances[from_node][to_node] = 0
      else:
        _distances[from_node][to_node] = (
            manhattan_distance(data["locations"][from_node],
                               data["locations"][to_node]))
  print(_distances)
  def distance_callback(from_node, to_node):
    """Returns the manhattan distance between the two nodes"""
    return _distances[from_node][to_node]

  return distance_callback

def create_demand_callback(data, distance_callback):
    """Creates callback to get demands at each location."""
    def demand_callback(from_node, to_node):
        travel_dist = distance_callback(from_node, to_node)
        travel_time = travel_dist/data["travel_speed"]
        return data["demands"][from_node] + travel_time
    return demand_callback

def add_capacity_constraints(routing, data, demand_callback):
    """Adds capacity constraint"""
    capacity = "Capacity"
    routing.AddDimensionWithVehicleCapacity(
        demand_callback,
        0, # null capacity slack
        data["vehicle_capacities"], # vehicle maximum capacities
        True, # start cumul to zero
        capacity)

###########
# Printer #
###########
def print_solution(data, routing, assignment):
    """Print routes on console."""
    total_dist = 0
    total_time = 0
    for vehicle_id in range(data["num_vehicles"]):
        index = routing.Start(vehicle_id)
        plan_output = 'Route for vehicle {0}:\n'.format(vehicle_id)
        route_dist = 0
        route_time = 0
        while not routing.IsEnd(index):
            node_index = routing.IndexToNode(index)
            next_node_index = routing.IndexToNode(assignment.Value(routing.NextVar(index)))
            travel_dist = 0
            if node_index == 0 or next_node_index == 0:
              travel_dist += 0
            else:
              travel_dist += manhattan_distance(
                  data["locations"][node_index],
                  data["locations"][next_node_index])
            route_dist += travel_dist
            route_time += (data["demands"][node_index] + travel_dist/data["travel_speed"])
            plan_output += ' {0} Load({1}) -> '.format(node_index, route_time)
            index = assignment.Value(routing.NextVar(index))

        node_index = routing.IndexToNode(index)
        total_dist += route_dist
        total_time += route_time
        plan_output += ' {0} Load({1})\n'.format(node_index, route_time)
        plan_output += 'Distance of the route: {0}m\n'.format(route_dist)
        plan_output += 'Load of the route: {0}\n'.format(route_time)
        print(plan_output)
    print('Total Distance of all routes: {0}m'.format(total_dist))
    print('Total Time of all routes: {0}m'.format(total_time))

########
# Main #
########
def main():
  """Entry point of the program"""
  # Instantiate the data problem.
  data = create_data_model()
  # Create Routing Model
  routing = pywrapcp.RoutingModel(
      data["num_locations"],
      data["num_vehicles"],
      data["depot"])
  # Define weight of each edge
  distance_callback = create_distance_callback(data)
  routing.SetArcCostEvaluatorOfAllVehicles(distance_callback)
# Add Capacity constraint
  demand_callback = create_demand_callback(data, distance_callback)
  add_capacity_constraints(routing, data, demand_callback)
  # Setting first solution heuristic (cheapest addition).
  search_parameters = pywrapcp.RoutingModel.DefaultSearchParameters()
  search_parameters.first_solution_strategy = (
      routing_enums_pb2.FirstSolutionStrategy.PATH_CHEAPEST_ARC)
  # Solve the problem.
  assignment = routing.SolveWithParameters(search_parameters)
  if assignment:
    print_solution(data, routing, assignment)
if __name__ == '__main__':
  main()