from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.canvasser.blueprint import mod_canvasser
from app.campaign.models import Location
from app.questionnaire.models import Response
from app.canvasser.models import Canvasser

@mod_canvasser.route('/enter_results', methods=['POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def enter_results():
    print("Canvasser enters results for a location visit.")

    # check if canvasser
    cookieID = request.cookies.get('cookieID')
    if not Canvasser.is_canvasser(cookieID):
        print("Entering location failed. Only canvassers can perform this action.")
        return make_response(jsonify({'comment': "Entering location failed. Only canvassers can perform this action.", 'status': 403}))

    # get request
    req = request.get_json(force=True)
    print(req)

    # get location from db
    location = Location.query.filter_by(id=req['location_id']).first()
    if not location:
        print("Enter results failed. Specified location not found")
        return make_response(jsonify({'status': 404, 'comment': "Enter results failed. Specified location not found"}))

    # enter results
    location.if_spoken = req['if_spoken']
    location.rating = req['rating']
    location.notes = req['notes']
    db.session.commit()

    # enter responses to campaign questions
    questions = location.campaign.questions
    for i in range(len(req['responses'])):
        question = questions[i]
        response = Response(response=req['responses'][i], location=location, question=question)
        location.responses.append(response)
        db.session.add(response)

    db.session.commit()
    location = Location.query.filter_by(id=req['location_id']).first()

    print(location)
    print("Entered results for %s" % location)
    resp = make_response(jsonify({
        "comment": "Entered results successful: %s" % str(location),
        "status": 200
    }))
    return resp