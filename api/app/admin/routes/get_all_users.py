from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.auth.models import User
from app.admin.models import Admin
from app.admin.blueprint import mod_admin

@mod_admin.route('/get_all_users', methods=['GET', 'POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def get_all_users():
    print("Attempting to get all users...")
    print(request.cookies)
    print(request.headers)
    print("---", request)
    cookieID = request.cookies.get('cookieID')
    print(cookieID)
    # check if admin
    cookieID = request.cookies.get('cookieID')
    if not Admin.is_admin(cookieID):
        print("Get all users failed. Only admin can perform this action.")
        return make_response(jsonify({'status': 403, 'comment': "Get all users failed. Only admin can perform this action."}))
    
    # query all users
    users = User.query.all()
    print(users)

    # return success
    print("Returning all users: %s" % users)
    resp = make_response(jsonify({'comment': 'Returning all users.', 'materials': [u.serialize() for u in users], 'status': 200, 'cookieID':cookieID}))
    return resp
