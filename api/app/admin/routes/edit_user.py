from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.admin.blueprint import mod_admin
from app.admin.models import Admin
from app.auth.models import User
from app.campaign.models import Campaign
from app.canvasser.models import Canvasser

# Edit an existing user
@mod_admin.route('/edit_user', methods=['POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def edit_user():
    print("Attempting to edit user...")    

    # check if admin
    cookieID = request.cookies.get('cookieID')
    if not Admin.is_admin(cookieID):
        print("Edit user failed. Only admin can perform this action.")
        return make_response(jsonify({'status': 403, 'comment': "Edit user failed. Only admin can perform this action."}))
    
    # get request
    req = request.get_json(force=True)
    print(req)

    # validate the input
    if not Admin.validate_edit_user_input(req):
        print("Edit user failed. Invalid email.")
        return make_response(jsonify({'status': 403, 'comment': "Edit user failed. Only admin can perform this action."}))

    # get user from db
    user = User.query.filter_by(email=req['email']).first()
    if not user:
        print("Edit user failed. User with email %s was not found" % (req['email']))
        return make_response(jsonify({'status': 404, 'comment': "Edit user failed. User with email %s was not found" % (req['email'])}))

    manager_role = req['manager'] if 'manager' in req else False
    canvasser_role = req['canvasser'] if 'canvasser' in req else False
    admin_role = req['admin'] if 'admin' in req else False
    user.set_roles(manager_role, canvasser_role, admin_role)

    # return success
    print("%s roles have been updated to: manager = %s, canvasser = %s, admin = %s" % (user, True if user.manager else False, True if user.canvasser else False, True if user.admin else False))

    resp = make_response(jsonify({'comment': "%s roles have been updated to: manager = %s, canvasser = %s, admin = %s" % (user, True if user.manager else False, True if user.canvasser else False, True if user.admin else False), 'status': 200}))
    return resp