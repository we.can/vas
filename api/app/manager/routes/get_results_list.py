from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.manager.blueprint import mod_manager
from app.manager.models import Manager
from app.campaign.models import Campaign
from app.campaign.models import Location
from app.questionnaire.models import Response

@mod_manager.route('/get_results_list', methods=['POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def get_results_list():
    print("Attempting to get list of locations for campaign results")

    # check if manager
    cookieID = request.cookies.get('cookieID')
    if not Manager.is_manager(cookieID):
        print("Get list of locations failed. Only managers can perform this cation")
        return make_response(jsonify({'status': 403, 'comment': "Get list of locations failed. Only managers can perform this cation"}))
        
    # get request
    req = request.get_json(force=True)
    print(req)

    # get campaign from db
    campaign = Campaign.query.filter_by(id=req['campaign_id']).first()
    if not campaign:
        print("Get list of locations failed. Specified campaign not found")
        return make_response(jsonify({'status': 404, 'comment': "Get list of locations failed. Specified campaign not found"}))

    # get locations 
    locations = campaign.locations

    # to_ret
    to_ret = []
    for l in locations:
        temp = {}
        temp['location_id'] = l.id
        temp['address'] = l.address
        responses = l.responses
        count = 0
        for r in responses:
            if r.response != None:
                count += 1
        temp['#_responses'] = str(count) + "/" + str(len(responses))
        temp['date'] = l.task.date.date
        to_ret.append(temp)

    # return success
    print("Returning list of locations for %s results: %s" % (campaign, to_ret))
    resp = make_response(jsonify({
        'comment': "Returning users for %s results" % campaign, 
        'materials': to_ret, 
        'status': 200}))
    return resp
