var URL = "http://lifeishard.fyi/api";

function displayRole(data) {
  if (data["status"] != 200) {
    $(".admin").hide();
    $(".canvasser").hide();
    $(".manager").hide();
    $(".modal-body").show();
    $(".system-down").hide();
  } else {
    $(".modal-body").hide();
    if (data["materials"]["role"]["admin"] != true) {
      $(".admin").hide();
    }
    if (data["materials"]["role"]["canvasser"] != true) {
      $(".canvasser").hide();
    }
    if (data["materials"]["role"]["manager"] != true) {
      $(".manager").hide();
    }
  }
}

function error_f(exception) {
  console.log(exception);
  $(".modal-body").show();
  $(".wrong-password").hide();
  $(".system-down").show();
  $(".modal-footer").hide();
}

function success_f(data) {
  console.log("success hit");
  if (data != null) {
    console.log(data);
    displayRole(data);
  }
}

$(document).ready(function() {
  var email;
  var password;
  // var path = URL + "/auth/login";
  var path = "/auth/login";

  $(".login-button").on("click", function() {
    password = $(".password-container").val();
    email = $(".email-textbox").val();
    var data = {
      email: email,
      password: password
    };
    console.log(data);
    makePost(path, data, success_f, error_f);
    setTimeout(2000);
  });

  $("#role-selection").on("hidden.bs.modal", function() {
    $(".modal-body").hide();
    $(".system-down").hide();
    $(".admin").show();
    $(".canvasser").show();
    $(".manager").show();
  });
});
