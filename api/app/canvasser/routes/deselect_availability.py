from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.canvasser.blueprint import mod_canvasser
from app.auth.models import User
from app.canvasser.models import Canvasser
from app.canvasser.models import Date
from app.campaign.models import Campaign
     
@mod_canvasser.route('/deselect_availability', methods=['POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def deselect_availability():
    print("Canvasser deselects a date for availability.")

    # check if canvasser
    cookieID = request.cookies.get('cookieID')
    if not Canvasser.is_canvasser(cookieID):
        print("Deselect availability failed. Only canvassers can perform this action.")
        return make_response(jsonify({'comment': "Deselect availability failed. Only canvassers can perform this action.", 'status': 403}))

    # get request
    req = request.get_json(force=True)
    print(req)
    if "dates" not in req:
        return make_response(jsonify({'comment': "Invalid Data format", 'status': 404}))

    # get canvasser from db
    user = User.query.filter_by(cookie_id=cookieID).first()
    if not user:
        print("Deselect availability failed. User not found.")
        return make_response(jsonify({'comment': "Deselect availability failed. User not found.", 'status': 404}))
    canvasser = user.canvasser

    # dates to delete and affected campaigns
    campaigns_affected = []
    date_obj_to_del = []
    for date_obj in canvasser.dates:

        # add date to delete 
        if date_obj.date in req['dates']:
            date_obj_to_del.append(date_obj)

            # add to affected campaigns
            if date_obj.task and date_obj.task.campaign not in campaigns_affected:
                campaigns_affected.append(date_obj.task.campaign)

 
    # delete date and associated task
    for date_obj in date_obj_to_del:
        if date_obj.task:
            db.session.delete(date_obj.task)
        db.session.delete(date_obj)

    # regenerate assignments for all campaigns affected
    for c in campaigns_affected:
        Campaign.generate_assignments(c)

    # commit to db
    db.session.commit()

    # return success
    print("The specified dates have been deselected.")
    return make_response(jsonify({'comment': "The specified dates have been deselected", 'status': 200}))

