$(document).ready(function() {
  // $(".sidebar-container").append(createSideBar());
  // rowClickHandler(onClickFunction);
});

//Creates a template for future data to be added to the side nav.
function createSideBar(setupFunc) {
  // var sidebar = [
  //   '<div class="container">',
  //   '<div class="section-1">',
  //   "</div>",
  //   '<div class="section-2">',
  //   "</div>",
  //   '<div class="section-3">',
  //   "</div>",
  //   "</div>"
  // ];

  // return $(sidebar.join(""));
  $(".sidebar-container").load("/components/sidebar.html", null, setupFunc);
}

//Append HTML to a section (NOT LOAD)
//@htmlContent: html to load
//@section: section class to load to
//@showFunc: optional function to run when appended
function appendSectionHTML(htmlContent, section, setupFunc) {
  var object = $("<div/>")
    .html(htmlContent)
    .contents();
  $(section).append(object);

  //If setup function isn't undefined run
  if (setupFunc != undefined) {
    setupFunc();
  }
}
