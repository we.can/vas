from app import db

class Global_Parameter(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    value = db.Column(db.Float)

    def __repr__(self):
        return "Global_Parameter(%s, %s)" % (self.name, self.value)

    def serialize(self):
        return {
            'name': self.name,
            'value': self.value
        }