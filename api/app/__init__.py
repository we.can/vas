__author__ = 'Manjur Khan'
__project__ = 'Canvass'
__credits__ = ['Manjur Khan']

# Import flask and template operators
from flask import Flask, jsonify, make_response

# Import the other stuff
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from flask_bcrypt import Bcrypt

# Define the WSGI application object
app = Flask(__name__)

# Configurations
app.config.from_object('config')

# Define the database object which is imported by modules and routes
db = SQLAlchemy(app)

# Define the CORS object
cors = CORS(app)

# Define password encryption object
bcrypt = Bcrypt(app)

# Import a module / component using its blueprint handler variable (mod_auth)
from app.auth.blueprint import mod_auth as auth_module
from app.auth.routes import login
from app.auth.routes import logout

# Import a module / component using its blueprint handler variable (mod_manager)
from app.manager.blueprint import mod_manager as manager_module
from app.manager.routes import add_campaign_user
from app.manager.routes import add_locations
from app.manager.routes import add_questions
from app.manager.routes import create_campaign
from app.manager.routes import generate_assignments
from app.manager.routes import get_campaign_users
from app.manager.routes import get_managing_campaigns
from app.manager.routes import unassigned_locations
from app.manager.routes import view_assignment_selected
from app.manager.routes import view_assignments
from app.manager.routes import get_results_list
from app.manager.routes import get_result_selected
from app.manager.routes import get_stat_summary
from app.manager.routes import get_results_map
from app.manager.routes import get_questions
from app.manager.routes import get_users_add_campaign
from app.manager.routes import edit_dates
from app.manager.routes import edit_visit_duration
from app.manager.routes import add_talking_points
from app.manager.routes import get_talking_points

# Import a module / component using its blueprint handler variable (mod_canvasser)
from app.canvasser.blueprint import mod_canvasser as canvasser_module
from app.canvasser.routes import view_assignment_selected
from app.canvasser.routes import view_assignment
from app.canvasser.routes import view_assignments
from app.canvasser.routes import select_availability
from app.canvasser.routes import deselect_availability
from app.canvasser.routes import get_availability
from app.canvasser.routes import enter_results
from app.canvasser.routes import get_questions

# Import a module / component using its blueprint handler variable (mod_admin)
from app.admin.blueprint import mod_admin as admin_module
from app.admin.routes import register
from app.admin.routes import get_all_users
from app.admin.routes import edit_user
from app.admin.routes import get_global_parameters
from app.admin.routes import edit_global_parameters

# Import a module / component using its blueprint handler variable (mod_campaign)
from app.campaign.blueprint import mod_campaign as campaign_module
from app.campaign.models import Campaign
from app.campaign.models import Task
from app.campaign.models import Location
from app.campaign.models import Talking_Point

# Import a module / component using its blueprint handler variable (mod_campaign)
from app.questionnaire.blueprint import mod_questionnaire as questionnaire_module
from app.questionnaire.models import Question
from app.questionnaire.models import Response


# Register blueprint(s)
app.register_blueprint(auth_module)
app.register_blueprint(manager_module)
app.register_blueprint(canvasser_module)
app.register_blueprint(admin_module)
app.register_blueprint(campaign_module)
app.register_blueprint(questionnaire_module)
# app.register_blueprint(xyz_module)
# ..

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify("url not found"))

@app.after_request # blueprint can also be app~~
def after_request(response):
    header = response.headers
    header['Access-Control-Allow-Origin'] = '*'
    return response

# Create tables
db.create_all()
