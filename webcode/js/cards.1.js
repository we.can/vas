var cards = $();

// var data = [
//   { Title: "Peter", Desc: "Programmer" },
//   { Title: "John", Desc: "Programmer" },
//   { Title: "Kevin", Desc: "Scientist" },
//   { Title: "Kevin", Desc: "Scientist" },
//   { Title: "Kevin", Desc: "Scientist" },
//   { Title: "Kevin", Desc: "Scientist" }
// ];

// data.forEach(function(item, i) {
//   cards = cards.add(createCard(item));
// });

$(document).ready(function() {
  var c = document.cookie.split(";");
  console.log(c);
  var em = "";
  c.forEach(function(item) {
    if (~item.indexOf("email")) {
      em = item.split('"')[1];
      console.log("we have email");
      console.log(em);
    }
  });
  // ######################### AJAX THIS MOFO
  var path = "/manager/get_managing_campaigns";
  var data = {
    email: em
  };
  console.log("im going in");
  makePost(path, data, loadCard);
  // $(".card-container").append(cards);
});

function loadCard(data) {
  console.log(data);
  $.get(
    "/components/card.html",
    function(my_var) {
      var ele = my_var;
      console.log(ele);
      var object = $("<div/>")
        .html(ele)
        .contents();

      object.find(".card").attr("data-toggle", "modal");
      object.find(".card").attr("data-target", "#addModal");
      object.find(".card").attr("data-title", "Add Campaign");
      object
        .find(".campaign-title")
        .append('<i class="fas fa-plus-circle fa-5x"/>');
      object.find(".campaign-title").addClass("text-center");
      object.find(".dates").remove();
      object.find(".card-link").remove();
      $(".card-container").append(object[0]);
      $(".modal-container").load("/components/modal.html", null, setupModal);
      $(".edit-modal").load("/components/modal.html", null, setupModal);
    },
    "html"
  );

  //GENERATES A CARD FOR EACH CAMPAIGN RETURNED

  data["materials"].forEach(function(item, i) {
    $.get(
      "/components/card.html",
      function(my_var) {
        var ele = my_var;
        console.log(ele);
        var object = $("<div/>")
          .html(ele)
          .contents();
        object.find(".campaign-title").text(item["title"]);
        object.find(".start").text("Start: " + item["startDate"]);
        object.find(".end").text("End: " + item["endDate"]);
        object
          .find(".view-camp")
          .attr(
            "href",
            "/campaign-manager/manage-canvassers/view-assignments.html?cid=" +
              item["id"] +
              "&startDate=" +
              item["startDate"]
          );

        $(".card-container").append(object[0]);
      },
      "html"
    );
  });
}

// function setupModal2(){
//   $.get(
//     "/components/modal-forms/edit-campaign-info.html",
//     function(my_var) {
//       var ele = my_var;
//       console.log(ele);
//       var object = $("<div/>")
//         .html(ele)
//         .contents();
//       appendSectionHTML(object, ".mSection1");
//       modalHandler("editModal", ".dynamicModal", submitFunct);
//     },
//     "html"
//   );
// }

// function submitFunct() {
//   // var data = {
//   //   title: $("#campaign-name").val(),
//   //   startDate: $("#start-date").val(),
//   //   endDate: $("#end-date").val(),
//   //   visit_duration: parseInt($("#visit_duration").val())
//   // };
//   var path = "/manager/create_campaign";
//   // makePost(path, data, addCampaignFunc);
// }

function setupModal() {
  $.get(
    "/components/modal-forms/add-campaign.html",
    function(my_var) {
      var ele = my_var;
      console.log(ele);
      var object = $("<div/>")
        .html(ele)
        .contents();
      appendSectionHTML(object, ".mSection1");
      modalHandler("addModal", ".dynamicModal", submitFunc);
    },
    "html"
  );
}

function submitFunc() {
  var data = {
    title: $("#campaign-name").val(),
    startDate: $("#start-date").val(),
    endDate: $("#end-date").val(),
    visit_duration: parseInt($("#visit_duration").val())
  };
  var path = "/manager/create_campaign";
  makePost(path, data, addCampaignFunc);
}

function addCampaignFunc(data) {
  console.log(data);
  alert(data["materials"]);
  location.reload(true);
}
