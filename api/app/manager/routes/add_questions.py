from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.manager.blueprint import mod_manager
from app.campaign.models import Campaign
from app.manager.models import Manager
from app.questionnaire.models import Question
import datetime as dt

@mod_manager.route('/add_questions', methods=['POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def add_questions():
    print("Attempting to add question to campaign.")

    # check if manager
    cookieID = request.cookies.get('cookieID')
    if not Manager.is_manager(cookieID):
        print("Add question to campaign failed. Only managers can perform this action.")
        return make_response(jsonify({'comment': "Add question to campaign failed. Only managers can perform this action.", 'status': 403}))

    # get request
    req = request.get_json(force=True)
    print(req)

    # get campaign from db
    campaign = Campaign.query.filter_by(id=req['campaign_id']).first()
    if not campaign:
        print("Add question failed. Campaign not found.")
        return make_response(jsonify({'comment': "Add question failed. Campaign not found.", 'status': 404}))

    # if campaign started, cannot make edits
    today_str = str(dt.date.today())
    if today_str >= campaign.startDate:
        print("Add questions failed. Campaign already started.")
        return make_response(jsonify({
            'comment': "Add questions failed. Campaign already started", 
            'status': 400,
            'locked': True}
        ))

    # add question to db
    for q in req['questions']:
        question = Question(question=q, campaign=campaign)
        db.session.add(question)

    db.session.commit()

    # return success
    print("Question(s) has/have been added to campaign.")
    resp = make_response(jsonify({
        'comment': "Question(s) has/have been added to campaign.", 
        'status': 200,
        'locked': False}))
    return resp