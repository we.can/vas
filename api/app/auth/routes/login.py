from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db, bcrypt
from app.auth.models import User
from app.auth.blueprint import mod_auth
from uuid import uuid4

# Set the route and accepted methods
@mod_auth.route('/login', methods=['POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def login():
    print("Attempting to login.")
    # Remove the currnet cookied user
    print(request.cookies)
    cookieID = request.cookies.get('cookieID')
    print(cookieID)
    if (cookieID is not None) and (cookieID is not ""):
        user = User.query.filter_by(cookie_id=cookieID).first()
        if user:
            user.cookie_id = None
            db.session.commit()
    
    # get request
    req = request.get_json(force=True)
    print(req)

    # get user from db
    cookieID = uuid4().hex
    user = User.query.filter_by(email=req['email']).first()
    print(user)

    # check password match
    if not user or not bcrypt.check_password_hash(user.password, req['password']):
        print("Login failed. Email does not exist or password does not match.")
        resp = make_response(jsonify({'status': 401, 'comment': "Login failed. Email does not exist or password does not match."}))
        
        resp.set_cookie('cookieID', "", expires=0)
        resp.set_cookie('name', "", expires=0)
        resp.set_cookie('email', "", expires=0)
        resp.set_cookie('manager', "", expires=0)
        resp.set_cookie('admin', "", expires=0)
        resp.set_cookie('canvasser', "", expires=0)

        return resp

    # check roles
    if not (user.canvasser or user.manager or user.admin):
        print("%s does not have a set role. Must have a role to be able to log in" % (user.email))
        resp = make_response(jsonify({'status': 403, 'comment': "%s does not have a set role. Must have a role to be able to log in" % (user.email)}))
        
        resp.set_cookie('cookieID', "", expires=0)
        resp.set_cookie('name', "", expires=0)
        resp.set_cookie('email', "", expires=0)
        resp.set_cookie('manager', "", expires=0)
        resp.set_cookie('admin', "", expires=0)
        resp.set_cookie('canvasser', "", expires=0)

        return resp

    # return success
    print("Login successful.")
    user.cookie_id = cookieID
    resp = make_response(jsonify({'comment': 'Login successful.', 'materials': user.serialize(), 'status': 200, 'cookieID':user.cookie_id}))
    
    resp.set_cookie('cookieID', user.cookie_id)
    resp.set_cookie('name', ("%s %s" % (user.first_name, user.last_name)))
    resp.set_cookie('email', user.email)
    resp.set_cookie('manager', "True" if user.manager else "False")
    resp.set_cookie('admin', "True" if user.admin else "False")
    resp.set_cookie('canvasser', "True" if user.canvasser else "False")
    
    # add the cookie to the db
    db.session.commit()
    
    return resp
