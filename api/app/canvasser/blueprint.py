from flask import Blueprint

# Define the blueprint: 'canvasser', set its url prefix: app.url/canvasser
mod_canvasser = Blueprint('canvasser', __name__, url_prefix='/api/canvasser')
 