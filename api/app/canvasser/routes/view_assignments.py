from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.canvasser.blueprint import mod_canvasser
import datetime as dt
from app.canvasser.models import Canvasser
from app.auth.models import User
from app.campaign.models import Task

@mod_canvasser.route('/view_assignments', methods=['POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def view_assignments():
    print("Attempting to view all upcoming assignments for a given canvasser.")

    # check if canvasser
    cookieID = request.cookies.get('cookieID')
    if not Canvasser.is_canvasser(cookieID):
        print("View all upcoming assignments failed. Only canvassers can perform this action.")
        return make_response(jsonify({'comment': "View all upcoming assignments failed. Only canvassers can perform this action.", 'status': 403}))

    # get request
    req = request.get_json(force=True)
    print(req)

    # get canvasser from db
    user = User.query.filter_by(cookie_id=cookieID).first()
    if not user:
        print("View today's assignment failed. User not found.")
        return make_response(jsonify({'comment': "View today's assignment. User not found.", 'status': 404}) )

    # check if canvasser
    canvasser = user.canvasser
    if not canvasser:
        print("View all upcoming assignments failed. User does not have canvasser role.")
        return make_response(jsonify({'comment': "View all upcoming assignments failed. User does not have canvasser role.", 'status': 403}) )

    # get all assignments (tasks) after today
    today = dt.date.today() # date object with today's date
    
    today = str(dt.date.today()) # date object with today's date
    tmp_tasks = Task.query.filter_by(canvasser=canvasser).all()
    tasks = []
    for t in tmp_tasks:
        if t.date.date > today:
            tasks.append(t)
    # format to_ret
    to_ret = []
    for t in tasks:
        temp = {}
        temp['task_id'] = t.id
        temp['date'] = t.date.date
        to_ret.append(temp)

    # return success
    print("Returning list of upcoming assignments (tasks) for %s: %s" % (canvasser, to_ret))
    resp = make_response(jsonify({
        'comment': "Returning list of upcoming assignments (tasks) for %s" % canvasser, 
        'materials': to_ret, 
        'status': 200}))
    return resp