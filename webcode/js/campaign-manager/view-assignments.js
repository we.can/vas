$(document).ready(function() {
  var path = "/manager/view_assignments";
  var data = {
    campaign_id: getQueryParm("cid")
  };

  makePost(path, data, parseData);
  //#########TEST JSON#################
  // $.getJSON("/test_json/campaign-manager/view-assignments.json", function(
  //   json
  // ) {
  //   console.log(json);
  //   parseData(json);
  //   // var markers = setupTable();

  //   // setupMap(markers);
  // });
  // //###################################
  createSideBar(setupSideBar);
  $(".search-container").load(
    "/components/search-bar.html",
    undefined,
    filterFunctionWrapper
  );
});

function filterFunctionWrapper() {
  filterFunction("#search-bar", ".assignment-table");
}

//Callback for table row click handler
function setupTable() {
  rowClickHandler(loadSideTableData);
}

function setupSideBar() {
  //$(".section-1").prop("id", "map");
  $(".section-1").html("<div id='map'></div>");
  $(".section-2").load("/components/table.html", null, setupSection2);
  // makePost(path, data, parseData);
}

//Setup section 2 by loading the table
function setupSection2(element) {
  $(".section-2").hide();
}

//Makes ajax call to load side table data
function loadSideTableData(element) {
  function parseData2Wrapper(data) {
    parseData2(data, ".locations", setupSideTable);
  }
  var path = "/manager/view_assignment_selected";
  var data = {
    task_id: $(element)
      .find(".task_id")
      .text()
  };

  makePost(path, data, parseData2Wrapper);
  // //#########TEST JSON#################
  // $.getJSON("/test_json/unassigned-locations.json", function(json) {
  //   console.log(json);
  //   parseData2(json, ".locations", setupSideTable);
  // });
  // //###################################
}

//Hides certain values in the table and setupMap
function setupSideTable() {
  var longCoord = [];
  var latCoord = [];

  //Arry of markers for map
  var markers = [];

  //LONGTITDUDE TAGS
  $(".locations .clickable-row .longitude").each(function() {
    longCoord.push($(this).attr("value"));
    $(this).css("display", "none");
  });

  //LATTIDUDE TAGS
  $(".locations .clickable-row .latitude").each(function() {
    latCoord.push($(this).attr("value"));
    $(this).css("display", "none");
  });

  //Combines the coordinates into markers
  for (i = 0; i < longCoord.length; i++) {
    var mark = new L.marker(L.latLng([longCoord[i], latCoord[i]]));
    markers.push(mark);
  }

  setupMap(markers);
}
