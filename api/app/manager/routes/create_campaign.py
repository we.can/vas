from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.manager.blueprint import mod_manager
from app.manager.models import Manager
from app.campaign.models import Campaign
from app.auth.models import User

@mod_manager.route('/create_campaign', methods=['POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def create_campaign():
    print("Attempting to create campaign.")

    # check if manager
    cookieID = request.cookies.get('cookieID')
    if not Manager.is_manager(cookieID):
        print("Create campaign failed. Only managers can perform this action.")
        return make_response(jsonify({'comment': "Create campaign failed. Only managers can perform this action.", 'status': 403}))

    # get request
    req = request.get_json(force=True)
    print(req)

    # validate the input
    if not Campaign.validate_create_campaign_input(req):
        print("Create campaign failed. Invalid input.")
        return make_response(jsonify({'comment': "Create campaign failed. Invalid input.", 'status': 400}))

    # create campaign
    campaign = Campaign.from_json(req)

    # add manager to campaign
    email = request.cookies.get('email')
    user = User.query.filter_by(email=email).first()
    if not user:
        print("Create campaign successful, but cannot add current user to campaign. Check cookies.")
        return make_response(jsonify({'comment': "Create campaign successful, but cannot add current user to campaign. Check cookies", 'status': 404}))
    campaign.managers.append(user.manager)

    # commit to db
    db.session.add(campaign)
    db.session.commit()

    # return success
    print("%s has been created." % campaign)
    resp = make_response(jsonify({
        'comment': "%s has been created." % campaign, 
        'status': 200}))
    return resp