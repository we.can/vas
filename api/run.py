__author__ = 'Manjur Khan'
__project__ = 'Canvass'
__credits__ = ['Manjur Khan']

from app import app
from config import DEBUG

app.run(host='0.0.0.0', port=8080, debug=DEBUG)