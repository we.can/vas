__author__ = 'Manjur Khan'
__project__ = 'Canvass'
__credits__ = ['Manjur Khan']

from requests import Session, Request
from requests_toolbelt.utils import dump
from uuid import uuid4
import json
import random
import inspect


class Test():

    def __init__(self, url):
        self.__url = url
        self.__users = []
        self.__tests = {}
        self.__totalTests = 0
        self.__totalPassed = 0
        self.setVervose()
    
    def setVervose(self, verbose=False):
        self.__verbose = verbose

    def createUsers(self):
        for i in range(5):
            self.users.append(self.generateUser())


    def runAllTests(self):
        self.testLogin()
        self.testAdminPriviledgeRegister()
        self.testAdminPriviledgeEdit()
        self.testAdminPriviledgeEditGlobalParams()
        self.testManagerPrivCreateCampaign()
        self.testManagerPrivAddCampaignUser()
        self.testManagerPrivAddLocation()

    def testLogin(self):
        funcName = inspect.stack()[0][3]
        funcReason = "Tests to see if the login works correctly for all the cases given"
        funcResult = {}
        funcResult['totalTest'] = 0
        funcResult['passedTest'] = 0
        funcResult['testReason'] = funcReason
        funcResult['individualTest'] = []

        paths = {
            'login': self.url + '/auth/login'
        }
        self.print('## Testing Url Path\n```code\n%s\n```\n' % (paths['login']))
        data = {
            'valid_admin': {'email': 'john.doe@gmail.com', 'password': 'password'},
            'valid_manager': {'email': 'manjur.khan@stonybrook.edu', 'password': 'secret'},
            'invalid_user': {'email': 'manjur@gmail.com', 'password': 'hi'},
            'valid_email_but_invalid_password': {'email': 'manjur.khan@stonybrook.edu', 'password': 'secrett'},
            'invalid_arguments': {'email': 'manjur.khan@stonybrook.edu', 'password': 'secret'}
        }

        data_test_reason = {
            'valid_admin': 'Testing to see if the User is set correctly and that the user can login properly with the correct access level',
            'valid_manager': 'Testing to see if the User is set correctly and that the user can login properly with the correct access level',
            'invalid_user': 'Testing to see of the user is being blocked and the correct error code is returned',
            'valid_email_but_invalid_password': 'Testing to see of the user is being blocked and the correct error code is returned',
            'invalid_arguments': 'Testing to see that if there is invalid arguments then the server returns bad reuqests'
        }

        data_test_name = {
            'valid_admin': 'Valid Admin',
            'valid_manager': 'Valid Manager',
            'invalid_user': 'Invalid User',
            'valid_email_but_invalid_password': 'Valid Email, But Invalid Password',
            'invalid_arguments': 'Invalid Arguments'
        }

        data_expectation = {
            'valid_admin': {
                'reason': 'The person is an admin, so we expect the return value to have user\'s first name, last name, email, and admin role to be true. Additionally a status code of 200 should be returned',
                'status': 200},
            'valid_manager': {
                'reason': 'The person is an admin, so we expect the return value to have user\'s first name, last name, email, and manager role to be true. Additionally a status code of 200 should be returned',
                'status': 200},
            'invalid_user': {'reason': 'Should return status code of 401 shoule be returned for invalid user',
                            'status': 401},
            'valid_email_but_invalid_password': {
                'reason': 'Should return status code of 401 shoule be returned for valid email but invalid password',
                'status': 401},
            'invalid_arguments': {'reason': 'Should return status code of 400 for bad requests', 'status': 400}
        }

        # data = {"firstname":"Manjur","lastname":"Khan","email":"daniel.li.2@stonybrook.edu","phone":"6463772357","password":"lifeishard","admin":True,"workday_duration":5,"travel_speed":3}

        for dt in data:
            funcResult['totalTest']+= 1 # adding to the test count
            testInfo = {}
            testInfo['passed'] = False
            testInfo['reason'] = data_test_reason[dt]
            testInfo['comment'] = ""
            self.print('## Test Name: %s\n' % (data_test_name[dt]))
            self.print('### Test Reason\n%s\n' % (data_test_reason[dt]))
            self.print('#### Data Expectations\n%s\n' % (data_expectation[dt]['reason']))

            self.print('#### Data Sent\n```json\n%s\n```\n' % (str(data[dt])))

            s = Session()
            resp = s.post(paths['login'], json=data[dt])
            # print(resp.text)
            js = json.loads(resp.text)
            self.print('#### Data Recieved\n```json\n%s\n```\n' % str(js))
            self.print('#### Output Result\n')
            if js['status'] == data_expectation[dt]['status']:
                self.print('- Status code matches the expected status code of %d' % (data_expectation[dt]['status']))
                if js['status'] == 200 and dt == 'valid_admin':
                    if js['materials']['role']['admin']:
                        testInfo['comment'] = '- The user was supposed to have admin privilege and this user has admin privilege'
                        testInfo['passed'] = True
                    else:
                        testInfo['passed'] = False
                        testInfo['comment'] = '- The user was supposed to have admin privilege and this user does not admin privilege that means the privilate might have been changed, or something went wrong in the code.'
                elif js['status'] == 200 and dt == 'valid_manager':
                    if js['materials']['role']['manager']:
                        testInfo['comment'] = '- The user was supposed to have manager privilege and this user has manager privilege'
                        testInfo['passed'] = True
                    else:
                        testInfo['passed'] = False
                        testInfo['comment'] = '- The user was supposed to have manager privilege and this user does not manager privilege that means the privilate might have been changed, or something went wrong in the code.'
            else:
                testInfo['comment'] = '- Status code does not match. Expected code %d, but got code %d\n- Reason for this might be because we made some mistake in our server while writing the code. Something changed in server that we forgot to check' % (
                    data_expectation[dt]['status'], js['status'])
                testInfo['passed'] = False
            self.print(testInfo['comment'])
            d = dump.dump_response(resp)
            self.print('\n### Raw Response\n')
            self.print('```')
            self.print(d.decode())
            self.print('```')
            
            if testInfo['passed']: funcResult['passedTest']+=1
            funcResult['individualTest'].append(testInfo)
        print(self.__totalTests, funcResult)
        self.tests[funcName] = funcResult
        self.__totalTests += funcResult['totalTest']
        self.__totalPassed += funcResult['passedTest']

    def testAdminPriviledgeRegister(self):
        funcName = inspect.stack()[0][3]
        funcReason = "Tests to see if only Admin can register"
        funcResult = {}
        funcResult['totalTest'] = 0
        funcResult['passedTest'] = 0
        funcResult['testReason'] = funcReason
        funcResult['individualTest'] = []

        paths = {
            'login': self.url + '/auth/login',
            'register': self.url + '/admin/register',
            'edit_user': self.url + '/admin/edit_user'
        }
        self.print('## Testing Url Path\n```code\n%s\n```\n' % (paths['register']))
        login = {
            'valid_admin': {'email': 'daniel.li.2@stonybrook.edu', 'password': 'lifeishard'},
            'valid_manager': {'email': 'manjur.khan@stonybrook.edu', 'password': 'secret'}
        }

        data = {
            'valid_admin': {"firstname":"Joon","lastname":"Lee","email":"joon.lee@stonybrook.edu","phone":"6461236789","password":"secret"},
            'valid_manager': {"firstname":"Andrew","lastname":"AuYoung","email":"andrew.auyoung@stonybrook.edu","phone":"6463772357","password":"secret"}
        }

        data_test_reason = {
            'valid_admin': 'Testing to see that only admin can create new users',
            'valid_manager': 'Testing to see that even though the user has some privilage, they cannot create another user'
        }

        data_expectation = {
            'valid_admin': {'reason':'Expect code 201 for success on creating the user', 'status':201},
            'valid_manager': {'reason':'Expect code 403 for invalid permission to create user', 'status':403}
        }

        data_test_name = {
            'valid_admin': 'Valid Admin Creating User',
            'valid_manager': 'Valid Manager Creating User'
        }

        for dt in data:
            funcResult['totalTest']+= 1 # adding to the test count
            testInfo = {}
            testInfo['passed'] = False
            testInfo['reason'] = data_test_reason[dt]
            testInfo['comment'] = ""

            s = Session()
            self.print('## Test Name: %s\n' % (data_test_name[dt]))
            self.print('### Test Reason\n%s\n' % (data_test_reason[dt]))
            self.print('#### Data Expectations\n%s\n' % (data_expectation[dt]['reason']))

            self.print('#### Data Prerequisite: Perform Login\n')

            s.post(paths['login'], json=login[dt])

            self.print('#### Now Attempt To Create User\n')

            self.print('#### Data Sent\n```json\n%s\n```\n' % (str(data[dt])))


            resp = s.post(paths['register'], json=data[dt])
            # print(resp.text)
            js = json.loads(resp.text)
            self.print('#### Data Recieved\n```json\n%s\n```\n' % str(js))
            self.print('#### Output Result\n')
            if js['status'] == data_expectation[dt]['status']:
                testInfo['passed'] = True
                testInfo['comment'] = '- Status code matches the expected status code of %d' % (data_expectation[dt]['status'])
            elif js['status'] == 409:
                testInfo['passed'] = True
                testInfo['comment'] = ' - User already exists in system. Cannot create another user with the same email. Status code 409 expected'
            else:
                testInfo['passed'] = False
                testInfo['comment'] = '- Status code does not match. Expected code %d, but got code %d\n- Reason for this might be because we made some mistake in our server while writing the code. Something changed in server that we forgot to check' % (
                    data_expectation[dt]['status'], js['status'])

            d = dump.dump_response(resp)

            self.print(testInfo['comment'])

            self.print('\n### Raw Response\n')
            self.print('```')
            self.print(d.decode())
            self.print('```')

            if testInfo['passed']: funcResult['passedTest']+=1
            funcResult['individualTest'].append(testInfo)
        
        self.tests[funcName] = funcResult
        self.__totalTests += funcResult['totalTest']
        self.__totalPassed += funcResult['passedTest']


    def testAdminPriviledgeEdit():
        funcName = inspect.stack()[0][3]
        funcReason = "Tests to see if only Admin can edit users"
        funcResult = {}
        funcResult['totalTest'] = 0
        funcResult['passedTest'] = 0
        funcResult['testReason'] = funcReason
        funcResult['individualTest'] = []

        paths = {
            'login': self.url + '/auth/login',
            'edit_user': self.url + '/admin/edit_user'
        }
        self.print('## Testing Url Path\n```code\n%s\n```\n' % (paths['edit_user']))
        login = {
            'valid_admin': {'email': 'daniel.li.2@stonybrook.edu', 'password': 'lifeishard'},
            'valid_manager': {'email': 'manjur.khan@stonybrook.edu', 'password': 'secret'}
        }

        data = {
            'valid_admin': {"email":"joon.lee@stonybrook.edu","canvasser":True},
            'valid_manager': {"email":"joon.lee@stonybrook.edu","manager":True}
        }

        data_test_reason = {
            'valid_admin': 'Testing to see that only admin can edit users',
            'valid_manager': 'Testing to see that even though the user has some privilage, they cannot edit user'
        }

        data_expectation = {
            'valid_admin': {'reason':'Expect code 200 for success on editing the user', 'status':200},
            'valid_manager': {'reason':'Expect code 403 for invalid permission to edit user', 'status':403}
        }

        data_test_name = {
            'valid_admin': 'Valid Admin Edit User Role',
            'valid_manager': 'Valid Manager Edit User Role'
        }

        for dt in data:
            funcResult['totalTest']+= 1 # adding to the test count
            testInfo = {}
            testInfo['passed'] = False
            testInfo['reason'] = data_test_reason[dt]
            testInfo['comment'] = ""

            s = Session()
            self.print('## Test Name: %s\n' % (data_test_name[dt]))
            self.print('### Test Reason\n%s\n' % (data_test_reason[dt]))
            self.print('#### Data Expectations\n%s\n' % (data_expectation[dt]['reason']))

            self.print('#### Data Prerequisite: Perform Login\n')

            s.post(paths['login'], json=login[dt])

            self.print('#### Now Attempt To Edit User Role\n')

            self.print('#### Data Sent\n```json\n%s\n```\n' % (str(data[dt])))


            resp = s.post(paths['edit_user'], json=data[dt])
            # self.print(resp.text)
            js = json.loads(resp.text)
            self.print('#### Data Recieved\n```json\n%s\n```\n' % str(js))
            self.print('#### Output Result\n')
            if js['status'] == data_expectation[dt]['status']:
                testInfo['passed'] = True
                testInfo['comment'] = '- Status code matches the expected status code of %d' % (data_expectation[dt]['status'])
            elif js['status'] == 404:
                testInfo['passed'] = True
                testInfo['comment'] = ' - User is not found in system. Expected outcome 404'
            else:
                testInfo['passed'] = False
                testInfo['comment'] = '- Status code does not match. Expected code %d, but got code %d\n- Reason for this might be because we made some mistake in our server while writing the code. Something changed in server that we forgot to check' % (
                    data_expectation[dt]['status'], js['status'])

            d = dump.dump_response(resp)

            self.print(testInfo['comment'])

            self.print('\n### Raw Response\n')
            self.print('```')
            self.print(d.decode())
            self.print('```')

            if testInfo['passed']: funcResult['passedTest']+=1
            funcResult['individualTest'].append(testInfo)
        
        self.tests[funcName] = funcResult
        self.__totalTests += funcResult['totalTest']
        self.__totalPassed += funcResult['passedTest']

    def testAdminPriviledgeEditGlobalParams(self):
        funcName = inspect.stack()[0][3]
        funcReason = "Tests to see if only Admin can edit global parameters"
        funcResult = {}
        funcResult['totalTest'] = 0
        funcResult['passedTest'] = 0
        funcResult['testReason'] = funcReason
        funcResult['individualTest'] = []

        paths = {
            'login': self.url + '/auth/login',
            'global_parameter': self.url + '/admin/edit_global_parameters'
        }
        self.print('## Testing Url Path\n```code\n%s\n```\n' % (paths['global_parameter']))
        login = {
            'valid_admin': {'email': 'daniel.li.2@stonybrook.edu', 'password': 'lifeishard'},
            'valid_manager': {'email': 'manjur.khan@stonybrook.edu', 'password': 'secret'}
        }

        data = {
            'valid_admin': {"workday_duration":5, "travel_speed":10},
            'valid_manager': {"workday_duration":10, "travel_speed":5}
        }

        data_test_reason = {
            'valid_admin': 'Testing to see that only admin can edit global parameter',
            'valid_manager': 'Testing to see that even though the user has some privilage, they cannot edit global parameter'
        }

        data_expectation = {
            'valid_admin': {'reason':'Expect code 200 for success on editing the global parameter', 'status':200},
            'valid_manager': {'reason':'Expect code 403 for invalid permission to edit global parameter', 'status':403}
        }

        data_test_name = {
            'valid_admin': 'Valid Admin Edit Global Parameter',
            'valid_manager': 'Valid Manager Edit Global Parameter'
        }

        for dt in data:

            funcResult['totalTest']+= 1 # adding to the test count
            testInfo = {}
            testInfo['passed'] = False
            testInfo['reason'] = data_test_reason[dt]
            testInfo['comment'] = ""

            s = Session()
            self.print('## Test Name: %s\n' % (data_test_name[dt]))
            self.print('### Test Reason\n%s\n' % (data_test_reason[dt]))
            self.print('#### Data Expectations\n%s\n' % (data_expectation[dt]['reason']))

            self.print('#### Data Prerequisite: Perform Login\n')

            s.post(paths['login'], json=login[dt])

            self.print('#### Now Attempt To Edit Global Parameters\n')

            self.print('#### Data Sent\n```json\n%s\n```\n' % (str(data[dt])))


            resp = s.post(paths['global_parameter'], json=data[dt])
            # self.print(resp.text)
            js = json.loads(resp.text)
            self.print('#### Data Recieved\n```json\n%s\n```\n' % str(js))
            self.print('#### Output Result\n')
            if js['status'] == data_expectation[dt]['status']:
                testInfo['passed'] = True
                testInfo['commnet'] = '- Status code matches the expected status code of %d' % (data_expectation[dt]['status'])
            elif js['status'] == 404:
                testInfo['passed'] = True
                testInfo['commnet'] = ' - User is not found in system. Expected outcome 404'
            else:
                testInfo['passed'] = False
                testInfo['commnet'] = '- Status code does not match. Expected code %d, but got code %d\n- Reason for this might be because we made some mistake in our server while writing the code. Something changed in server that we forgot to check' % (
                    data_expectation[dt]['status'], js['status'])

            self.print(testInfo['comment'])

            d = dump.dump_response(resp)
            self.print('\n### Raw Response\n')
            self.print('```')
            self.print(d.decode())
            self.print('```')

            if testInfo['passed']: funcResult['passedTest']+=1
            funcResult['individualTest'].append(testInfo)
        
        self.tests[funcName] = funcResult
        self.__totalTests += funcResult['totalTest']
        self.__totalPassed += funcResult['passedTest']

    def testManagerPrivCreateCampaign(self):
        funcName = inspect.stack()[0][3]
        funcReason = "Tests to see if only Manager can create campaign"
        funcResult = {}
        funcResult['totalTest'] = 0
        funcResult['passedTest'] = 0
        funcResult['testReason'] = funcReason
        funcResult['individualTest'] = []

        paths = {
            'login': self.url + '/auth/login',
            'create_campaign': self.url + '/manager/create_campaign'
        }
        self.print('## Testing Url Path\n```code\n%s\n```\n' % (paths['create_campaign']))
        login = {
            'valid_admin': {'email': 'daniel.li.2@stonybrook.edu', 'password': 'lifeishard'},
            'valid_manager': {'email': 'manjur.khan@stonybrook.edu', 'password': 'secret'}
        }

        data = {
            'valid_admin': {"title":"campaign 1","startDate":"10-10-18","endDate":"10-11-18"},
            'valid_manager': {"title":"campaign 2","startDate":"10-10-18","endDate":"10-11-18"}
        }

        data_test_reason = {
            'valid_admin': 'Testing to see that even though the user has some privilage, they cannot create campaign',
            'valid_manager': 'Testing to see that manager can create campaign successfully'
        }

        data_expectation = {
            'valid_manager': {'reason':'Expect code 201 for success on creating a new campaign and a campaign ID', 'status':201},
            'valid_admin': {'reason':'Expect code 403 for invalid permission to create a new campaign', 'status':403}
        }

        data_test_name = {
            'valid_admin': 'Valid Admin Create New Campaign',
            'valid_manager': 'Valid Manager Create New Campaign'
        }

        for dt in data:
            funcResult['totalTest']+= 1 # adding to the test count
            testInfo = {}
            testInfo['passed'] = False
            testInfo['reason'] = data_test_reason[dt]
            testInfo['comment'] = ""

            s = Session()
            self.print('## Test Name: %s\n' % (data_test_name[dt]))
            self.print('### Test Reason\n%s\n' % (data_test_reason[dt]))
            self.print('#### Data Expectations\n%s\n' % (data_expectation[dt]['reason']))

            self.print('#### Data Prerequisite: Perform Login\n')

            s.post(paths['login'], json=login[dt])

            self.print('#### Now Attempt To Create New Campaign\n')

            self.print('#### Data Sent\n```json\n%s\n```\n' % (str(data[dt])))


            resp = s.post(paths['create_campaign'], json=data[dt])
            # self.print(resp.text)
            js = json.loads(resp.text)
            self.print('#### Data Recieved\n```json\n%s\n```\n' % str(js))
            self.print('#### Output Result\n')
            if js['status'] == data_expectation[dt]['status']:
                testInfo['passed'] = True
                testInfo['comment'] = '- Status code matches the expected status code of %d' % (data_expectation[dt]['status'])
                if js['status'] == 201:
                    if dt == 'valid_manager':
                        testInfo['passed'] = True
                        testInfo['comment'] = '- Valid manager created the campaign with id %s as expected' % (js['materials']['id'])
                    else:
                        testInfo['passed'] = False
                        testInfo['comment'] = '- Uh something went wrong in our logic. This is not supposed to happen'
            else:
                testInfo['passed'] = False
                testInfo['comment'] = '- Status code does not match. Expected code %d, but got code %d\n- Reason for this might be because we made some mistake in our server while writing the code. Something changed in server that we forgot to check' % (
                    data_expectation[dt]['status'], js['status'])

            self.print(testInfo['comment'])

            d = dump.dump_response(resp)
            self.print('\n### Raw Response\n')
            self.print('```')
            self.print(d.decode())
            self.print('```')

            if testInfo['passed']: funcResult['passedTest']+=1
            funcResult['individualTest'].append(testInfo)
        
        self.tests[funcName] = funcResult
        self.__totalTests += funcResult['totalTest']
        self.__totalPassed += funcResult['passedTest']

    def testManagerPrivAddCampaignUser(self):
        funcName = inspect.stack()[0][3]
        funcReason = "Tests to see if only Manager can add users to campaign"
        funcResult = {}
        funcResult['totalTest'] = 0
        funcResult['passedTest'] = 0
        funcResult['testReason'] = funcReason
        funcResult['individualTest'] = []

        paths = {
            'login': self.url + '/auth/login',
            'add_campaign_user': self.url + '/manager/add_campaign_user'
        }
        self.print('## Testing Url Path\n```code\n%s\n```\n' % (paths['add_campaign_user']))
        
        login = {
            '0': {'email': 'manjur.khan@stonybrook.edu', 'password': 'secret'},
            '1': {'email': 'manjur.khan@stonybrook.edu', 'password': 'secret'},
            '2': {'email': 'manjur.khan@stonybrook.edu', 'password': 'secret'},
            '3': {'email': 'manjur.khan@stonybrook.edu', 'password': 'secret'},
            '4': {'email': 'manjur.khan@stonybrook.edu', 'password': 'secret'}
        }

        data = {
            '0': {"id": 1, "email": "daniel.li.2@stonybrook.edu","role": "manager"},
            '1': {"id": 1, "email": "john2.doe@gmail.com","role": "canvasser"},
            '2': {"id": 1, "email": "john3.doe@gmail.com","role": "canvasser"},
            '3': {"id": 1, "email": "john4.doe@gmail.com","role": "manager"},
            '4': {"id": 1, "email": "jane.doe@gmail.com","role": "canvasser"}
        }

        data_test_reason = {
            '0': 'Testing to see that a manager can be added to a campaign',
            '1': 'Testing to see that a canvasser can be added to a campaign',
            '2': 'Testing to see that duplicate users with same roles cannot be added to a campaign',
            '3': 'Testing to see that a user cannot be added to a campaign with a role it doesn\'t have',
            '4': 'Testing to see that users who don\'t exist cannot be added to a campaign'
        }

        data_expectation = {
            '0': {'reason':'Expect code 200 for success on adding a manager to a campaign', 'status':200},
            '1': {'reason':'Expect code 200 for success on adding a canvasser to a campaign', 'status':200},
            '2': {'reason':'Expect code 409 for failure on adding a duplicate user with same roles to a campaign', 'status':409},
            '3': {'reason':'Expect code 400 for failure on adding a user with a role it doesn\'t have to a campaign', 'status':400},
            '4': {'reason':'Expect code 404 for failure on adding a user that doesn\'t exist to a campaign', 'status':404}
        }

        data_test_name = {
            '0': 'Add Manager to Campaign',
            '1': 'Add Canvasser to Campaign',
            '2': 'Add Duplicate User to Campaign',
            '3': 'Add User with Invalid Role to Campaign',
            '4': 'Add Nonexisting User to Campaign'     
        }

        for dt in data:
            funcResult['totalTest']+= 1 # adding to the test count
            testInfo = {}
            testInfo['passed'] = False
            testInfo['reason'] = data_test_reason[dt]
            testInfo['comment'] = ""

            s = Session()
            self.print('## Test Name: %s\n' % (data_test_name[dt]))
            self.print('### Test Reason\n%s\n' % (data_test_reason[dt]))
            self.print('#### Data Expectations\n%s\n' % (data_expectation[dt]['reason']))

            self.print('#### Data Prerequisite: Perform Login\n')

            s.post(paths['login'], json=login[dt])

            self.print('#### Now Attempt To Add User to Campaign\n')

            self.print('#### Data Sent\n```json\n%s\n```\n' % (str(data[dt])))


            resp = s.post(paths['add_campaign_user'], json=data[dt])
            # self.print(resp.text)
            js = json.loads(resp.text)
            self.print('#### Data Recieved\n```json\n%s\n```\n' % str(js))
            self.print('#### Output Result\n')

            if js['status'] == data_expectation[dt]['status']:
                testInfo['passed'] = True
                testInfo['comment'] = '- Status code matches the expected status code of %d' % (data_expectation[dt]['status'])
            else:
                testInfo['passed'] = True
                testInfo['comment'] = '- Status code does not match. Expected code %d, but got code %d\n- Reason for this might be because we made some mistake in our server while writing the code. Something changed in server that we forgot to check' % (
                    data_expectation[dt]['status'], js['status'])

            self.print(testInfo['comment'])
            d = dump.dump_response(resp)
            self.print('\n### Raw Response\n')
            self.print('```')
            self.print(d.decode())
            self.print('```')

            if testInfo['passed']: funcResult['passedTest']+=1
            funcResult['individualTest'].append(testInfo)
        
        self.tests[funcName] = funcResult
        self.__totalTests += funcResult['totalTest']
        self.__totalPassed += funcResult['passedTest']
            
    def testManagerPrivAddLocation(self):
        funcName = inspect.stack()[0][3]
        funcReason = "Tests to see if only Manager can create location to campaign"
        funcResult = {}
        funcResult['totalTest'] = 0
        funcResult['passedTest'] = 0
        funcResult['testReason'] = funcReason
        funcResult['individualTest'] = []

        paths = {
            'login': self.url + '/auth/login',
            'add_location': self.url + '/manager/add_location'
        }
        self.print('## Testing Url Path\n```code\n%s\n```\n' % (paths['add_location']))
        
        login = {
            '0': {'email': 'manjur.khan@stonybrook.edu', 'password': 'secret'},
            '1': {'email': 'manjur.khan@stonybrook.edu', 'password': 'secret'}
        }

        data = {
            '0': {"campaign_id": 1, "address": "40, Piedmont Drive, Apartment 16B, Brookhaven, NY, 11776"},
            '1': {"campaign_id": 1, "address": ""}
        }

        data_test_reason = {
            '0': 'Testing to see that a location with a valid address can be added to a campaign',
            '1': 'Testing to see that a location with an address that cannot be found cannot be added to a campaign'
        }

        data_expectation = {
            '0': {'reason':'Expect code 200 for success on adding a manager to a campaign', 'status':200},
            '1': {'reason':'Expect code 404 for failure on adding a location with an address that cannot be found to a campaign', 'status':404},
        }

        data_test_name = {
            '0': 'Add Location to Campaign',
            '1': 'Add Location with Nonexisting Address to Campaign',
        }

        for dt in data:

            funcResult['totalTest']+= 1 # adding to the test count
            testInfo = {}
            testInfo['passed'] = False
            testInfo['reason'] = data_test_reason[dt]
            testInfo['comment'] = ""


            s = Session()
            self.print('## Test Name: %s\n' % (data_test_name[dt]))
            self.print('### Test Reason\n%s\n' % (data_test_reason[dt]))
            self.print('#### Data Expectations\n%s\n' % (data_expectation[dt]['reason']))

            self.print('#### Data Prerequisite: Perform Login\n')

            s.post(paths['login'], json=login[dt])

            self.print('#### Now Attempt To Add Location to Campaign\n')

            self.print('#### Data Sent\n```json\n%s\n```\n' % (str(data[dt])))


            resp = s.post(paths['add_location'], json=data[dt])
            # self.print(resp.text)
            js = json.loads(resp.text)
            self.print('#### Data Recieved\n```json\n%s\n```\n' % str(js))
            self.print('#### Output Result\n')

            if js['status'] == data_expectation[dt]['status']:
                testInfo['passed'] = True
                testInfo['comment'] = '- Status code matches the expected status code of %d' % (data_expectation[dt]['status'])
            else:
                testInfo['passed'] = False
                testInfo['comment'] = '- Status code does not match. Expected code %d, but got code %d\n- Reason for this might be because we made some mistake in our server while writing the code. Something changed in server that we forgot to check' % (
                    data_expectation[dt]['status'], js['status'])

            d = dump.dump_response(resp)
            self.print('\n### Raw Response\n')
            self.print('```')
            self.print(d.decode())
            self.print('```')

            if testInfo['passed']: funcResult['passedTest']+=1
            funcResult['individualTest'].append(testInfo)
        
        self.tests[funcName] = funcResult
        self.__totalTests += funcResult['totalTest']
        self.__totalPassed += funcResult['passedTest']

    def print(self, *args, **kwargs):
        if(self.verbose):
            print(" ".join(map(str,args)), **kwargs)

    @staticmethod
    def generateUser(admin=False, manager=False, canvassar=False):
        user = {}
        user['firstname'] = uuid4().hex[:8]
        user['lastname'] = uuid4().hex[-8]
        user['email'] = "%s.%s@gmail.com" % (user['firstname'], user['lastname'])
        user['password'] = uuid4().hex
        user['admin'] = admin
        user['manager'] = manager
        user['canvassar'] = canvassar
        user['phone'] = ''.join(str(random.randint(1,9)) for i in range(10))
        user['created'] = False
        return user

    def __str__(self):
        ret = self.url + '\n'
        for t_name in self.tests:
            ret += "Test name: %s, Passed: %d/%d, Test Reason: %s\n" %(t_name, self.tests[t_name]['passedTest'], self.tests[t_name]['totalTest'], self.tests[t_name]['testReason'])
            for test in self.tests[t_name]['individualTest']:
                passed = "+ Passed"
                comment = "-"
                if not test['passed']:
                    passed = "- Failed"
                    comment = test['comment'].replace('\n', '')
                ret += "\t%s - %s %s\n" % (passed, test['reason'], comment)
        ret += 'Passed/Total = %d/%d' % (self.__totalPassed, self.__totalTests)

        return ret

    @property
    def users(self):
        return self.__users
    
    @property
    def tests(self):
        return self.__tests

    @property
    def url(self):
        return self.__url

    @property
    def verbose(self):
        return self.__verbose

    @property
    def totalPassed(self):
        return self.__totalPassed
    
    @property
    def totalTests(self):
        return self.__totalTests


URL = 'http://54.172.85.69:8080'
test = Test(URL)
test.setVervose(True)
test.testLogin()

print(test)