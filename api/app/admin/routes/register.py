from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db, bcrypt
from app.admin.models import Admin
from app.auth.models import User
from app.admin.blueprint import mod_admin

# Set the route and accepted methods
@mod_admin.route('/register', methods=['POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def register():
    print("Attempting to register a new user...")

    # check if admin
    cookieID = request.cookies.get('cookieID')
    # if not Admin.is_admin(cookieID):
    #     print("Register user failed. Only admins can perform this action.")
    #     return make_response(jsonify({'comment': "Register user failed. Only admins can perform this action.", 'status': 403}))

    # get request
    req = request.get_json(force=True)
    print(req)

   # validate the input
    if not User.validate_register_user_input(req):
        print("Registration failed. Invalid input.")
        return make_response(jsonify({'comment': "Registration failed. Invalid input.", 'status': 400}))

    # check if user already exists
    user = User.query.filter_by(email=req['email']).first()
    if user:
        print("Registration failed. User already exists.")
        return make_response(jsonify({'comment': "Registration failed. User already exists.", 'status': 409}))
        
    # hash password & create user
    hashed_password = bcrypt.generate_password_hash(req['password']).decode('utf-8')
    user = User(first_name=req['firstname'], last_name=req['lastname'], phone=req['phone'], email=req['email'], password=hashed_password)

    # set user roles
    user.set_roles(req['manager'], req['canvasser'], req['admin'])
    
    # add user to db
    db.session.add(user)
    db.session.commit()

    # return success
    resp = make_response(jsonify({'comment': '%s created' % user, 'status': 201}))
    return resp