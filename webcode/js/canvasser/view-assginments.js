$(document).ready(function() {
  $(".window-container").load("/components/window.html", null, setupWindow);
});

function setupWindow() {
  $(".window-container").draggable();
  $(".title").text("Task Assginment");
  $(".section-1").load(
    "/components/window-sections/assignment-tabs.html",
    undefined,
    setupTabs
  );
  $(".modal-container").load("/components/modal.html", null, setupModal);
  $(".section-3").load(
    "/components/window-sections/table.html",
    null,
    pullData
  );
}

function setupTabs() {
  function setupUpcomingTable(data) {
    //LONGTITDUDE TAGS
    $(".table-container .clickable-row .longitude").each(function() {
      //longCoord.push($(this).attr("value"));
      $(this).css("display", "none");
    });

    //LATTIDUDE TAGS
    $(".table-container .clickable-row .latitude").each(function() {
      //latCoord.push($(this).attr("value"));
      $(this).css("display", "none");
    });

    //ID TAGS
    $(".table-container .clickable-row .id").each(function() {
      //latCoord.push($(this).attr("value"));
      $(this).css("display", "none");
    });
  }

  //Change tab and populate the upcoming locations table
  function switchTab(data) {
    $(".input-group").hide();
    $(".next-location").hide();
    $(".today-locations").hide();
    $(".upcoming-locations").empty();
    parseData2(data, ".upcoming-locations", setupUpcomingTable);
    $(".upcoming-locations").show();
  }
  function todayHandler() {
    $(".today-btn").click(function() {
      $(".input-group").show();
      $(".next-location").show();
      $(".today-locations").show();
      $(".upcoming-locations").hide();
    });
  }
  function dropdownHandler() {
    //On click event handler for upcoming date
    $(".upcoming-dropdown .dropdown-item").each(function() {
      $(this).on("click", function() {
        var path = "/canvasser/view_assignment_selected";
        var data = {
          task_id: $(this).attr("value")
        };
        makePost(path, data, switchTab);
        // //#########TEST JSON#################
        // $.getJSON(
        //   "/test_json/canvasser/upcoming-assignments-selected.json",
        //   function(json) {
        //     console.log(json);
        //     switchTab(json);
        //   }
        // );
        // //###################################
      });
    });
  }

  //Parse the dates returned by the dropdown
  function parseDropdown(data) {
    for (item in data["materials"]) {
      var date = data["materials"][item]["date"];
      var task_id = data["materials"][item]["task_id"];
      $(".upcoming-dropdown").append(
        '<a class="dropdown-item upcoming-date class="' +
          date +
          '" value="' +
          task_id +
          '"href="#" data-toggle="tab">' +
          date +
          "</a>"
      );
    }
    todayHandler();
    dropdownHandler();
  }

  var path = "/canvasser/view_assignments";
  var data = {};
  makePost(path, data, parseDropdown);
  // //#########TEST JSON#################
  // $.getJSON("/test_json/canvasser/upcoming-dropdown.json", function(json) {
  //   console.log(json);
  //   parseDropdown(json);
  // });
  // //###################################
}

//Setup add result modal
function setupModal() {
  //Loads the question container
  function loadQuestions() {
    //Parse the questionairre json
    function parseQuestions(data) {
      //Append question to modal
      function appendQuestion(data) {
        $.get(
          "/components/modal-forms/question.html",
          function(my_var) {
            var ele = my_var;
            console.log(ele);
            var questionObject = $("<div/>")
              .html(ele)
              .contents();
            questionObject.find(".question").append(data["question"]);
            $(".question-container").append(questionObject);
          },
          "html"
        );
      }
      for (question in data["materials"]) {
        //Load quesiton module and append it the question-container
        appendQuestion(data["materials"][question]);
      }
    }
    var path = "/cavasser/get_questions";
    var data = {
      task_id: $("#task_id").attr("value")
    };
    makePost(path, data, parseQuestions);
    //#########TEST JSON#################
    // $.getJSON("/test_json/canvasser/questionairre-questions.json", function(
    //   json
    // ) {
    //   console.log(json);
    //   parseQuestions(json);
    // });
    //###################################
  }
  function eventFunc(trigger) {}

  function submitFunc() {
    function success_f(data) {
      alert(data["comment"]);
      location.reload(true);
    }
    var response = [];
    var if_spoken;
    $("question-container .answer-toggle").each(function() {
      //active  button is yes
      if ($(this + " .active").hasClass("yes")) {
        response.push("yes");
      } else {
        response.push("no");
      }
    });

    if ($(".isSpokenTo .active").hasClass("yes")) {
      if_spoken = true;
    } else {
      if_spoken = false;
    }

    var path = "/canvasser/enter_result";

    var data = {
      location_id: $("#report-location").attr("value"),
      if_spoken: if_spoken,
      rating: $("#filterRate label.active input").val(),
      notes: $("#breifNotes").val()
    };
    makePost(path, data, success_f);
  }

  $(".mSection1").load(
    "/components/modal-forms/submit-questionairre.html",
    undefined,
    loadQuestions
  );
  modalHandler("reportModal", ".dynamicModal", submitFunc);
}

//Setup the next location html
function setupNextLocation() {
  //Populate the next location label
  function getNextLocation() {
    var nextAddress = $(".table-container .clickable-row .address")
      .first()
      .text();
    $(".next-location").text("Next: " + nextAddress);
  }

  $(".section-2").load(
    "/components/window-sections/next-location.html",
    null,
    getNextLocation
  );
}

function setupTable() {
  var longCoord = [];
  var latCoord = [];
  var orderMarkers = [];

  //Arry of markers for map
  var markers = [];

  //Rating TAGS
  $(".id").hide();
  $("task_id").hide();

  $(".number").each(function() {
    var number = parseInt($(this).attr("value"));
    var letter = String.fromCharCode(number + 64);
    $(this).text(letter);
  });

  $(".table-container .clickable-row .number").each(function() {
    var orderMarker = L.ExtraMarkers.icon({
      icon: "fa-number",
      markerColor: "red",
      shape: "square",
      prefix: "fa",
      number: $(this).attr("value")
    });
    orderMarkers.push({ icon: orderMarker });
  });

  //LONGTITDUDE TAGS
  $(".table-container .clickable-row .longitude").each(function() {
    longCoord.push($(this).attr("value"));
    $(this).css("display", "none");
  });

  //LATTIDUDE TAGS
  $(".table-container .clickable-row .latitude").each(function() {
    latCoord.push($(this).attr("value"));
    $(this).css("display", "none");
  });

  //Combines the coordinates into markers
  for (i = 0; i < longCoord.length; i++) {
    var mark = L.marker([longCoord[i], latCoord[i]], orderMarkers[i]);
    markers.push(mark);
  }
  var addressArr = [];
  $(".table-container .clickable-row .address").each(function() {
    addressArr.push($(this).text());
  });

  var map = setupMap(markers);
  getDirections(addressArr, map);

  rowClickHandler(rowHandlerFunc);

  setupNextLocation();
  // $("tr").disableSelection();
}

function rowHandlerFunc(element) {
  var task_id = $(element + ".task_id").attr("value");
  $("#task_id").attr("value", task_id);
  var reportLocation = element.getElementsByClassName("address")[0].textContent;
  $("#report-location").attr("value", reportLocation);
  $(".report-btn").attr("data-title", reportLocation + " Questionairre");
  $(".report-btn").data("title", reportLocation + " Questionairre");
  $("#search-bar").attr("value", reportLocation);
}

function pullData() {
  var path = "/canvasser/view_assignment";
  var data = {};
  makePost(path, data, setupTable);
  // //#########TEST JSON#################
  // $.getJSON("/test_json/canvasser/view-assignment.json", function(json) {
  //   console.log(json);
  //   $(".upcoming-locations").hide();
  //   parseData2(json, ".today-locations", setupTable);
  // });
  // //###################################
}

// function filterFunction() {
//   var input, filter, ul, li, a, i;
//   input = document.getElementById("location-search");
//   filter = input.value.toUpperCase();
//   div = $(".next-location");
//   a = $("a");
//   for (i = 0; i < a.length; i++) {
//     if (a[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
//       a[i].style.display = "";
//     } else {
//       a[i].style.display = "none";
//     }
//   }
// }
