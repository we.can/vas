$(document).ready(function() {
  var path = "/manager/unassigned_locations";
  var data = {
    campaign_id: getQueryParm("cid")
  };

  makePost(path, data, parseData);
  // //#########TEST JSON#################
  // $.getJSON("/test_json/unassigned-locations.json", function(json) {
  //   console.log(json);
  //   parseData(json);
  //   var markers = setupTable();

  //   setupMap(markers);
  // });
  // //###################################
  //   makePost(path, data, loadCard);

  $(".modal-container").load("/components/modal.html", null, setupModal);
  $(".modal-container-2").load("/components/modal.html", null, setupModal2);
});

function setupTable() {
  var longCoord = [];
  var latCoord = [];

  //Arry of markers for map
  var markers = [];

  //LONGTITDUDE TAGS
  $(".table-container .clickable-row .longitude").each(function() {
    longCoord.push($(this).attr("value"));
    $(this).css("display", "none");
  });

  //LATTIDUDE TAGS
  $(".table-container .clickable-row .latitude").each(function() {
    latCoord.push($(this).attr("value"));
    $(this).css("display", "none");
  });

  //Combines the coordinates into markers
  for (i = 0; i < longCoord.length; i++) {
    var mark = L.marker([longCoord[i], latCoord[i]]);
    markers.push(mark);
  }

  setupMap(markers);
}

function setupModal() {
  function submitFunc() {
    function split(texts) {
      arr = texts.split("\n");
      toRet = [];
      tmp = [];
      for (var i = 0; i < arr.length; i++) {
        if (tmp.length == 10) {
          toRet.push(tmp);
          tmp = [];
        }
        if (arr[i].trim() != "") {
          tmp.push(arr[i]);
        }
      }
      if (tmp.length > 0) {
        toRet.push(tmp);
      }
      return toRet;
    }

    function succesFunc() {
      location.reload(true);
    }

    var text = $("#addressTextArea").val();
    var postSplit = split(text);
    console.log(postSplit);

    for (items in postSplit) {
      var data = {
        campaign_id: getQueryParm("cid"), /////////////////////////make post
        addresses: postSplit[items]
      };
      var path = "/manager/add_locations";
      makePost(path, data, succesFunc);
    }

    //makePost
  }

  $(".mSection1").load("/components/modal-forms/add-address.html");

  modalHandler("AddAddressModal", ".modal-container .dynamicModal", submitFunc);
}

function setupModal2() {
  function submitFunc() {
    var path = "/manager/generate_assignments";
    var data = {
      campaign_id: getQueryParm("cid")
    };
    makePost(path, data, succesFunc); /////////////////////////make post
  }
  var htmlContent =
    "<p>This will only generate if there are enough canvassers to deal with all locations.</p>";
  modalHandler("GenAssModal", ".modal-container-2 .dynamicModal", submitFunc);
  appendSectionHTML(htmlContent, "#GenAssModal .mSection1");
}

function succesFunc(data) {
  console.log(data);
  alert(data["comment"]);
  location.reload(true);
}
