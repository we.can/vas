from __future__ import print_function
from ortools.constraint_solver import pywrapcp
from ortools.constraint_solver import routing_enums_pb2
import math
###########################
# Problem Data Definition #
###########################
def create_data_model(workday_duration, travel_speed, visit_duration, num_shifts, locations):
    # num_shifts = 1

    # Create demands and capacities
    demands = [visit_duration for l in locations]    
    demands[0] = 0
    capacities = [workday_duration for i in range(num_shifts)]

    # Store the data
    data = {}
    data["locations"] = locations
    data["num_locations"] = len(data["locations"])
    data["num_vehicles"] = num_shifts
    data["depot"] = 0
    data["demands"] = demands
    data["vehicle_capacities"] = capacities
    data["travel_speed"] = travel_speed
    return data

#######################
# Problem Constraints #
#######################
def manhattan_distance(position_1, position_2):
    # """Computes the Manhattan distance between two points"""
    #TODO: NOT ACTUALLY MANHATTAN DISTANCE. Actually using great-circle distance
    lat1 = position_1[0]
    lon1 = position_1[1]
    lat2 = position_2[0]
    lon2 = position_2[1]
    R = 6371000 # earth's radius (meters)
    φ1 = math.radians(lat1)
    φ2 = math.radians(lat2)
    Δφ = math.radians((lat2-lat1))
    Δλ = math.radians((lon2-lon1))
    a = math.sin(Δφ/2) * math.sin(Δφ/2) + math.cos(φ1) * math.cos(φ2) * math.sin(Δλ/2) * math.sin(Δλ/2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    d = R * c
    return d * 0.00062137 # meters to miles

def create_distance_callback(data):
    # """Creates callback to return distance between points."""
    _distances = {}

    for from_node in range(data["num_locations"]):
        _distances[from_node] = {}

        for to_node in range(data["num_locations"]):
            if from_node == data["depot"] or to_node == data["depot"] or from_node == to_node:
                _distances[from_node][to_node] = 0
            else:
                _distances[from_node][to_node] = (manhattan_distance(data["locations"][from_node],data["locations"][to_node]))
    # print(_distances)
    def distance_callback(from_node, to_node):
        # """Returns the manhattan distance between the two nodes"""
        return _distances[from_node][to_node]

    return distance_callback

def create_demand_callback(data, distance_callback):
    # """Creates callback to get demands at each location."""
    def demand_callback(from_node, to_node):
        travel_dist = distance_callback(from_node, to_node)
        travel_time = travel_dist/data["travel_speed"]
        to_ret = data["demands"][to_node] + travel_time
        # print(from_node, to_node, to_ret)
        return to_ret
    return demand_callback

def add_capacity_constraints(routing, data, demand_callback):
    # """Adds capacity constraint"""
    capacity = "Capacity"
    routing.AddDimensionWithVehicleCapacity(
        demand_callback,
        0, # null capacity slack
        data["vehicle_capacities"], # vehicle maximum capacities
        True, # start cumul to zero
        capacity)

###########
# Printer #
###########
def print_solution(data, routing, assignment):
    # """Print routes on console."""
    total_dist = 0
    total_time = 0
    for vehicle_id in range(data["num_vehicles"]):
        index = routing.Start(vehicle_id)
        plan_output = 'Route for vehicle {0}:\n'.format(vehicle_id)
        route_dist = 0
        route_time = 0
        while not routing.IsEnd(index):
            node_index = routing.IndexToNode(index)
            next_node_index = routing.IndexToNode(assignment.Value(routing.NextVar(index)))
            if node_index == 0 or next_node_index == 0:
                travel_dist = 0
                travel_time = 0
            else:
                travel_dist = manhattan_distance(
                    data["locations"][node_index],
                    data["locations"][next_node_index])
                travel_time = travel_dist/data["travel_speed"]
            route_dist += travel_dist
            route_time += (travel_time + data["demands"][node_index])
            plan_output += ' {0} Time({1}hr) -> '.format(node_index, round(route_time, 2))
            index = assignment.Value(routing.NextVar(index))
            #print(node_index, next_node_index, travel_dist, travel_dist/data["travel_speed"])

        node_index = routing.IndexToNode(index)
        total_dist += route_dist
        total_time += route_time
        plan_output += ' {0} Time({1}hr)\n'.format(node_index, round(route_time, 2))
        plan_output += 'Distance of the route: {0} mi\n'.format(round(route_dist, 2))
        plan_output += 'Time of the route: {0}hr\n'.format(round(route_time, 2))
        print(plan_output)
    print('Total Distance of all routes: {0} mi'.format(round(total_dist,2)))
    print('Total Time of all routes: {0}hr'.format(round(total_time, 2)))

###############
# TO 2D ARRAY #
###############
def to_2d_array(data, routing, assignment):
    route_matrix = []
    for vehicle_id in range(data["num_vehicles"]):
        index = routing.Start(vehicle_id)
        route = []
        while not routing.IsEnd(index):
            node_index = routing.IndexToNode(index)
            next_node_index = routing.IndexToNode(assignment.Value(routing.NextVar(index)))
            if data["locations"][next_node_index] != data["locations"][data["depot"]]:
                route.append(data["locations"][next_node_index])
            index = assignment.Value(routing.NextVar(index))
        if len(route) != 0:
            route_matrix.append(route)
            node_index = routing.IndexToNode(index)
    return route_matrix
#########
# SOLVE #
#########
def solve_cvrp(workday_duration, travel_speed, visit_duration, num_shifts, locations):

    # Instantiate the data problem
    data = create_data_model(workday_duration, travel_speed, visit_duration, num_shifts, [(0, 0)] + locations)

    # Create Routing Model
    routing = pywrapcp.RoutingModel(
        data["num_locations"],
        data["num_vehicles"],
        data["depot"])

    # Define weight of each edge
    distance_callback = create_distance_callback(data)
    routing.SetArcCostEvaluatorOfAllVehicles(distance_callback)

    # Add Capacity constraint
    demand_callback = create_demand_callback(data, distance_callback)
    add_capacity_constraints(routing, data, demand_callback)

    # Setting first solution heuristic (cheapest addition)
    search_parameters = pywrapcp.RoutingModel.DefaultSearchParameters()
    search_parameters.first_solution_strategy = (routing_enums_pb2.FirstSolutionStrategy.PATH_CHEAPEST_ARC)
    search_parameters.time_limit_ms = 5000

    # Solve the problem
    assignment = routing.SolveWithParameters(search_parameters)
    if assignment:
        print_solution(data, routing, assignment)
        return to_2d_array(data, routing, assignment)
    else:
        return None
