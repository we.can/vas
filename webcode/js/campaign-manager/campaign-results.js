$(document).ready(function() {
  var path = "/manager/get_results_list";
  var data = {
    campaign_id: getQueryParm("cid")
  };

  ////////// json file does not exist   
  makePost(path, data, parseData);

  //#########TEST JSON#################
  // $.getJSON("/test_json/campaign-manager/campaign-results.json", function(
  //   json
  // ) {
  //   console.log(json);
  //   parseData(json);
  // });
  //###################################
  createSideBar(setupSideBar);

  //HIDE MAP-VIEW
  $(".map-view").hide();
});

//Setups the table with handlers
function setupTable(data) {
  setPillColors("spoken", "success");
  setPillColors("no response", "danger");
  setPillColors("scheduled", "warning");
  setupMapView();
  rowClickHandler(loadSideTableData);
}

//Loads the sidebar section templates
function setupSideBar() {
  $.get(
    "/components/campaign-results-sections/section-1.html",
    function(my_var) {
      var ele = my_var;
      console.log(ele);
      $(".section-1").replaceWith(ele);
    },
    "html"
  );
  $(".section-2").load("/components/campaign-results-sections/section-2.html");
  $(".section-3").load("/components/campaign-results-sections/section-3.html");
  $(".section-1").hide();
  $(".section-2").hide();
  $(".section-3").hide();
}

//Function for when the row is clicked to load table data
function loadSideTableData(element) {
  console.log("clicked here")
  console.log($(element).find(".location"));
  $(".location-stat").text(
    $(element)
      .find(".address")
      .attr("value")
  );
  var path = "/manager/get_results_selected";
  var data = {
    campaign_id: getQueryParm("cid"),
    location_id: $(element).find(".location_id").attr("value")
  };

  ////////// json file does not exist
  makePost(path,data,loadSideBarData);

  //#########TEST JSON#################
  // $.getJSON("/test_json/campaign-manager/get-results-selected.json", function(
  //   json
  // ) {
  //   console.log(json);

  //   loadSideBarData(json);
  // });
  //###################################
}

//Functio to load side bar data when clicked
function loadSideBarData(data) {
  //Load the rating and locaiton title
  if(data != "url not found"){
    $(".rating").text(data["materials"]["rating"]);
  
    //Replace the note message
    $(".notes-message").text(data["materials"]["notes"]);
  
    //Setup the section2 qa table
    $(".result-container").empty();
    var data_copy = data;
    delete data_copy["materials"]["if_spoken"];
    delete data_copy["materials"]["rating"];
    delete data_copy["materials"]["notes"];
    delete data_copy["materials"]["question"];
    parseData3(data_copy["materials"], ".result-container", 
    ["location_id","address","date","#_responses"]);
  }

  $(".section-2").addClass("mt-3 mx-2");

  $(".section-1").show();
  $(".section-2").show();
  $(".section-3").show();
  $(".rating-container").css("background-color", "white");
}

//Shows the map-view and hides the table-view
function setupMapView() {
  //Get info for popup on map
  function setupPopupWrapper(marker) {
    var popupArr = [];

    //Visit Date TAGS
    $(".clickable-row").each(function() {
      var tmp =
        "<strong>" +
        $(this)
          .children(".location")
          .html() +
        "</strong></br><strong>Vist Date: </strong>" +
        $(this)
          .children(".visited")
          .html() +
        $(this)
          .children(".status")
          .html();
      popupArr.push(tmp);
    });

    setupPopup(markers, popupArr);
  }
  var markers = loadCoords();
  $(".map-btn").click(function() {
    $(".table-view").hide();
    $(".sidebar-container").hide();
    $(".map-view").show();
    setupMap(markers);
    setupPopupWrapper(markers);
  });

  $(".table-btn").click(function() {
    $(".table-view").show();
    $(".sidebar-container").show();
    $(".map-view").hide();
  });
}

//Load the longitude and latitude coords
function loadCoords() {
  var longCoord = [];
  var latCoord = [];
  var ratingMarkers = [];

  //Arry of markers for map
  var markers = [];

  //Rating TAGS
  $(".table-container .clickable-row .rating").each(function() {
    var ratingMarker = L.ExtraMarkers.icon({
      icon: "fa-number",
      markerColor: "red",
      shape: "square",
      prefix: "fa",
      number: $(this).attr("value")
    });
    ratingMarkers.push({ icon: ratingMarker });
    $(this).css("display", "none");
  });

  //LONGTITDUDE TAGS
  $(".table-container .clickable-row .longitude").each(function() {
    longCoord.push($(this).attr("value"));
    $(this).css("display", "none");
  });

  //LATTIDUDE TAGS
  $(".table-container .clickable-row .latitude").each(function() {
    latCoord.push($(this).attr("value"));
    $(this).css("display", "none");
  });

  //Combines the coordinates into markers
  for (i = 0; i < longCoord.length; i++) {
    var mark = L.marker([longCoord[i], latCoord[i]], ratingMarkers[i]);
    markers.push(mark);
  }
  return markers;
}

function setPillColors(target, color) {
  $(".table-container .clickable-row .status[value='" + target + "']").each(
    function() {
      $(this).html(
        '<h6><span class="badge badge-pill badge-' +
          color +
          '">' +
          target +
          "</span></h6>"
      );
    }
  );
}
