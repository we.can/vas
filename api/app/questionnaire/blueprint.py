from flask import Blueprint

# Define the blueprint: 'questionnaire', set its url prefix: app.url/questionnaire
mod_questionnaire = Blueprint('questionnaire', __name__, url_prefix='/questionnaire')
 