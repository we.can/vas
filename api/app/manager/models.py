from app import db

managing_campaigns = db.Table('managing_campaigns',
    db.Column('manager_id', db.Integer, db.ForeignKey('manager.id')),
    db.Column('campaign_id', db.Integer, db.ForeignKey('campaign.id'))
)

class Manager(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user = db.relationship('User', back_populates='manager')
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    campaigns = db.relationship('Campaign', secondary=managing_campaigns, back_populates='managers')

    def __repr__(self):
        return "Manager(%s, %s)" % (self.id, self.user.email)

    def serialize(self):
        return {
            'id': self.id,            
            'email': self.user.email,
            'firstname': self.user.first_name,
            'lastname': self.user.last_name
        }

    @staticmethod
    def is_manager(cookieID):
        from app.auth.models import User
        if (cookieID is None) or (cookieID is ""):
            return False
        user = User.query.filter_by(cookie_id=cookieID).first()
        if not user:
            return False
        if not user.manager:
            return False
        
        return True