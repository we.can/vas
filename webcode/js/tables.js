var table = $();

function parseData(data) {
  console.log(data);
  if (data["materials"].length == 0) {
    alert("No data available. Please add data.");
  } else {
    data["materials"].forEach(function(item, i) {
      table = table.add(createTable(item));
      console.log(table);
    });
    $(".table-container").append(table);
    setupTable();
    table = $();
  }
}

function parseData2(data, target, setupFunc) {
  console.log(data);
  if (data["materials"].length == 0) {
    alert("No data available. Please add data.");
  } else {
    data["materials"].forEach(function(item, i) {
      table = table.add(createTable(item));
      console.log(table);
    });
    $(target).html(table);
    setupFunc();
    table = $();
  }
}

function parseData3(data, target, key) {
  console.log(data);
  if (data.length == 0) {
    alert("No data available. Please add data.");
  } else {
    table = table.add(formTable(data[key]));
    $(target).html(table);
    table = $();
  }
}

// $(document).ready(function() {
//   // $(".table-container").append(table);
// });
function formTable(tableData, keys) {
  var tableTemplate = ['<tr class="clickable-row">'];
  for (var key in keys) {
    if (typeof tableData[key] === "object") {
      var innerTableData = tableData[key];
      for (var key1 in innerTableData) {
        var tempTable = [
          '"<td class="',
          key1,
          '"',
          'value="',
          innerTableData[key1],
          '">',
          innerTableData[key1],
          "</td>"
        ];
        tableTemplate = tableTemplate.concat(tempTable);
      }
    } else {
      var tempTable = [
        '"<td class="',
        key,
        '"',
        'value="',
        tableData[key],
        '">',
        tableData[key],
        "</td>"
      ];
      tableTemplate = tableTemplate.concat(tempTable);
    }
  }
  tableTemplate.concat("</tr>");

  return $(tableTemplate.join(""));
}
function createTable(tableData) {
  var tableTemplate = ['<tr class="clickable-row">'];

  for (var key in tableData) {
    if (typeof tableData[key] === "object") {
      var innerTableData = tableData[key];
      for (var key1 in innerTableData) {
        var tempTable = [
          '"<td class="',
          key1,
          '"',
          'value="',
          innerTableData[key1],
          '">',
          innerTableData[key1],
          "</td>"
        ];
        tableTemplate = tableTemplate.concat(tempTable);
      }
    } else {
      var tempTable = [
        '"<td class="',
        key,
        '"',
        'value="',
        tableData[key],
        '">',
        tableData[key],
        "</td>"
      ];
      tableTemplate = tableTemplate.concat(tempTable);
    }
  }
  tableTemplate.concat("</tr>");

  return $(tableTemplate.join(""));
}

//Event Handler for each row. Takes in a fucntion as an argument
function rowClickHandler(clickFunc) {
  var location;
  var element;

  $(".clickable-row").on("click", function(event) {
    // location = $(".clickable-row").val();
    // $(".sidebar").replaceWith("<h2>New heading</h2>");
    $(this)
      .addClass("active")
      .siblings()
      .removeClass("active");

    element = event.delegateTarget;
    clickFunc(element);
  });
}
