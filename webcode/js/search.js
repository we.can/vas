function filterFunction(searchId, tableTarget) {
  $(searchId).on("keyup", function() {
    var value = $(this)
      .val()
      .toLowerCase();
    $(tableTarget + " tr:not(.table-head)").filter(function() {
      $(this).toggle(
        $(this)
          .text()
          .toLowerCase()
          .indexOf(value) > -1
      );
    });
  });
}
