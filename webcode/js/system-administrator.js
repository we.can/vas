var data = {};

$(document).ready(function() {
  $(".modal-container").load(
    "/system-administrator/modal.html",
    undefined,
    onModalShow
  );
  var path = "/admin/get_all_users";
  makeGet(path, data, parseData);
  createSideBar();
});

//Setups up the table to reflect the information needed to be display
function setupTable() {
  //ADMIN TAGS
  $(".table-container .clickable-row .admin").each(function() {
    if ($(this).attr("value") == "true") {
      console.log("hitt");
      $(this).html(
        '<h6><span class="badge badge-pill badge-success">Sys. Admin</span></h6>'
      );
    } else if ($(this).attr("value") == "false") {
      $(this).text("");
    }
  });

  //CANVASSER TAGS
  $(".table-container .clickable-row .canvasser").each(function() {
    if ($(this).attr("value") == "true") {
      $(this).html(
        '<h6><span class="badge badge-pill badge-warning">Canvasser</span></h6>'
      );
    } else if ($(this).attr("value") == "false") {
      $(this).text("");
    }
  });

  //MANAGER TAGS
  $(".table-container .clickable-row .manager").each(function() {
    if ($(this).attr("value") == "true") {
      $(this).html(
        '<h6><span class="badge badge-pill badge-info">Campaign Manager</span></h6>'
      );
    } else if ($(this).attr("value") == "false") {
      $(this).text("");
    }
  });

  $(".phone").hide();
  $(".email").hide();
  $(".id").hide();

  rowClickHandler(onClickFunction);
}

function onClickFunction(element) {
  var data = [
    {
      firstname: $(element)
        .find(".firstname")
        .text(),
      lastname: $(element)
        .find(".lastname")
        .text(),
      title: "Lead Manager",
      phone: $(element)
        .find(".phone")
        .text(),
      email: $(element)
        .find(".email")
        .text(),
      admin: $(element)
        .find(".admin")
        .attr("value"),
      canvasser: $(element)
        .find(".canvasser")
        .attr("value"),
      manager: $(element)
        .find(".manager")
        .attr("value")
    }
  ];

  var section1 = [
    '<div class="row section-1 pt-3">',
    '<div class="col-5">',
    '<img src="https://via.placeholder.com/100" alt="Responsive image"></img>',
    "</div>",
    '<div class="col-7">',
    "<h4>",
    '<span class="firstname">',
    data[0].firstname,
    "</span>",
    " ",
    '<span class="lastname">',
    data[0].lastname,
    "</span>",
    "</h4>",
    "<h5>",
    data[0].title,
    "</h5>",
    "</div>",
    "</div>"
  ];

  $(".section-1").replaceWith($(section1.join("")));

  var section2 = [
    '<div class="row section-2">',
    '<div class="col-12">',
    "<h5>General Information</h5>",
    "<h6>Phone</h6>",
    '<span class="phone">',
    data[0].phone,
    "</span>",
    "<h6>Email</h6>",
    '<span class="email">',
    data[0].email,
    "</span>",
    "</div>",
    "</div>"
  ];

  $(".section-2").replaceWith($(section2.join("")));

  $(".section-3").load("/components/system-admin-section3.html");
  $(".section-3").attr("class", "row section-3");
}

//DYNAMICALLY CHANGES MODAL
function onModalShow() {
  $(".modal").on("show.bs.modal", function(event) {
    var button = $(event.relatedTarget);
    var usecase = button.data("whatever");
    var modal = $(this);
    modal.find(".modal-title").text(usecase);
    preFillModal(usecase);
    onClickSwitch();
  });
  modalHideFunc();
}

function preFillModal(usecase) {
  var path;
  //ADD NEW USER CASE
  if (usecase == "Add User") {
    path = "/admin/register";
    onModalSubmit(path, usecase);
  }
  //EDIT PROFILE CAS
  else if (usecase == "Edit Profile") {
    path = "/admin/edit_user";
    $("input[name=password]").hide();
    onModalSubmit(path, usecase);

    //GETTING PREFILLED VALUE OF THE PROFILE
    var email = $(".section-2 .col-12 .email").text();
    var firstname = $("td:contains('" + email + "')")
      .siblings(".firstname")
      .attr("value");
    var lastname = $("td:contains('" + email + "')")
      .siblings(".lastname")
      .attr("value");
    var phone = $("td:contains('" + email + "')")
      .siblings(".phone")
      .attr("value");
    var admin = $("td:contains('" + email + "')")
      .siblings(".admin")
      .attr("value");
    var manager = $("td:contains('" + email + "')")
      .siblings(".manager")
      .attr("value");
    var canvasser = $("td:contains('" + email + "')")
      .siblings(".canvasser")
      .attr("value");

    //PREFILLING THE TEXTBOXED
    $(".form-group input[name=firstname]").attr("value", firstname);
    $(".form-group input[name=firstname]").val(firstname);
    $(".form-group input[name=lastname]").attr("value", lastname);
    $(".form-group input[name=lastname]").val(lastname);
    $(".form-group input[name=phone]").attr("value", phone);
    $(".form-group input[name=phone]").val(phone);
    $("input[name=email]").attr("value", email);
    $("input[name=email]").val(email);

    //PREFILLING THE SWITHCES
    if (admin === "true") {
      $("form")
        .find(".switch  input[name=admin]")
        .attr("checked", "checked");
    }

    if (manager === "true") {
      $("form")
        .find(".switch  input[name=manager]")
        .attr("checked", "checked");
    }

    if (canvasser === "true") {
      $("form")
        .find(".switch  input[name=canvasser]")
        .attr("checked", "checked");
    }
  }
}

function onClickSwitch() {
  $(".switch input").click(function(e) {
    //SETS the checked property
    if ($(this).attr("checked") == undefined) {
      $(this).attr("checked", "checked");
    } else {
      $(this).removeAttr("checked");
    }
  });
}

function onModalSubmit(path, usecase) {
  //var path = "/admin/register";

  $(document).on("click", ".submit-form", function(event) {
    var data = {};
    //INPUT FIELD DATA
    $("form .form-group input").each(function() {
      data[this.name] = $(this).val();
      console.log(data[this.name], $(this).val());
    });

    //ROLE FIELD DATA
    $(".switch input").each(function() {
      var isRole;
      if ($(this).attr("checked") == undefined) {
        isRole = false;
      } else {
        isRole = true;
      }
      data[this.name] = isRole;
      //console.log(data[this.name], $(this).val());
    });

    //TO-DO: REMOVE IN FINAL VERSION
    if (usecase == "Edit Profile") {
      delete data["password"];
      delete data["phone"];
    }

    makePost(path, data, success_f);
  });
}

function success_f(data) {
  console.log(data);

  location.reload(true);
}

//When the modal is hidden function
function modalHideFunc() {
  $(".modal-container .modal").on("hidden.bs.modal", function(event) {
    location.reload(true);
    // $("input").each(function() {
    //   $(this).attr("value", "");
    //   $(this).val("");
    //   $(this).show();
    // });

    // //ROLE FIELD DATA RESET
    // $(".switch input").each(function() {
    //   $(this).removeAttr("checked");
    // });
  });
}
