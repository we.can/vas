$(document).ready(function() {
  $(".window-container").load("/components/window.html", null, setupWindow);
});

function setupWindow() {
  $(".window-container container").attr("class", "col-10");
  $(".section-1").load(
    "/components/window-sections/assignment-tabs-manager.html"
  );
  $(".section-2").load(
    "/components/window-sections/table-questions.html",
    null,
    loadTable
  );
}

function loadTable() {
  var path = "/manager/get_questionairre_results";
  var data = {
    email: "daniel.li.2@stonybrook.edu"
  };
  //#########TEST JSON#################
  $.getJSON("/test_json/campaign-manager/questionairre-results.json", function(
    json
  ) {
    console.log(json);
    parseData(json);
  });
  //###################################
}

function setupTable() {
  rowClickHandler(loadChartInfo);
}

function loadChartInfo(element) {
  var data = {
    question: $(element)
      .find("question")
      .attr("value")
  };

  //#########TEST JSON#################
  $.getJSON(
    "/test_json/campaign-manager/questionairre-results-piechart.json",
    function(json) {
      console.log(json);
      pieChart1Function(json);
      pieChart2Function(json);
    }
  );
  //###################################
}

function pieChart1Function(ajaxData) {
  //TO-DO: Change to load data from database
  google.charts.load("current", { packages: ["corechart"] });
  google.charts.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ["Yes/No", "Response"],
      ["Yes", ajaxData["materials"][0]["yes"]],
      ["No", ajaxData["materials"][0]["no"]]
    ]);

    var options = {
      title: "Yes/No Distribution"
    };

    var chart = new google.visualization.PieChart(
      document.getElementById("piechart1")
    );

    chart.draw(data, options);
  }
}

function pieChart2Function(ajaxData) {
  google.charts.load("current", { packages: ["corechart"] });
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {
    var pieData = [["Assignments", "# Response"]];
    for (item in ajaxData["materials"][1]) {
      pieData.push([item, ajaxData["materials"][1][item]]);
    }
    var data = google.visualization.arrayToDataTable(pieData);

    var options = {
      title: "Assignment Distribution"
    };

    var chart = new google.visualization.PieChart(
      document.getElementById("piechart2")
    );

    chart.draw(data, options);
  }
}
