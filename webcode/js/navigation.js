$(document).ready(function() {
  var title = $(document)
    .find("title")
    .text();

  if (~title.indexOf("Campaign Manager")) {
    if (~title.indexOf("Campaign Home")) {
      $(".header").load(
        "/components/navigation/navigation-manager-home.html",
        null,
        setupNav
      );
    } else {
      $(".header").load(
        "/components/navigation/navigation-manager.html",
        null,
        setupNav
      );
    }
  } else if (~title.indexOf("Canvasser")) {
    $(".header").load(
      "/components/navigation/navigation-canvasser.html",
      null,
      setupNav
    );
  } else if (~title.indexOf("System Administrator")) {
    $(".header").load(
      "/components/navigation/navigation-system-administrator.html",
      null,
      setupNav
    );
  }
});

function setupNav() {
  function getQuery(param) {
    var urlParams = new URLSearchParams(window.location.search);
    urlParams = urlParams.get(param);
    if (urlParams == null) {
      return "";
    } else {
      return "?" + param + "=" + urlParams;
    }
  }

  function getQuery2(param) {
    var urlParams = new URLSearchParams(window.location.search);
    urlParams = urlParams.get(param);
    if (urlParams == null) {
      return "";
    } else {
      return param + "=" + urlParams;
    }
  }

  function setActive(val) {
    var path = window.location.pathname.substring(1);
    var s = path.split("/");
    path = s[0] + "/" + s[1];
    var a = $('.nav-link[href="/' + path + val + '"]').parent();
    console.log(a);
    a.addClass("active");
  }
  function logout() {
    function success_f() {
      // function redirect(){
      //   window.location.replace("http://www.lifeishard.fyi/");
      // }
      setTimeout(1000);
      alert("successful logout");
    }
    $(".logout").click(function() {
      var path = "/auth/logout";
      var data = {};
      makeGet(path, data, success_f);
    });
  }
  var id = getQuery("cid");
  var startDate = getQuery2("startDate");
  if (startDate != undefined && startDate != "") {
    startDate = "&" + startDate;
  }
  $(".nav-link, .dropdown-item").each(function() {
    var hrefLink = $(this).attr("href");
    $(this).attr("href", hrefLink + id + startDate);
  });
  setActive(id);
  logout();
  hideRoles();
}

function getQueryParm(param) {
  var urlParams = new URLSearchParams(window.location.search);
  urlParams = urlParams.get(param);
  return urlParams;
}

function getCookie(name) {
  var match = document.cookie.match(new RegExp("(^| )" + name + "=([^;]+)"));
  if (match) return match[2];
}

function hideRoles() {
  if (getCookie("admin") != "True") {
    $(".admin-drop").hide();
  }
  if (getCookie("manager") != "True") {
    $(".manager-drop").hide();
  }
  if (getCookie("canvasser") != "True") {
    $(".canvasser-drop").hide();
  }
}
