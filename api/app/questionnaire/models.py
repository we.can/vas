from app import db
from app.campaign.models import Campaign

class Question(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    question = db.Column(db.Text, nullable=False)
    campaign = db.relationship('Campaign', back_populates='questions')
    campaign_id = db.Column(db.Integer, db.ForeignKey('campaign.id'))
    responses = db.relationship('Response', back_populates='question')

    def __repr__(self):
        return "Question(%s, '%s')" % (self.id, self.question)

    def serialize(self):
        return {
            'question_id': self.id,
            'question': self.question
        }

class Response(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    response = db.Column(db.String(128))
    location = db.relationship('Location', back_populates='responses')
    location_id = db.Column(db.Integer, db.ForeignKey('location.id'))
    question = db.relationship('Question', back_populates='responses')
    question_id = db.Column(db.Integer, db.ForeignKey('question.id'))
    
    def __repr__(self):
        return "Response(%s, %s, %s)" % (self.id, self.question, self.response)

    def serialize(self):
        return {
            'response_id': self.id,
            'question': self.question,
            'response': self.response
        }

    @classmethod
    def from_json(cls, req):
        return cls(response=req['response'] if req['response'] else None)

    
