from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.admin.blueprint import mod_admin
from app.admin.models import Admin
from app.global_parameter.models import Global_Parameter
from app.campaign.models import Campaign

# Edit the global parameters
@mod_admin.route('/edit_global_parameters', methods=['POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def edit_global_parameters():
    print("Attempting to edit global parameters.")

    # check if admin
    cookieID = request.cookies.get('cookieID') 
    if not Admin.is_admin(cookieID):
        print("Edit global parameters failed. Only admin can perform this action")
        return make_response(jsonify({'comment': "Edit global parameters failed. Only admin can perform this action", 'status': 403}))

    # get request
    req = request.get_json(force=True)
    print(req)

    # query global parameters
    travel_speed = Global_Parameter.query.filter_by(name='travel_speed').first()
    workday_duration = Global_Parameter.query.filter_by(name='workday_duration').first()
    print("current:", travel_speed.value, workday_duration.value)
    if not travel_speed:
        travel_speed = Global_Parameter(name="travel_speed", value=5)
        db.session.add(travel_speed)
    if not workday_duration:
        workday_duration = Global_Parameter(name="workday_duration", value=8)
        db.session.add(workday_duration)

    # edit global parameters
    if 'travel_speed' in req:
        travel_speed.value = req['travel_speed']
    if 'workday_duration' in req:
        workday_duration.value = req['workday_duration']
    
    db.session.commit()

    # fix campaign assignments
    campaigns = Campaign.query.all()
    for c in campaigns:
        Campaign.generate_assignments(c)

    # to return
    gps = {
        'travel_speed': travel_speed.value,
        'workday_duration': workday_duration.value
    }

    # return success
    print("Global parameters have been updated: %s" % gps)
    resp = make_response(jsonify({'comment': "Global parameters have been updated: %s" % gps, 'status': 200})) 
    return resp
