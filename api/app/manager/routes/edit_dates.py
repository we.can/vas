from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.manager.blueprint import mod_manager
from app.campaign.models import Campaign
from app.manager.models import Manager
import datetime as dt


@mod_manager.route('/edit_dates', methods=['POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def edit_dates():
    print("Attempting to edit start and/or end dates for a campaign.")

    # check if manager
    # cookieID = request.cookies.get('cookieID')
    # if not Manager.is_manager(cookieID):
    #     print("Edit dates failed. Only managers can perform this action.")
    #     return make_response(jsonify({'comment': "Edit dates failed. Only managers can perform this action.", 'status': 403}))

    # get request
    req = request.get_json(force=True)
    print(req)

    # get campaign from db
    campaign = Campaign.query.filter_by(id=req['campaign_id']).first()
    if not campaign:
        print("Edit dates failed. Campaign not found.")
        return make_response(jsonify({'comment': "Edit dates failed. Campaign not found.", 'status': 404}))

    # check valid start date

    # edit date
    campaign.startDate = req['start_date'] if 'start_date' in req else campaign.startDate
    campaign.endDate = req['end_date'] if 'end_date' in req else campaign.endDate

    # commit
    db.session.commit()

    # generate assignments
    Campaign.generate_assignments(campaign)

    # commit
    db.session.commit()

    # return success
    print("Campaign dates have been edited: %s, %s" % (campaign.startDate, campaign.endDate))
    resp = make_response(jsonify({
        'comment': "Campaign dates have been edited: %s, %s" % (campaign.startDate, campaign.endDate), 
        'status': 200
    }))
    return resp


