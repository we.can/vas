from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.manager.blueprint import mod_manager
from app.manager.models import Manager
from app.campaign.models import Campaign
from app.campaign.models import Location

@mod_manager.route('/unassigned_locations', methods=['POST'])
@cross_origin( headers=['Content-Type','application/json'])
def unassigned_locations():
    print("Attempting to get unassigned locations for a campaign.")

    # check if manager
    cookieID = request.cookies.get('cookieID')
    if not Manager.is_manager(cookieID):
        print("Get unassigned locations failed. Only managers can perform this action.")
        return make_response(jsonify({'comment': "Get unassigned locations failed. Only managers can perform this action.", 'status': 403}))

    # get request
    req = request.get_json(force=True)
    print(req)

    # get campaign from db
    campaign = Campaign.query.filter_by(id=req['campaign_id']).first()
    if not campaign:
        print("Get unassigned locations failed. Campaign not found.")
        return make_response(jsonify({'comment': "Get unassigned locations failed. Campaign not found.", 'status': 404}) )

    # get unassigned locations from db
    unassigned = Location.query.filter_by(task=None, campaign_id=campaign.id).all()

    # to_ret 
    to_ret = [l.serialize() for l in unassigned]

    # return success
    print("Returning unassigned locations for %s" % (campaign))
    resp = make_response(jsonify({
        'comment': "Returning unassigned locations for %s" % campaign, 
        'materials': to_ret, 
        'status': 200}))
    return resp
