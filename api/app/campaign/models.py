from datetime import datetime
from requests import post
from app import db
from app import algo
from app.auth.models import User
from app.manager.models import managing_campaigns
from app.canvasser.models import Canvasser
from app.canvasser.models import canvassing_campaigns
from app.canvasser.models import Date
from app.global_parameter.models import Global_Parameter
import json

class Campaign(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(256),  nullable=False)
    startDate = db.Column(db.String(128), nullable=False)
    endDate = db.Column(db.String(128),  nullable=False)
    visit_duration = db.Column(db.Float, nullable=False)
    talking_points = db.relationship('Talking_Point', back_populates='campaign')
    questions = db.relationship('Question', back_populates='campaign')
    managers = db.relationship('Manager', secondary=managing_campaigns, back_populates='campaigns')
    canvassers = db.relationship('Canvasser', secondary=canvassing_campaigns, back_populates='campaigns')
    tasks = db.relationship('Task', back_populates='campaign')
    locations = db.relationship('Location', back_populates='campaign')

    def __repr__(self):
        return "Campaign(%s, %s, %s, %s, %s)" % (self.id, self.title, self.startDate, self.endDate, self.visit_duration)

    def serialize(self):
        return {
            'id': self.id,
            'title': self.title,
            'startDate': self.startDate,
            'endDate': self.endDate,
            'visit_duration': self.visit_duration
        }
    
    @classmethod
    def from_json(cls, req):
        title = req['title']
        startDate = req['startDate']
        endDate = req['endDate']
        visit_duration = req['visit_duration'] if 'visit_duration' in req else 10
        visit_duration = visit_duration if visit_duration else 10
        
        return cls(title=title, startDate=startDate, endDate=endDate, visit_duration=visit_duration)

    @staticmethod
    def validate_create_campaign_input(req):
        #TODO
        return True

    @staticmethod
    def generate_assignments(campaign):
        print("Regenerating assignments")
        # get geo coordinates
        locations = campaign.locations

        # get global parameters
        workday_duration = Global_Parameter.query.filter_by(name='workday_duration').first()
        travel_speed = Global_Parameter.query.filter_by(name='travel_speed').first()

        if not travel_speed:
            travel_speed = Global_Parameter(name="travel_speed", value=5)
            db.session.add(travel_speed)
        if not workday_duration:
            workday_duration = Global_Parameter(name="workday_duration", value=8)
            db.session.add(workday_duration)
        db.session.commit()

        # TODO: get number of canvassing shifts available
        c_start = datetime.strptime(campaign.startDate, '%Y-%m-%d').date()
        c_end = datetime.strptime(campaign.endDate, '%Y-%m-%d').date()

        available_dates = []
        for canvasser in campaign.canvassers: # for each canvasser in campaign

            for date_obj in canvasser.dates:  # for each of their available dates
                d_date = datetime.strptime(date_obj.date, '%Y-%m-%d').date()

                if d_date >= c_start and d_date <= c_end and (not date_obj.task):
                    available_dates.append(date_obj)

        # solve cvrp
        # returns a 2d array of locations
        route_matrix = algo.Algorithm.Algorithm(
            locations,
            campaign.visit_duration,
            len(available_dates),
            workday_duration.value,
            travel_speed.value) 

        if not route_matrix:
            return False

        # delete all previous assignments
        for t in campaign.tasks:
            db.session.delete(t)

        # create tasks and make assignments
        for route in route_matrix:

            # mark Date object as busy
            next_date = available_dates.pop(0)
            
            # create task
            task = Task(campaign=campaign, canvasser=next_date.canvasser, date=next_date)

            for location in route:
                task.locations.append(location)

            db.session.add(task)

        # commit to db
        db.session.commit()  

        return True

        
class Task(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    campaign = db.relationship('Campaign', back_populates='tasks')
    campaign_id = db.Column(db.Integer, db.ForeignKey('campaign.id'))
    canvasser = db.relationship('Canvasser', back_populates='tasks')
    canvasser_id = db.Column(db.Integer, db.ForeignKey('canvasser.id'))
    date = db.relationship('Date', uselist=False, back_populates='task')
    locations = db.relationship('Location', back_populates='task')

    def __repr__(self):
        return "Task(%s)" % (self.id)

    def serialize(self):
        return {
            'id': self.id,
            'date': self.date.date
        }

class Location(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    task = db.relationship('Task', back_populates='locations')
    task_id = db.Column(db.Integer, db.ForeignKey('task.id'))
    campaign = db.relationship('Campaign', back_populates='locations')
    campaign_id = db.Column(db.Integer, db.ForeignKey('campaign.id'))
    address = db.Column(db.String(128), nullable=False)
    latitude = db.Column(db.Float, nullable=False)
    longitude = db.Column(db.Float, nullable=False)
    responses = db.relationship('Response', back_populates='location')
    if_spoken = db.Column(db.Boolean)
    rating = db.Column(db.Integer)
    notes = db.Column(db.Text)
    
    def __repr__(self):
        return "Location(%s, '%s', %s, %s, %s, %s, %s)" % (self.id, self.address, self.latitude, self.longitude, self.if_spoken if self.if_spoken else 'None', self.rating if self.rating else 'None',self.notes if self.notes else 'None')

    def serialize(self):
        return {
            'id': self.id,
            'address': self.address,
            'latitude': self.latitude,
            'longitude': self.longitude,
            'if_spoken': self.if_spoken if self.if_spoken else 'None',
            'rating': self.rating if self.rating else 'None',
            'notes': self.notes if self.notes else 'None'
        }

    @classmethod
    def from_json(cls, req):
        campaign = Campaign.query.filter_by(id=req['campaign_id']).first()
        latitude, longitude = Location.geocode(req['address'])
        return cls(campaign_id=req['campaign_id'], campaign=campaign, address=req['address'], latitude=latitude, longitude=longitude)

    @classmethod
    def many_from_json(cls, req):
        to_ret = []
        campaign = Campaign.query.filter_by(id=req['campaign_id']).first()
        addresses = req['addresses']
        for a in addresses:
            latitude, longitude = Location.geocode(a)
            l = cls(campaign_id=req['campaign_id'], campaign=campaign, address=a, latitude=latitude, longitude=longitude)
            to_ret.append(l)
        return to_ret

    @staticmethod
    def geocode(address):
        if not address:
            return (0, 0)
        url = "http://www.mapquestapi.com/geocoding/v1/address?key=NcMyDVHarHdppdp3RzhqG2WohI8IbjJm"
        data = {
            "location": address,
            "options": {
                "thumbMaps": False
            }
        }
        r = post(url, data=json.dumps(data)).json()
        lat, lng = r['results'][0]['locations'][0]['displayLatLng'].values()
        return lat, lng
    
class Talking_Point(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    campaign = db.relationship('Campaign', back_populates='talking_points')
    campaign_id = db.Column(db.Integer, db.ForeignKey('campaign.id'))
    text = db.Column(db.Text, nullable=False)

    def __repr__(self):
        return "Talking_Point(%s, '%s')" % (self.id, self.text)

    def serialize(self):
        return {
            'id': self.id,
            'description': self.text
        }

    @classmethod
    def from_json(cls, req):
        to_ret = []
        campaign = Campaign.query.filter_by(id=req['campaign_id']).first()
        talking_points = req['talking_points']
        for t in talking_points:
            tp = Talking_Point(campaign=campaign, text=t)
            to_ret.append(tp)
        return to_ret