"""Capacitated Vehicle Routing Problem with Time Windows (CVRPTW).
"""
from __future__ import print_function
from six.moves import xrange
from ortools.constraint_solver import pywrapcp
from ortools.constraint_solver import routing_enums_pb2
from app.global_parameter.models import Global_Parameter

###########################
# Problem Data Definition #
###########################

class Vehicle():
    """Stores the property of a vehicle"""

    def __init__(self, travel_speed):
        """Initializes the vehicle properties"""
        self._capacity = 200
        # Travel speed: 5km/h to convert in m/min
        self._speed = travel_speed * 60 / 3.6

    @property
    def capacity(self):
        """Gets vehicle capacity. For our case it does not matter"""
        return self._capacity

    @property
    def speed(self):
        """Gets the average travel speed of a vehicle in meters per minute"""
        return self._speed

class DataProblem():
    """Stores the data for the problem"""
    
    def __init__(self, locations, visit_duration, num_shifts, workday_duration, travel_speed):
        """Initializes the data for the problem. travel_speed is in km/h"""
        self._vehicle = Vehicle(travel_speed)
        self._num_vehicles = num_shifts
        self._visit_duration = visit_duration

        self._depot = 0

        self._locations = [locations[0]] + locations

        """Gives everything the same priority"""
        self._demands = [1] * len(self._locations)

        """Work duration is converted to minutes"""
        self._time_windows = [(0, workday_duration*60)] * len(self._locations)

    @property
    def vehicle(self):
        """Gets a vehicle"""
        return self._vehicle

    @property
    def num_vehicles(self):
        """Gets number of vehicles"""
        return self._num_vehicles

    @property
    def locations(self):
        """Gets locations"""
        return self._locations

    @property
    def num_locations(self):
        """Gets number of locations"""
        return len(self.locations)

    @property
    def depot(self):
        """Gets depot location index"""
        return self._depot

    @property
    def demands(self):
        """Gets demands at each location"""
        return self._demands

    @property
    def time_per_demand_unit(self):
        """Gets the time (in min) to load a demand"""
        return self._visit_duration  # minutes/unit

    @property
    def time_windows(self):
        """Gets (start time, end time) for each locations"""
        # Can change to constant
        return self._time_windows


#######################
# Problem Constraints #
#######################


def manhattan_distance(position_1, position_2):
    a_constant = 100000 # this is changing geo coordinate to meters
    """Computes the Manhattan distance between two points"""
    distance = (abs(position_1.latitude - position_2.latitude) +
            abs(position_1.longitude - position_2.longitude)) * a_constant
    # print("distance is ", distance)
    return distance


class CreateDistanceEvaluator(object):
    """Creates callback to return distance between points."""

    def __init__(self, data):
        """Initializes the distance matrix."""
        self._distances = {}
        
        # precompute distance between location to have distance callback in O(1)
        for from_node in xrange(data.num_locations):
            self._distances[from_node] = {}
            for to_node in xrange(data.num_locations):
                if from_node == data.depot or to_node == data.depot :
                    self._distances[from_node][to_node] = 0
                else:
                    self._distances[from_node][to_node] = (
                        manhattan_distance(
                            data.locations[from_node],
                            data.locations[to_node]))

    def distance_evaluator(self, from_node, to_node):
        """Returns the manhattan distance between the two nodes"""
        return self._distances[from_node][to_node]


class CreateDemandEvaluator(object):
    """Creates callback to get demands at each location."""

    def __init__(self, data):
        """Initializes the demand array."""
        self._demands = data.demands

    def demand_evaluator(self, from_node, to_node):
        """Returns the demand of the current node"""
        del to_node
        return self._demands[from_node]


def add_capacity_constraints(routing, data, demand_evaluator):
    """Adds capacity constraint"""
    capacity = "Capacity"
    routing.AddDimension(
        demand_evaluator,
        0,  # null capacity slack
        data.vehicle.capacity,  # vehicle maximum capacity
        True,  # start cumul to zero
        capacity)


class CreateTimeEvaluator(object):
    """Creates callback to get total times between locations."""
    @staticmethod
    def service_time(data, node):
        """Gets the service time for the specified location."""
        # return data.demands[node] * data.time_per_demand_unit
        # Service time is the time to wait at each location
        return 10

    @staticmethod
    def travel_time(data, from_node, to_node):
        """Gets the travel times between two locations."""
        if from_node == to_node:
            travel_time = 0
        else:
            travel_time = manhattan_distance(
                data.locations[from_node],
                data.locations[to_node]) / data.vehicle.speed
        return travel_time

    def __init__(self, data):
        """Initializes the total time matrix."""
        self._total_time = {}
        # precompute total time to have time callback in O(1)
        for from_node in xrange(data.num_locations):
            self._total_time[from_node] = {}
            for to_node in xrange(data.num_locations):
                if from_node == to_node:
                    self._total_time[from_node][to_node] = 0
                else:
                    self._total_time[from_node][to_node] = int(
                        self.service_time(data, from_node) +
                        self.travel_time(data, from_node, to_node))

    def time_evaluator(self, from_node, to_node):
        """Returns the total time between the two nodes"""
        return self._total_time[from_node][to_node]


def add_time_window_constraints(workday_duration, routing, data, time_evaluator):
    """Add Global Span constraint"""
    time = "Time"
    horizon = 60 * workday_duration
    routing.AddDimension(
        time_evaluator,
        horizon,  # allow waiting time
        horizon,  # maximum time per vehicle
        False,  # don't force start cumul to zero since we are giving TW to start nodes
        time)
    time_dimension = routing.GetDimensionOrDie(time)
    for location_idx, time_window in enumerate(data.time_windows):
        if location_idx == 0:
            continue
        index = routing.NodeToIndex(location_idx)
        time_dimension.CumulVar(index).SetRange(time_window[0], time_window[1])
        routing.AddToAssignment(time_dimension.SlackVar(index))
    for vehicle_id in xrange(data.num_vehicles):
        index = routing.Start(vehicle_id)
        time_dimension.CumulVar(index).SetRange(
            data.time_windows[0][0], data.time_windows[0][1])
        routing.AddToAssignment(time_dimension.SlackVar(index))

###########
# Printer #
###########


class ConsolePrinter():
    """Print solution to console"""

    def __init__(self, data, routing, assignment):
        """Initializes the printer"""
        self._data = data
        self._routing = routing
        self._assignment = assignment

    @property
    def data(self):
        """Gets problem data"""
        return self._data

    @property
    def routing(self):
        """Gets routing model"""
        return self._routing

    @property
    def assignment(self):
        """Gets routing model"""
        return self._assignment

    def print(self):
        """Prints assignment on console"""
        # Inspect solution.
        capacity_dimension = self.routing.GetDimensionOrDie('Capacity')
        time_dimension = self.routing.GetDimensionOrDie('Time')
        total_dist = 0
        total_time = 0
        for vehicle_id in xrange(self.data.num_vehicles):
            index = self.routing.Start(vehicle_id)
            plan_output = 'Route for vehicle {0}:\n'.format(vehicle_id)
            route_dist = 0
            while not self.routing.IsEnd(index):
                node_index = self.routing.IndexToNode(index)
                next_node_index = self.routing.IndexToNode(
                    self.assignment.Value(self.routing.NextVar(index)))
                route_dist += manhattan_distance(
                    self.data.locations[node_index],
                    self.data.locations[next_node_index])
                load_var = capacity_dimension.CumulVar(index)
                route_load = self.assignment.Value(load_var)
                time_var = time_dimension.CumulVar(index)
                time_min = self.assignment.Min(time_var)
                time_max = self.assignment.Max(time_var)
                slack_var = time_dimension.SlackVar(index)
                slack_min = self.assignment.Min(slack_var)
                slack_max = self.assignment.Max(slack_var)
                plan_output += ' {0} Load({1}) Time({2},{3}) Slack({4},{5}) ->'.format(
                    node_index,
                    route_load,
                    time_min, time_max,
                    slack_min, slack_max)
                index = self.assignment.Value(self.routing.NextVar(index))

            node_index = self.routing.IndexToNode(index)
            load_var = capacity_dimension.CumulVar(index)
            route_load = self.assignment.Value(load_var)
            time_var = time_dimension.CumulVar(index)
            route_time = self.assignment.Value(time_var)
            time_min = self.assignment.Min(time_var)
            time_max = self.assignment.Max(time_var)
            total_dist += route_dist
            total_time += route_time
            plan_output += ' {0} Load({1}) Time({2},{3})\n'.format(
                node_index, route_load, time_min, time_max)
            plan_output += 'Distance of the route: {0}m\n'.format(route_dist)
            plan_output += 'Load of the route: {0}\n'.format(route_load)
            plan_output += 'Time of the route: {0}min\n'.format(route_time)
            print(plan_output)
        print('Total Distance of all routes: {0}m'.format(total_dist))
        print('Total Time of all routes: {0}min'.format(total_time))
    def getRoutes(self):
        """Prints assignment on console"""
        # Inspect solution.
        routes = []
        for vehicle_id in xrange(self.data.num_vehicles):
            index = self.routing.Start(vehicle_id)
            route_dist = 0
            locations = []
            while not self.routing.IsEnd(index):
                node_index = self.routing.IndexToNode(index)
                next_node_index = self.routing.IndexToNode(
                    self.assignment.Value(self.routing.NextVar(index)))
                locations.append(self.data.locations[node_index])
                route_dist += manhattan_distance(
                    self.data.locations[node_index],
                    self.data.locations[next_node_index])
                index = self.assignment.Value(self.routing.NextVar(index))

            if(len(locations) > 1):
                routes.append(locations)
        return routes


########
# Main #
########

class Algorithm():
    """To run, do `from Algorithm import Algorithm` then Algorithm(locations, visit_duration, num_shifts, workday_duration, travel_speed)"""
    @staticmethod
    def Algorithm(locations, visit_duration, num_shifts, workday_duration, travel_speed):
        print("Locations:",locations)
        print("Visit duration:",visit_duration)
        print("Number of workdays available:",num_shifts)
        print("Workday duration:",workday_duration)
        print("Travel speed:",travel_speed)
        if len(locations)==0 or num_shifts==0 or workday_duration==0 or travel_speed==0 or visit_duration == 0:
            return None
        num_shifts = int(num_shifts)
        workday_duration = int(workday_duration)
        visit_duration = int(visit_duration)
        """Entry point of the program. visit_duration in minutes. workday_duration in hours. travel_speed in km/h"""
        # Instantiate the data problem.
        data = DataProblem(locations, int(visit_duration), int(num_shifts), int(workday_duration), travel_speed)

        # Create Routing Model
        routing = pywrapcp.RoutingModel(
            data.num_locations, data.num_vehicles, data.depot)
        # Define weight of each edge
        distance_evaluator = CreateDistanceEvaluator(data).distance_evaluator
        routing.SetArcCostEvaluatorOfAllVehicles(distance_evaluator)
        # Add Capacity constraint
        demand_evaluator = CreateDemandEvaluator(data).demand_evaluator
        add_capacity_constraints(routing, data, demand_evaluator)
        # Add Time Window constraint
        time_evaluator = CreateTimeEvaluator(data).time_evaluator
        add_time_window_constraints(workday_duration, routing, data, time_evaluator)

        # Setting first solution heuristic (cheapest addition).
        search_parameters = pywrapcp.RoutingModel.DefaultSearchParameters()
        search_parameters.first_solution_strategy = (
            routing_enums_pb2.FirstSolutionStrategy.PATH_CHEAPEST_ARC)
        search_parameters.time_limit_ms = 10000

        # Solve the problem.
        assignment = routing.SolveWithParameters(search_parameters)
        if assignment:
            printer = ConsolePrinter(data, routing, assignment)
            printer.print()
            return printer.getRoutes()
        
        return None

