from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.manager.blueprint import mod_manager
from app.manager.models import Manager
from app.campaign.models import Campaign
from app.campaign.models import Location
from app.questionnaire.models import Response
import statistics

@mod_manager.route('/get_stat_summary', methods=['POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def get_stat_summary():
    print("Attempting to get statistical summary for campaign")

    # check if manager
    cookieID = request.cookies.get('cookieID')
    if not Manager.is_manager(cookieID):
        print("Get statistical summary failed. Only managers can perform this cation")
        return make_response(jsonify({'status': 403, 'comment': "Get statistical summary failed. Only managers can perform this cation"}))
        
    # get request
    req = request.get_json(force=True)
    print(req)

    # get campaign from db
    campaign = Campaign.query.filter_by(id=req['campaign_id']).first()
    if not campaign:
        print("Get statistical summary failed. Specified campaign not found")
        return make_response(jsonify({'status': 404, 'comment': "Get statistical summary failed. Specified campaign not found"}))

    # get locations and ratings
    locations_to_ret = []
    ratings = []
    for l in campaign.locations:
        if l.rating:
            ratings.append(l.rating)
            temp = {
                'address': l.address,
                'rating': l.rating
            }
            locations_to_ret.append(temp)
            
    # print(ratings)
    # print(locations_to_ret)
    # to_ret
    to_ret = {
        'average': statistics.mean(ratings), 
        'standard_deviation': statistics.stdev(ratings),
        'locations': locations_to_ret
    }
    
    # print(to_ret)
    # return success
    print("Returning statistical summary for %s: avg: %s, stdev: %s" % (campaign, to_ret['average'], to_ret['standard_deviation']))
    return make_response(jsonify({'comment': "Returning statistical summary for %s" % campaign, 'materials': to_ret, 'status': 200}))
