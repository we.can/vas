from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.manager.blueprint import mod_manager
from app.manager.models import Manager
from app.campaign.models import Campaign

# Get all the users that are in the campaign
@mod_manager.route('/get_campaign_users', methods=['POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def get_campaign_users():
    print("Attempting to get all users assigned to a campaign")

    # check if manager
    cookieID = request.cookies.get('cookieID')
    if not Manager.is_manager(cookieID):
        print("Get campaign users failed. Only managers can perform this cation")
        return make_response(jsonify({'status': 403, 'comment': "Get campaign users failed. Only managers can perform this cation"}))
        
    # get request
    req = request.get_json(force=True)
    print(req)

    # get campaign from db
    campaign = Campaign.query.filter_by(id=req['id']).first()
    if not campaign:
        print("Get campaign users failed. Specified campaign not found")
        return make_response(jsonify({'status': 404, 'comment': "Get campaign users failed. Specified campaign not found"}))

    # to_ret
    to_ret = [m.serialize() for m in campaign.managers]
    for m in to_ret:
        m['role'] = {
            'manager': True,
            'canvasser': False
        }

    to_add = []
    for c in campaign.canvassers:
        for m in to_ret:
            if m['email'] == c.user.email:
                (m['role'])['canvasser'] = True
                break
            else:
                to_add.append(c)
    
    for c in to_add:
        c_serialized = c.serialize()
        c_serialized['role'] = {
            'manager': False,
            'canvasser': True
        }
        to_ret.append(c_serialized)
    
    # return success
    print("Returning users for %s: %s" % (campaign, to_ret))
    resp = make_response(jsonify({
        'comment': "Returning users for %s" % campaign, 
        'materials': to_ret, 
        'status': 200}))
    return resp
