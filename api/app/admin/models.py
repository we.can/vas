from app import db

class Admin(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user = db.relationship('User', back_populates='admin')
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __repr__(self):
        return "Admin(%s, %s)" % (self.id, self.user.email)

    def serialize(self):
        return {
            'id': self.id,
            'email': self.user.email,
            'firstname': self.user.first_name,
            'lastname': self.user.last_name
        }

    @staticmethod
    def is_admin(cookieID):
        from app.auth.models import User
        """Must have cookieID ors it will be false"""
        if (cookieID is None) or (cookieID is ""):
            return False
        user = User.query.filter_by(cookie_id=cookieID).first()
        if not user:
            return False
        if not user.admin:
            return False
        
        return True
        
    def validate_edit_user_input(self):
        #TODO
        return True
