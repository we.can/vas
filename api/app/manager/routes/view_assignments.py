from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.manager.blueprint import mod_manager
from app.manager.models import Manager
from app.campaign.models import Campaign
from app.campaign.models import Task

@mod_manager.route('/view_assignments', methods=['POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def view_assignments():
    print("Attempting to view all assignments (tasks) for a campaign.")

    # check if manager
    cookieID = request.cookies.get('cookieID')
    if not Manager.is_manager(cookieID):
        print("View assignments (tasks) for a campaign failed. Only managers can perform this action.")
        return make_response(jsonify({'comment': "View assignments (tasks) for a campaign failed. Only managers can perform this action.", 'status': 403}))

    # get request
    req = request.get_json(force=True)
    print(req)

    # get campaign from db
    campaign = Campaign.query.filter_by(id=req['campaign_id']).first()
    if not campaign:
        print("View assignments (tasks) for a campaign failed. Campaign not found.")
        return make_response(jsonify({'comment': "View assignments (tasks) for a campaign failed. Campaign not found.", 'status': 404}) )

    # get tasks for campaign
    tasks = Task.query.filter_by(campaign_id=req['campaign_id']).all()
    # tasks = Task.query.filter_by(campaign=campaign).all()

    # format to_ret
    to_ret = []
    for t in tasks:
        temp = {}
        temp['task_id'] = t.id
        temp['date'] = t.date.date
        temp['canvasser_email'] = t.canvasser.user.email 
        temp['#_locations'] = len(t.locations)
        to_ret.append(temp)

    # return success
    # print("Returning list of assignments (tasks) for %s: %s" % (campaign, to_ret))
    print("Returning list of assignments (tasks) for %s" % (campaign))
    resp = make_response(jsonify({
        'comment': "Returning assignments (tasks) for %s" % campaign, 
        'materials': to_ret, 
        'status': 200}))
    return resp