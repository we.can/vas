from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.manager.blueprint import mod_manager
from app.campaign.models import Campaign
from app.campaign.models import Talking_Point
from app.manager.models import Manager
import datetime as dt

@mod_manager.route('/add_talking_points', methods=['POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def add_talking_points():
    print("Attempting to add talking points to a campaign.")

    # check if manager
    cookieID = request.cookies.get('cookieID')
    if not Manager.is_manager(cookieID):
        print("Add talking points failed. Only managers can perform this action.")
        return make_response(jsonify({'comment': "Add talking points failed. Only managers can perform this action.", 'status': 403}))

    # get request
    req = request.get_json(force=True)
    print(req)

    # get campaign from db
    campaign = Campaign.query.filter_by(id=req['campaign_id']).first()
    if not campaign:
        print("Add talking points failed. Campaign not found.")
        return make_response(jsonify({'comment': "Add talking points failed. Campaign not found.", 'status': 404}))

    # if campaign started, cannot make edits
    today_str = str(dt.date.today())
    if today_str >= campaign.startDate:
        print("Add questions failed. Campaign already started.")
        return make_response(jsonify({
            'comment': "Add questions failed. Campaign already started", 
            'status': 400,
            'locked': True}
        ))

    # edit visit duration
    talking_points = Talking_Point.from_json(req)

    # talking points to db
    for t in talking_points:
        db.session.add(t)
        db.session.commit()

    # return success
    print("Talking point(s) added to %s" % campaign)
    resp = make_response(jsonify({
        'comment': "Talking point(s) added to %s" % campaign, 
        'status': 200
    }))
    return resp

