from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.manager.blueprint import mod_manager
from app.manager.models import Manager
from app.campaign.models import Campaign

@mod_manager.route('/get_talking_points', methods=['POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def get_talking_points():
    print("Attempting to get talking points for campaign.")

    # check if manager
    cookieID = request.cookies.get('cookieID')
    if not Manager.is_manager(cookieID):
        print("Get talking points to campaign failed. Only managers can perform this action.")
        return make_response(jsonify({'comment': "Get talking points to campaign failed. Only managers can perform this action.", 'status': 403}))

    # get request
    req = request.get_json(force=True)
    print(req)

    # get campaign from db
    campaign = Campaign.query.filter_by(id=req['campaign_id']).first()
    if not campaign:
        print("Get talking points failed. Campaign not found.")
        return make_response(jsonify({'comment': "Get talking points failed. Campaign not found.", 'status': 404}))

    # if campaign started, cannot make edits
    today_str = str(dt.date.today())
    if today_str >= campaign.startDate:
        print("Add questions failed. Campaign already started.")
        return make_response(jsonify({
            'comment': "Add questions failed. Campaign already started", 
            'status': 400,
            'locked': True}
        ))

    # get questions
    talking_points = [t.text for t in campaign.talking_points]

    # return success
    print("Return talking points for %s: %s" % (campaign, talking_points))
    resp = make_response(jsonify({
        'comment': "Return talking points for %s" % (campaign), 
        'materials': talking_points,
        'status': 200}))
    return resp
