from app import db
from app.manager.models import Manager
from app.canvasser.models import Canvasser
from app.admin.models import Admin

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    cookie_id = db.Column(db.String(128),  nullable=True)
    first_name = db.Column(db.String(128),  nullable=False)
    last_name = db.Column(db.String(128),  nullable=False)
    phone = db.Column(db.String(128),  nullable=False)
    email = db.Column(db.String(128),  nullable=False)
    password = db.Column(db.String(200),  nullable=False)
    manager = db.relationship('Manager', uselist=False, back_populates='user')
    canvasser = db.relationship('Canvasser', uselist=False, back_populates='user')
    admin = db.relationship('Admin', uselist=False, back_populates='user')

    def __repr__(self):
        return "User(%s, %s)" % (self.id, self.email)

    def serialize(self):
        return {
            'id': self.id,
            'email': self.email,
            'firstname': self.first_name,
            'lastname': self.last_name,
            'phone': self.phone,
            'role': {
                'manager': True if self.manager else False,
                'canvasser': True if self.canvasser else False,
                'admin': True if self.admin else False
            }
        }
    
    def getCookieID(self):
        return self.cookie_id

    @staticmethod
    def validate_register_user_input(req):
        #TODO
        return True

    def set_roles(self, manager, canvasser, admin):
        if self.manager and not manager:
            manager = Manager.query.filter_by(user=self).first()
            db.session.delete(manager)
            db.session.commit()
            for c in manager.campaigns:
                if not c.managers:
                    for l in c.locations:
                        db.session.delete(l)
                    for t in c.tasks:
                        db.session.delete(t)
                    for q in c.questions:
                        db.session.delete(q)
                    db.session.delete(c)
            db.session.commit()
        elif not self.manager and manager:
            manager = Manager(user=self)
            db.session.add(manager)

        if self.canvasser and not canvasser:
            canvasser = Canvasser.query.filter_by(user=self).first()
            campaigns_affected = []
            for t in canvasser.tasks:
                if t.campaign not in campaigns_affected:
                    campaigns_affected.append(t.campaign)
                db.session.delete(t)
            for d in canvasser.dates:
                db.session.delete(d)
            db.session.delete(canvasser)
            db.session.commit()
            for c in campaigns_affected:
                Campaign.generate_assignments(c)

        elif not self.canvasser and canvasser:
            canvasser = Canvasser(user=self)
            db.session.add(canvasser)

        if self.admin and not admin:
            admin = Admin.query.filter_by(user=self).first()
            db.session.delete(admin)
        elif not self.admin and admin:
            admin = Admin(user=self)
            db.session.add(admin)
            
        db.session.commit()
        return
