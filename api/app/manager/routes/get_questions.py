from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.manager.blueprint import mod_manager
from app.manager.models import Manager
from app.campaign.models import Campaign

@mod_manager.route('/get_questions', methods=['POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def get_questions():
    print("Attempting to get question for campaign.")

    # check if manager
    cookieID = request.cookies.get('cookieID')
    if not Manager.is_manager(cookieID):
        print("Get question to campaign failed. Only managers can perform this action.")
        return make_response(jsonify({'comment': "Get question to campaign failed. Only managers can perform this action.", 'status': 403}))

    # get request
    req = request.get_json(force=True)
    print(req)

    # get campaign from db
    campaign = Campaign.query.filter_by(id=req['campaign_id']).first()
    if not campaign:
        print("Get question failed. Campaign not found.")
        return make_response(jsonify({'comment': "Get question failed. Campaign not found.", 'status': 404}))

    # get questions
    questions = [q.question for q in campaign.questions]

    # return success
    print("Return questions for %s: %s" % (campaign, questions))
    resp = make_response(jsonify({
        'comment': "Return questions for %s" % (campaign), 
        'materials': questions,
        'status': 200}))
    return resp
