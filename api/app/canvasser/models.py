from app import db


canvassing_campaigns = db.Table('canvassing_campaigns',
    db.Column('canvasser_id', db.Integer, db.ForeignKey('canvasser.id')),
    db.Column('campaign_id', db.Integer, db.ForeignKey('campaign.id'))
)

class Canvasser(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user = db.relationship('User', back_populates='canvasser')
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    campaigns = db.relationship('Campaign', secondary=canvassing_campaigns, back_populates='canvassers')
    tasks = db.relationship('Task', back_populates='canvasser')
    dates = db.relationship('Date', back_populates='canvasser')

    def __repr__(self):
        return "Canvasser(%s, %s)" % (self.id, self.user.email)

    def serialize(self):
        return {
            'id': self.id,
            'email': self.user.email,
            'firstname': self.user.first_name,
            'lastname': self.user.last_name
        }

    @staticmethod
    def is_canvasser(cookieID):
        from app.auth.models import User
        if (cookieID is None) or (cookieID is ""):
            return False
        user = User.query.filter_by(cookie_id=cookieID).first()
        if not user:
            return False
        if not user.canvasser:
            return False
        
        return True
        
class Date(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    canvasser = db.relationship('Canvasser', back_populates='dates')
    canvasser_id = db.Column(db.Integer, db.ForeignKey('canvasser.id'))
    date = db.Column(db.String(128), nullable=False)
    task = db.relationship('Task', back_populates='date')
    task_id = db.Column(db.Integer, db.ForeignKey('task.id'))

    def __repr__(self):
        return "Date(%s, %s)" % (self.date, self.canvasser)

    def serialize(self):
        return {
            'id': self.id,
            'date': self.date,
            'canvasser': self.canvasser
        }
