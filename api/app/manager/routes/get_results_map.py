from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.manager.blueprint import mod_manager
from app.manager.models import Manager
from app.campaign.models import Campaign
from app.campaign.models import Location
from app.questionnaire.models import Response

@mod_manager.route('/get_results_map', methods=['POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def get_results_map():
    print("Attempting to get location results for map view")

    # check if manager
    cookieID = request.cookies.get('cookieID')
    if not Manager.is_manager(cookieID):
        print("Get location results failed. Only managers can perform this cation")
        return make_response(jsonify({'status': 403, 'comment': "Get location results failed. Only managers can perform this cation"}))
        
    # get request
    req = request.get_json(force=True)
    print(req)

    # get campaign from db
    campaign = Campaign.query.filter_by(id=req['campaign_id']).first()
    if not campaign:
        print("Get location results failed. Specified campaign not found")
        return make_response(jsonify({'status': 404, 'comment': "Get location results failed. Specified campaign not found"}))

    # get locations 
    locations = campaign.locations

    # to_ret
    to_ret = []
    for l in locations:
        temp = {}
        temp['location_id'] = l.id
        temp['address'] = l.address
        temp['latitude'] = l.latitude
        temp['longitude'] = l.longitude
        temp['rating'] = l.rating if l.rating else "None"
        to_ret.append(temp)

    # return success
    print("Returning location results for %s for map view: %s" % (campaign, to_ret))
    resp = make_response(jsonify({
        'comment': "Returning location results for %s for map view" % campaign, 
        'materials': to_ret, 
        'status': 200}))
    return resp
