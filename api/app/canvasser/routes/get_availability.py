from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.canvasser.blueprint import mod_canvasser
from app.auth.models import User
from app.canvasser.models import Canvasser
import datetime as dt

@mod_canvasser.route('/get_availability', methods=['POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def get_availability():
    print("Attempting to get available dates for canvasser.")

    # check if canvasser
    cookieID = request.cookies.get('cookieID')
    if not Canvasser.is_canvasser(cookieID):
        print("Select availability failed. Only canvassers can perform this action.")
        return make_response(jsonify({'comment': "Select availability failed. Only canvassers can perform this action.", 'status': 403}))

    # get request
    req = request.get_json(force=True)
    print(req)

    # get canvasser from db
    user = User.query.filter_by(cookie_id=cookieID).first()
    if not user:
        print("Select availability failed. User not found.")
        return make_response(jsonify({'comment': "Select availability failed. User not found.", 'status': 404}) )
    canvasser = user.canvasser

    # get available dates
    dates = canvasser.dates

    # to_ret
    to_ret = []
    for d in dates:
        to_ret.append(d.date)

    # success return
    print("Get availability successful. %s" % to_ret)
    resp = make_response(jsonify({
        "comment": "Get availability successful.",
        "materials": to_ret,
        "status": 200
    }))
    return resp
