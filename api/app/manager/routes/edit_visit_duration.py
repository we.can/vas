from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.manager.blueprint import mod_manager
from app.campaign.models import Campaign
from app.manager.models import Manager
import datetime as dt


@mod_manager.route('/edit_visit_duration', methods=['POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def edit_visit_duration():
    print("Attempting to edit visit duration for a campaign.")

    # check if manager
    cookieID = request.cookies.get('cookieID')
    if not Manager.is_manager(cookieID):
        print("Edit visit duration failed. Only managers can perform this action.")
        return make_response(jsonify({'comment': "Edit visit duration failed. Only managers can perform this action.", 'status': 403}))

    # get request
    req = request.get_json(force=True)
    print(req)

    # get campaign from db
    campaign = Campaign.query.filter_by(id=req['campaign_id']).first()
    if not campaign:
        print("Edit visit duration failed. Campaign not found.")
        return make_response(jsonify({'comment': "Edit visit duration failed. Campaign not found.", 'status': 404}))

    # if campaign started, cannot make edits
    today_str = str(dt.date.today())
    if today_str >= campaign.startDate:
        print("Add questions failed. Campaign already started.")
        return make_response(jsonify({
            'comment': "Add questions failed. Campaign already started", 
            'status': 400,
            'locked': True}
        ))

    # if campaign started, cannot make edits
    today_str = str(dt.date.today())
    if today_str >= campaign.startDate:
        print("Add questions failed. Campaign already started.")
        return make_response(jsonify({
            'comment': "Add questions failed. Campaign already started", 
            'status': 400,
            'locked': True}
        ))

    # edit visit duration
    campaign.visit_duration = req['visit_duration']

    # commit
    db.session.commit()

    # generate assignments
    Campaign.generate_assignments(campaign)

    # commit
    db.session.commit()

    # return success
    print("Campaign visit duration has been edited: %s" % (campaign.visit_duration))
    resp = make_response(jsonify({
        'comment': "Campaign visit duration has been edited: %s" % (campaign.visit_duration),
        'status': 200
    }))
    return resp

