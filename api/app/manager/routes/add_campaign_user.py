from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.manager.blueprint import mod_manager
from app.manager.models import Manager
from app.auth.models import User
from app.campaign.models import Campaign
import datetime as dt

@mod_manager.route('/add_campaign_user', methods=['POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def add_campaign_user():
    print("Attempting to add a participating user to campaign.")

    # check if manager
    cookieID = request.cookies.get('cookieID')
    if not Manager.is_manager(cookieID):
        print("Add user to campaign failed. Only managers can perform this action.")
        return make_response(jsonify({'comment': "Add user to campaign failed. Only managers can perform this action.", 'status': 403}))

    # get request
    req = request.get_json(force=True)
    print(req)

    # get user from db
    user = User.query.filter_by(email=req['email']).first()
    if not user:
        print("Add campaign user failed. User with email not found")
        return make_response(jsonify({'comment': "Add campaign user failed. User with email not found", 'status': 404}))
    if 'id' not in req:
        return make_response(jsonify({'comment': "Invalid format, no campaign_id found", 'status': 404}))

    # get campaign from db
    campaign = Campaign.query.filter_by(id=req['id']).first()
    if not campaign:
        print("Add campaign user failed. Campaign not found")
        return make_response(jsonify({'comment': "Add campaign user failed. Campaign not found", 'status': 404}))

    # if campaign started, cannot make edits
    today_str = str(dt.date.today())
    if today_str >= campaign.startDate:
        print("Add campaign user failed. Campaign already started.")
        return make_response(jsonify({
            'comment': "Add campaign user failed. Campaign already started", 
            'status': 400,
            'locked': True
        }))

    if 'manager' in req['role']:
        if not user.manager:
            print("User to add doesn'tn have manager role.")
            return make_response(jsonify({'comment': "User to add doesn'tn have manager role", 'status': 403}))
        campaign.managers.append(user.manager)

    if 'canvasser' in req['role']:
        if not user.canvasser:
            print("User to add doesn'tn have canvasser role.")
            return make_response(jsonify({'comment': "User to add doesn'tn have canvasser role", 'status': 403}))
        campaign.canvassers.append(user.canvasser)

    db.session.commit()

    # return success
    print("%s has been added to %s with roles: %s" % (user, campaign, req['role']))
    resp = make_response(jsonify({
        'status': 200, 
        'comment': "%s has been added to %s with roles: %s" % (user, campaign, req['role']),
        'locked': False
    }))
    return resp
