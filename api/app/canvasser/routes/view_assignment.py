from flask import Blueprint, request, jsonify, make_response
from flask_cors import cross_origin
from app import db
from app.canvasser.blueprint import mod_canvasser
from app.auth.models import User
import datetime as dt
from app.canvasser.models import Canvasser
from app.campaign.models import Task
from app.campaign.models import Location

@mod_canvasser.route('/view_assignment', methods=['POST'])
@cross_origin(origin = '*', headers=['Content-Type','application/json'])
def view_assignment():
    print("Attempting to view today's assignment for a given canvasser.")

    # check if canvasser
    cookieID = request.cookies.get('cookieID')
    if not Canvasser.is_canvasser(cookieID):
        print("View today's assignment failed. Only canvassers can perform this action.")
        return make_response(jsonify({'comment': "View today's assignment failed. Only canvassers can perform this action.", 'status': 403}))

    # get request
    req = request.get_json(force=True)
    print(req)

    # get canvasser from db
    user = User.query.filter_by(cookie_id=cookieID).first()
    if not user:
        print("View today's assignment failed. User not found.")
        return make_response(jsonify({'comment': "View today's assignment. User not found.", 'status': 404}) )

    # check if canvasser
    canvasser = user.canvasser
    if not canvasser:
        print("View today's assignment failed. User does not have canvasser role.")
        return make_response(jsonify({'comment': "View today's assignment failed. User does not have canvasser role.", 'status': 403}) )

    # get task from db
    today = str(dt.date.today()) # date object with today's date
    tmp_tasks = Task.query.filter_by(canvasser=canvasser).all()
    if not tmp_tasks:
        return make_response(jsonify({
            'comment': "Returning locations for today's task", 
            'materials': [], 
            'status': 200}))
    task = tmp_tasks[0]
    for t in tmp_tasks:
        if t.date.date == today:
            task = t
    print(tmp_tasks)
    # get locations (ordered)
    locations = []
    i = 1
    for l in task.locations:
        if not l.rating:
            temp = l.serialize()
            temp['number'] = i
            temp['task_id'] = task.id
            i += 1
            locations.append(temp)
            
    # return success
    print("Returning locations for today's task: %s" % (locations))
    resp = make_response(jsonify({
        'comment': "Returning locations for today's task", 
        'materials': locations, 
        'status': 200}))
    return resp

